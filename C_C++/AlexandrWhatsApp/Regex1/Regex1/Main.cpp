#include <iostream>
#include <string>

using namespace std;

bool match(string, string);
bool matchHere(string, string);
bool matchStar(char, string, string);

bool matchStar(char c, string regex, string text) {
	int i = 0;
	while (i <= text.length() && text[i] == c || c == '.') {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

bool matchHere(string regex, string text) {
	if (regex[0] == '\0') {
		return true;
	}
	if (regex[1] == '*') {
		return matchStar(regex[0], regex.substr(2), text);
	}
	if (regex[0] == '$' && regex[1] == '\0') {
		return (text == "");
	}
	if (text != "" && (regex[0] == '.' || regex[0] == text[0])) {
		return matchHere(regex.substr(1), text.substr(1));
	}
	return false;
}

bool match(string regex, string text) {
	if (regex[0] == '^') {
		return matchHere(regex.substr(1), text);
	}
	int i = 0;
	while (i <= text.length()) {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

int main() {
	setlocale(LC_ALL, "RUS");

	string regex;
	string text;

	cout << "������� ������: gr*\n";
	//cin >> regex;
	regex = "gr*";

	cout << "������� ������ ��� ��������: popgrrr\n";
	//cin >> text;
	text = "popgrrr";

	if (match(regex, text)) {
		cout << "������ ������������� �������\n";
	}
	else {
		cout << "������ �� ������������� �������\n";
	}

	cout << "������� ������ ��� ��������: poprrr\n";
	//cin >> text;
	text = "poprrr";

	if (match(regex, text)) {
		cout << "������ ������������� �������\n";
	}
	else {
		cout << "������ �� ������������� �������\n";
	}
	system("PAUSE");
	return 0;
}