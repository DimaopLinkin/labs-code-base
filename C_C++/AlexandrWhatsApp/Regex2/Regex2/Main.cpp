#include <iostream>
#include <string>
#include "Regex.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "RUS");

	Regex* regexpr = new Regex("gr*");

	string text;
	cout << "������� ������: popgrrrr" << endl;
	//cin >> text;
	text = "popgrrrr";

	cout << "������������ ���������� cout\n" << *regexpr << endl;
	string metodToString = regexpr->operator string();
	cout << "������������ ������ string()\n" << metodToString << endl;

	if (regexpr->match(text)) {
		cout << "�������������" << endl;
	}
	else {
		cout << "�� �������������" << endl;
	}

	cout << "������� ������: popr" << endl;
	text = "popr";
	if (regexpr->match(text)) {
		cout << "�������������" << endl;
	}
	else {
		cout << "�� �������������" << endl;
	}

	system("PAUSE");
	return 0;
}