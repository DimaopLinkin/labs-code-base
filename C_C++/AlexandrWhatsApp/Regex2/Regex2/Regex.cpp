#include "Regex.h"

bool Regex::matchHere(string regex, string text)
{
	if (regex[0] == '\0') {
		return true;
	}
	if (regex[1] == '*') {
		return matchStar(regex[0], regex.substr(2), text);
	}
	if (regex[0] == '$' && regex[1] == '\0') {
		return (text == "");
	}
	if (text != "" && (regex[0] == '.' || regex[0] == text[0])) {
		return matchHere(regex.substr(1), text.substr(1));
	}
	return false;
}

bool Regex::matchStar(char c, string regex, string text)
{
	int i = 0;
	while (i <= text.length() && text[i] == c || c == '.') {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

Regex::Regex() {
	cout << "regex ";
	cin >> regex;
}

Regex::Regex(string regex) {
	this->regex = regex;
}

bool Regex::match(string text)
{
	if (regex[0] == '^') {
		return matchHere(regex.substr(1), text);
	}
	int i = 0;
	while (i <= text.length()) {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

Regex& Regex::operator=(const Regex& regex1)
{
	if (this == &regex1) return *this;
	regex = regex1.regex;
	return *this;
}

Regex::operator string() const
{
	return regex;
}

ostream& operator<<(ostream& out, const Regex& regex1)
{
	out << "Regex is " << regex1.regex;
	return out;
}

bool operator==(const Regex& left, const Regex& right)
{
	return left.regex == right.regex;
}
