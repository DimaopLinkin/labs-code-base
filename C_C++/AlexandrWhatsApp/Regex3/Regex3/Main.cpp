#include <iostream>
#include <string>
#include "Regex.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "RUS");

	cout << "�������� SimpleRegex: rgg\n";
	SimpleRegex* simpleRegex = new SimpleRegex("rgg");

	string simpleString;
	cout << "������� ������ ��� SimpleRegex: goorggle\n";
	simpleString = "goorggle";
	cout << "������������ ������������ ������������ �������\n";
	cout << *simpleRegex << endl;
	cout << simpleRegex->operator string() << endl;
	if (simpleRegex->match(simpleString)) {
		cout << "������ ������������� ����������� ����������� ���������\n";
	}
	else {
		cout << "������ �� ������������� ����������� ����������� ���������\n";
	}
	cout << "������� ������ ��� SimpleRegex: goorgle\n";
	simpleString = "goorgle";
	if (simpleRegex->match(simpleString)) {
		cout << "������ ������������� ����������� ����������� ���������\n";
	}
	else {
		cout << "������ �� ������������� ����������� ����������� ���������\n";
	}

	cout << "�������� FileRegex: cpp\n";
	FileRegex* fileRegex = new FileRegex("cpp");

	string fileString;
	cout << "������� ��� ����� ��� FileRegex: Main.cpp\n";
	fileString = "Main.cpp";
	cout << "������������ ������������ ������������ �������\n";
	cout << *fileRegex << endl;
	cout << fileRegex->operator string() << endl;
	if (fileRegex->isExtensionMatch(fileString)) {
		cout << "���������� ����� ������������� ����������� ���������\n";
	}
	else {
		cout << "���������� ����� �� ������������� ����������� ���������\n";
	}
	if (fileRegex->isNameMatch(fileString)) {
		cout << "��� ����� ������������� ����������� ���������\n";
	}
	else {
		cout << "��� ����� �� ������������� ����������� ���������\n";
	}


	cout << "������� ��� ����� ��� FileRegex: cpp.h\n";
	fileString = "cpp.h";
	if (fileRegex->isExtensionMatch(fileString)) {
		cout << "���������� ����� ������������� ����������� ���������\n";
	}
	else {
		cout << "���������� ����� �� ������������� ����������� ���������\n";
	}
	if (fileRegex->isNameMatch(fileString)) {
		cout << "��� ����� ������������� ����������� ���������\n";
	}
	else {
		cout << "��� ����� �� ������������� ����������� ���������\n";
	}

	system("PAUSE");
	return 0;
}