#pragma once
#include <string>
#include <iostream>

using namespace std;

class Regex {
protected:
	string regex;
	bool matchHere(string, string);
	bool matchStar(char, string, string);
public:
	Regex();
	Regex(string);
	bool match(string);

	friend ostream& operator<<(ostream&, const Regex&);
	friend bool operator==(const Regex&, const Regex&);
	Regex& operator=(const Regex&);
	operator string() const;
};

class SimpleRegex : public Regex {
	string isValid(string);
public:
	SimpleRegex();
	SimpleRegex(string regex);
};

class FileRegex : public Regex {
public:
	FileRegex(string regex);
	bool isExtensionMatch(string);
	bool isNameMatch(string);
};