#include "Regex.h"

bool Regex::matchHere(string regex, string text)
{
	if (regex[0] == '\0') {
		return true;
	}
	if (regex[1] == '*') {
		return matchStar(regex[0], regex.substr(2), text);
	}
	if (regex[0] == '$' && regex[1] == '\0') {
		return (text == "");
	}
	if (text != "" && (regex[0] == '.' || regex[0] == text[0])) {
		return matchHere(regex.substr(1), text.substr(1));
	}
	return false;
}

bool Regex::matchStar(char c, string regex, string text)
{
	int i = 0;
	while (i <= text.length() && text[i] == c || c == '.') {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

StringList* Regex::deleteInList(StringList* lst) {
	int count = 0;
	StringList* current = lst;
	StringList* root = lst;
	do {
		if (!match(current->data)) {
			if (root == current) {
				root = root->next;
			}
			if (current->next != NULL) {
				current = current->next;
				deletelem(current->prev);
			}
			else {
				deletelem(current);
				count++;
				break;
			}			
			count++;
		}
		else {
			current = current->next;
		}
	} while (true);
	cout << "������� " << count << " ���������\n";
	return root;
}

Regex::Regex() {
	cout << "regex ";
	cin >> regex;
}

Regex::Regex(string regex) {
	this->regex = regex;
}

bool Regex::match(string text)
{
	if (regex[0] == '^') {
		return matchHere(regex.substr(1), text);
	}
	int i = 0;
	while (i <= text.length()) {
		if (matchHere(regex, text.substr(i))) {
			return true;
		}
		i++;
	}
	return false;
}

Regex& Regex::operator=(const Regex& regex1)
{
	if (this == &regex1) return *this;
	regex = regex1.regex;
	return *this;
}

Regex::operator string() const
{
	return regex;
}

StringList* init(string a)  // �- �������� ������� ����
{
	StringList* lst = new StringList();
	lst->data = a;
	lst->next = NULL; // ��������� �� ��������� ����
	lst->prev = NULL; // ��������� �� ���������� ����
	return(lst);
}

StringList* addelem(StringList* lst, string str)
{
	StringList* temp = new StringList();
	StringList* p = new StringList();
	p = lst->next; // ���������� ��������� �� ��������� ����
	lst->next = temp; // ���������� ���� ��������� �� �����������
	temp->data = str; // ���������� ���� ������ ������������ ����
	temp->next = p; // ��������� ���� ��������� �� ��������� ����
	temp->prev = lst; // ��������� ���� ��������� �� ���������� ����
	if (p != NULL)
		p->prev = temp;
	return(temp);
}

void deletelem(StringList* lst)
{
	StringList* prev = new StringList();
	StringList* next = new StringList();
	prev = lst->prev; // ����, �������������� lst
	next = lst->next; // ����, ��������� �� lst
	if (prev != NULL)
		prev->next = lst->next; // ������������ ���������
	if (next != NULL)
		next->prev = lst->prev; // ������������ ���������
	free(lst); // ����������� ������ ���������� ��������
}

void listprint(StringList* lst)
{
	StringList* p = new StringList();
	p = lst;
	int i = 1;
	do {
		cout << i << ". " << p->data << endl;
		p = p->next; // ������� � ���������� ����
		i++;
	} while (p != NULL); // ������� ��������� ������
}

ostream& operator<<(ostream& out, const Regex& regex1)
{
	out << "Regex is " << regex1.regex;
	return out;
}

bool operator==(const Regex& left, const Regex& right)
{
	return left.regex == right.regex;
}

string SimpleRegex::isValid(string regex)
{
	for (int i = 0; i <= regex.length(); i++) {
		if (regex[i] == '*') {
			regex.erase(i, 1);
		}
	}
	return regex;
}

SimpleRegex::SimpleRegex()
{
	cout << "regex ";
	cin >> regex;
	regex = isValid(regex);
}

SimpleRegex::SimpleRegex(string regex)
{
	this->regex = isValid(regex);
}

bool FileRegex::isExtensionMatch(string text)
{
	return match(text.substr(text.rfind('.')));
}

bool FileRegex::isNameMatch(string text) {
	return match(text.substr(0, text.rfind('.')));
}
