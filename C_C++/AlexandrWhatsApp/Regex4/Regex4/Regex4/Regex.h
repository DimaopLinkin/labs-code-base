#pragma once
#include <string>
#include <iostream>

using namespace std;

class StringList {
public:
    StringList* next;
	StringList* prev;
	string data;
};

StringList* init(string a);

StringList* addelem(StringList* lst, string str);

void deletelem(StringList* lst);

void listprint(StringList* lst);

class Regex {
protected:
	string regex;
	bool matchHere(string, string);
	bool matchStar(char, string, string);
public:
	Regex();
	Regex(string);
	bool match(string);
	StringList* deleteInList(StringList* lst);

	friend ostream& operator<<(ostream&, const Regex&);
	friend bool operator==(const Regex&, const Regex&);
	Regex& operator=(const Regex&);
	operator string() const;
};

class SimpleRegex : public Regex {
	string isValid(string);
public:
	SimpleRegex();
	SimpleRegex(string regex);
};

class FileRegex : public Regex {
public:
	bool isExtensionMatch(string);
	bool isNameMatch(string);
};