#include <iostream>
#include <fstream>
#include <string>
#include "Regex.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "RUS");

	cout << "�������� Regex: rg\n";
	Regex* regex = new Regex("rg");

	cout << "�������� ����� Input.txt ...\n";
	ifstream fileIn("Input.txt");
	if (!fileIn.is_open()) {
		cout << "���� �� ������!\n";
		return -1;
	}
	cout << "���� ������!\n";
	cout << "������ �����, � �������� ���������� ����������:\n";
	string s;
	for (fileIn >> s; !fileIn.eof(); fileIn >> s) {
		cout << s << " - ";
		if (regex->match(s)) cout << "�������� �������" << endl;
		else cout << "�� �������� �������" << endl;
	}

	fileIn.close();
	delete regex;

	system("PAUSE");
	return 0;
}