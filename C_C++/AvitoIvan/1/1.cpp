#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, n2, n3;
    cout << "Введите три числа " << endl;
    cin >> n1 >> n2 >> n3;
    if (n1 > n2) swap(n1, n2);
    if (n1 > n3) swap(n1, n3);
    if (n2 > n3) swap(n2, n3);
    cout << n1 << " " << n2 << " " << n3 << endl;

    // system("PAUSE");
    return 0;
}