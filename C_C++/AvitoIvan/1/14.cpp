#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2, n3;
    cout << "Введите три числа " << endl;
    cin >> n1 >> n2 >> n3;
    if ((n1 + n2 + n3) % 3 == 0) cout << "Да" << endl;
    else cout << "Нет" << endl;

    // system("PAUSE");
    return 0;
}