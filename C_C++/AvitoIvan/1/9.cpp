#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, n2;
    int n3;
    cout << "Введите три числа " << endl;
    cin >> n1 >> n2 >> n3;
    switch(n3) {
        case 1: cout << n1 + n2 << endl; break;
        case 2: cout << n1 - n2 << endl; break;
        case 3: cout << n1 * n2 << endl; break;
        default: cout << "Ошибка ввода" << endl; break;
    }

    // system("PAUSE");
    return 0;
}