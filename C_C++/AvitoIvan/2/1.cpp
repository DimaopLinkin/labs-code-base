#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int input, max;
    cout << "Введите числа. Для остановки введите 777" << endl;
    if (input == 777) return 0;
    max = input;
    while (input != 777) {
        if (input > max) max = input;
        cin >> input;
    }
    cout << max << endl;

    // system("PAUSE");
    return 0;
}