#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2, n3, count, i;
    float sum;
    count, sum, i = 0;
    while (true) {
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2 >> n3;
        if (n1 == 999) break;
        if (i % 2 == 0) {
            if (n2 % 2 == 1) { 
                sum += n2;
                count++;
            }
        }
        else {
            if (n1 % 2 == 1) { 
                sum += n1;
                count++;
            }
            if (n3 % 2 == 1) { 
                sum += n3;
                count++;
            }
        }
        i++;
    }
    cout << sum / count << endl;
    

    // system("PAUSE");
    return 0;
}