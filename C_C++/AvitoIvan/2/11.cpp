#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, min, i, mini;
    mini = 0;
    i = 0;
    cin >> n1;
    if (n1 == 777) return 0;
    min = n1;
    while(true) {
        cin >> n1;
        if (n1 == 777) break;
        i++;
        if (n1 < min) {
            min = n1;
            mini = i;
        }
    }
    cout << mini << endl;

    // system("PAUSE");
    return 0;
}