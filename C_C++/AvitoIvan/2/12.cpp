#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, max, i, maxi;
    maxi = 0;
    i = 0;
    cin >> n1;
    if (n1 == 999) return 0;
    max = n1;
    while(true) {
        cin >> n1;
        if (n1 == 999) break;
        i++;
        if (n1 > max && n1 % 10 == 6) {
            max = n1;
            maxi = i;
        }
    }
    cout << maxi << endl;

    // system("PAUSE");
    return 0;
}