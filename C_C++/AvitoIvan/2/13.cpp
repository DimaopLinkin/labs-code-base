#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, proizv;
    proizv = 1;
    cin >> n1;
    if (n1 == 999) return 0;
    while(true) {
        cin >> n1;
        if (n1 == 999) break;
        if (n1 % 10 == 0 && n1 % 3 == 0) proizv *= n1;
    }

    cout << proizv << endl;

    // system("PAUSE");
    return 0;
}