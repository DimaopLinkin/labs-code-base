#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, min, minsec;
    cin >> n1;
    if (n1 == 999) return 0;
    min = n1;
    while(true) {
        cin >> n1;
        if (n1 == 999) break;
        if (n1 < min) {
            minsec = min;
            min = n1;
        }
    }
    cout << minsec << endl;

    // system("PAUSE");
    return 0;
}