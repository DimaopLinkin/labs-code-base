#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1;
    cin >> n1;
    if (n1 == 777) return 0;
    while(true) {
        cin >> n1;
        if (n1 == 777) break;
        cout << (n1 + 5) % 7;
    }

    // system("PAUSE");
    return 0;
}