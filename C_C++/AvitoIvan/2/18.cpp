#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2, n3, sum;
    bool simple;
    while (true) {
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2 >> n3;
        if (n1 == 999) return 0;
        sum = n1 + n2 + n3;
        simple = true;
        for (int i = 2; i < sum; i++) {
            if (sum % i == 0) {
                simple = false; break;
            }
        }
        if (simple) {
            cout << "Да" << endl;
        }
        else {
            if (n1 > n2) swap(n1, n2);
            if (n1 > n3) swap(n1, n3);
            if (n2 > n3) swap(n2, n3);
            cout << n3 << endl;
        }
    }
    

    // system("PAUSE");
    return 0;
}