#include "iostream"
#include "math.h"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2;
    int n3;
    while (true){
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2 >> n3;
        if (n1 == 777) return 0;
        switch(n3) {
            case 1: cout << pow(n1, n2) << endl; break;
            case 2: cout << n1 - n2 << endl; break;
            case 3: cout << n1 + n2 << endl; break;
            case 4: cout << n1 % n2 << endl; break;
            default: cout << "Ошибка ввода" << endl; break;
        }
    }

    // system("PAUSE");
    return 0;
}