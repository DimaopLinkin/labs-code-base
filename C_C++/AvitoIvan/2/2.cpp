#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float input, sum, count;
    cout << "Введите числа. Для остановки введите 999" << endl;
    sum, count = 0;
    while (true) {
        cin >> input;
        if (input == 999) break;
        sum += input;
        count++;
    }
    if (!count) cout << "Нет чисел" << endl;
    else cout << sum / count << endl;    

    // system("PAUSE");
    return 0;
}