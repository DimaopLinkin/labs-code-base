#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, count;
    bool chet;
    count = 0;
    cin >> n1;
    if (n1 == 777) return 0;
    while(true) {
        cin >> n1;
        if (n1 == 777) break;
        chet = true;
        while (n1 >= 10) {
            if (n1 % 2 != 0) chet = false;
            n1 = n1 / 10;
        }
        if (chet) count++;
    }
    cout << count << endl;

    // system("PAUSE");
    return 0;
}