#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2, count;
    count = 0;
    cin >> n1 >> n2;
    if (n1 == 999) return 0;
    while(true) {
        cin >> n1;
        if (n1 == 999) break;
        if (n1 * n1 == n2 || n2 * n2 == n1) count++;
    }
    cout << count << endl;

    // system("PAUSE");
    return 0;
}