#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int day, month;
    cout << "Введите день и месяц. Для остановки введите 777" << endl;
    cin >> day >> month;
    while (day != 777) {
        if (month > 12 || month < 1) cout << "Месяца не сущестует" << endl;
        else if (day <= days[month-1] && day > 0) cout << "Да" << endl;
        else cout << "Нет" << endl;
        cin >> day >> month;
    }

    // system("PAUSE");
    return 0;
}