#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    int day, month;
    cout << "Введите день. Для остановки введите 777" << endl;
    while (true) {
        cin >> day;
        if (day == 777) return 0;
        if (day < 1 || day > 365) continue;
        month = 0;
        while (day > days[month]) {
            day -= days[month];
            month++;
        }
        cout << day << " " << month + 1 << endl;
    }

    // system("PAUSE");
    return 0;
}