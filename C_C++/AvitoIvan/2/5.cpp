#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, n2, n3;
    cout << "Для остановки введите 999 " << endl;
    while (true) {
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2 >> n3;
        if (n1 == 999) return 0;
        if (n1 < n2 && n1 > n3 || n1 < n3 && n1 > n2) cout << "1" << endl;
        if (n2 < n1 && n2 > n3 || n2 < n3 && n2 > n1) cout << "2" << endl;
        if (n3 < n1 && n3 > n2 || n3 < n2 && n3 > n1) cout << "3" << endl;
    }

    // system("PAUSE");
    return 0;
}