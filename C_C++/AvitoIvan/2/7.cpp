#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, plus, minus, zero = 0;
    cout << "Для остановки введите 777 " << endl;
    while (true) {
        cout << "Введите число" << endl;
        cin >> n1;
        if (n1 == 777) break;
        if (n1 > 0) plus++;
        if (n1 < 0) minus++;
        if (n1 ==0) zero++;
    }
    cout << "+ " << plus << endl << "- " << minus << endl << "0 " << zero << endl;

    // system("PAUSE");
    return 0;
}