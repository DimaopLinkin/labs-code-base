#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    float n1, n2, n3;
    while (true) {
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2 >> n3;
        if (n1 == 777) return 0;
        if (n3 <= n1 && n3 >= n2 || n3 <= n2 && n3 >= n1) cout << "Да" << endl;
        else cout << "Нет" << endl;
    }
    

    // system("PAUSE");
    return 0;
}