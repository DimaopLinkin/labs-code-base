#include "iostream"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    int n1, n2, count;
    count = 0;
    while (true) {
        cout << "Введите три числа " << endl;
        cin >> n1 >> n2;
        if (n1 == 999) break;
        if (n1 * n1 == n2) count++;
    }
    cout << count << endl;    

    // system("PAUSE");
    return 0;
}