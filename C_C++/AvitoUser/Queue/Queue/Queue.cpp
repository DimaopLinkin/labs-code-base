#include <iostream>

#include <deque>
#include <list>

template <typename T, typename Container = std::deque<T>>
class Queue {
protected:
	Container _queue;
public:
	Queue() {}
	T front() const {
		if (!_queue.empty()) return _queue.front();
		else return NULL;
	}
	T& front() {
		if (!_queue.empty() != 0) return _queue.front();
	}
	void pop() {
		if (_queue.size() != 0) _queue.pop_front();
	}
	void push(T elem) {
		_queue.push_back(elem);
	}
	int size() {
		return _queue.size();
	}
	bool empty() {
		return _queue.empty();
	}
	friend bool operator==(const Queue& left, const Queue& right) {
		return left._queue == right._queue;
	}
	friend bool operator!=(const Queue& left, const Queue& right) {
		return left._queue != right._queue;
	}
};

int main() {
	std::cout << "queue created\n";
	Queue<int, std::deque<int>> queue = Queue<int, std::deque<int>>();
	std::cout << "queue2 created\n";
	Queue<int, std::list<int>> queue2 = Queue<int, std::list<int>>();
	std::cout << "queue size\n";
	std::cout << queue.size() << std::endl;
	std::cout << "queue try to pop empty\n";
	queue.pop();
	std::cout << "queue push 10\n";
	queue.push(10);
	std::cout << "queue push 15\n";
	queue.push(15);
	std::cout << "queue front ";
	std::cout << queue.front() << std::endl;
	std::cout << "queue pop\n";
	queue.pop();
	std::cout << "queue size ";
	std::cout << queue.size() << std::endl;
	std::cout << "queue front ";
	std::cout << queue.front() << std::endl;
	std::cout << "&front = 30\n";
	int& front = queue.front();
	front = 30;
	std::cout << "queue front\n";
	std::cout << queue.front() << std::endl;
	std::cout << "front = 50\n";
	int front1 = queue.front();
	front1 = 50;
	std::cout << "queue front ";
	std::cout << queue.front() << std::endl << std::endl;

	std::cout << "queue_match created\n";
	Queue<int, std::deque<int>> queue_match = Queue<int, std::deque<int>>();
	std::cout << "queue_match push 10\n";
	queue_match.push(10);
	std::cout << "queue_match push 30\n";
	queue_match.push(30);
	std::cout << "queue_match == queue ";
	std::cout << (queue == queue_match) << std::endl;
	std::cout << "queue_match != queue ";
	std::cout << (queue != queue_match) << std::endl;
	std::cout << "queue_match pop\n";
	queue_match.pop();
	std::cout << "queue_match pop\n";
	queue_match.pop();
	std::cout << "queue_match push 30\n";
	queue_match.push(30);
	std::cout << "queue_match == queue ";
	std::cout << (queue == queue_match) << std::endl;
	std::cout << "queue_match != queue ";
	std::cout << (queue != queue_match) << std::endl << std::endl;

	std::cout << queue2.size() << std::endl;
	queue2.push(10);
	queue2.push(15);
	std::cout << queue2.front() << std::endl;
	queue2.pop();
	std::cout << queue2.size() << std::endl;
	std::cout << queue2.front() << std::endl;
	int& front2 = queue2.front();
	front2 = 30;
	std::cout << queue2.front() << std::endl;

	system("PAUSE");
	return 0;
}