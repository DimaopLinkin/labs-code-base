#include <stdio.h>
#pragma warning(disable : 4996)

int pow(int X, int N) {
	int result = X;
	if (N == 0) return 1;
	if (N < 0) return -1;
	for (int i = 1; i < N; i++) {
		result = result * X;
	}
	return result;
}

int main() {
	int x, n;
	printf("x = ");
	scanf("%d", &x);
	printf("n = ");
	scanf("%d", &n);
	printf("%d\n", pow(x,n));
	system("PAUSE");
	return 0;
}