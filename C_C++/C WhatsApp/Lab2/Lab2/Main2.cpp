#include <stdio.h>
#include <stdlib.h>
#pragma warning(suppress : 4996)

struct vector {
	int x;
	vector *next;
};

int length(vector* item) {
	int i = 0;
	while (item->next != NULL) {
		item = item->next;
		i++;
	}
	return i;
}

vector* vectorAdd(vector* item, int x) {
	vector* head = item;
	vector* newItem = (vector*)malloc(sizeof(vector));
	if (head == NULL) {
		newItem->x = x;
		newItem->next = NULL;
		return newItem;
	}
	if (newItem != NULL) {
		newItem->x = x;
		newItem->next = NULL;
		if (item != NULL) {
			while (item->next != NULL) { 
				item = item->next;
			}
			item->next = newItem;
		}
		else return newItem;
	}
	return head;
}

vector* plus(vector* item1, vector* item2) {
	vector* newItem = (vector*)malloc(sizeof(vector));
	newItem = NULL;
	if (length(item1) != length(item2)) {
		printf("Lengths are not equal");
		return newItem;
	}
	while (item1 != NULL) {
		newItem = vectorAdd(newItem, item1->x + item2->x);
		item1 = item1->next;
		item2 = item2->next;
	}
	return newItem;
}

vector* minus(vector* item1, vector* item2) {
	vector* newItem = (vector*)malloc(sizeof(vector));
	newItem = NULL;
	if (length(item1) != length(item2)) {
		printf("Lengths are not equal");
		return newItem;
	}
	while (item1 != NULL) {
		newItem = vectorAdd(newItem, item1->x - item2->x);
		item1 = item1->next;
		item2 = item2->next;
	}
	return newItem;
}

int scalar(vector* item1, vector* item2) {
	if (length(item1) != length(item2)) {
		printf("Lengths are not equal");
		return 0;
	}
	int result = 0;
	while (item1->next != NULL) {
		result += item1->x * item2->x;
		item1 = item1->next;
		item2 = item2->next;
	}
	return result;
}

void vectorPrint(vector* item) {
	if (item != NULL) {
		printf("%d ", item->x);
		vectorPrint(item->next);
	}
}

vector* vectorDelete(vector* item) {
	if (item != NULL) {
		vector* nextNode = item->next;
		free(item);
		return vectorDelete(nextNode);
	}
	return NULL;
}

int main() {

	vector* v1 = vectorAdd(NULL, 2);
	v1 = vectorAdd(v1, 5);
	v1 = vectorAdd(v1, 3);
	v1 = vectorAdd(v1, 6);
	v1 = vectorAdd(v1, 1);
	vectorPrint(v1);
	printf("\n");

	vector* v2 = vectorAdd(NULL, 5);
	v2 = vectorAdd(v2, 1);
	v2 = vectorAdd(v2, 4);
	v2 = vectorAdd(v2, 6);
	v2 = vectorAdd(v2, 9);
	vectorPrint(v2);
	printf("\n");

	vector* v3 = plus(v1, v2);
	vectorPrint(v3);
	printf("\n");

	vector* v4 = minus(v1, v2);
	vectorPrint(v4);
	printf("\n");

	printf("%d", scalar(v1, v2));
	printf("\n");

	v1 = vectorDelete(v1);
	v2 = vectorDelete(v2);
	v3 = vectorDelete(v3);
	v4 = vectorDelete(v4);
	vectorPrint(v1);
	printf("\n");
	vectorPrint(v2);
	printf("\n");
	vectorPrint(v3);
	printf("\n");
	vectorPrint(v4);
	printf("\n");
	system("PAUSE");
	return 0;
}