#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#pragma warning(disable : 4996)

int main() {
	FILE* file1 = fopen("file1", "r");
	
	int count1 = 0, count2 = 0;
	double sum1 = 1, sum2 = 0;

	int number;
	printf("File1 ");
	while (fscanf(file1, "%d", &number) != EOF) {
		printf("%d ", number);
		if (number % 2 == 0) {
			sum1 *= number;
			count1++;
		}
		else {
			sum2 += number;
			count2++;
		}
	}
	fclose(file1);
	if (count1 != 0) {
		sum1 = pow(sum1, (1.0 / count1));
	}
	else sum1 = 0;

	if (count2 != 0) {
		sum2 = sum2 / count2;
	}
	
	FILE* file2 = fopen("file2", "w+");
	printf("\nFile2 %.2f", sum1);
	fprintf(file2, "%.2f", sum1);
	fclose(file2);
	FILE* file3 = fopen("file3", "w+");
	printf("\nFile3 %.2f\n", sum2);
	fprintf(file3, "%.2f", sum2);
	fclose(file3);
	system("PAUSE");
	return 0;
}