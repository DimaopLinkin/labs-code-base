#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node
{
	int data;
	node* next;
};

node* nodeAdd(node* item, int data)
{
	node* newItem = (node*)malloc(sizeof(node));
	
	if (newItem != NULL)
	{		
		newItem->data = data;
		newItem->next = item;
	}
	return newItem;
}

node* nodeDelete(node* item)
{
	if (item != NULL)
	{
		if (item->next != NULL) {
			node* nextNode = item->next;
			free(item);
			return nodeDelete(nextNode);
		}
		free(item);
	}
	return NULL;
}

node* nodeReverse(node* item, node* tail = NULL) {
	if (item != NULL) {
		node* nx = item->next;
		item->next = tail;
		if (nx != NULL) return nodeReverse(nx, item);
		else return item;
	}
	return item;
}

node* nodeCopy(node* item) {
	node* reversed = nodeReverse(item);
	node* newNode = NULL;
	while (reversed != NULL) {
		newNode = nodeAdd(newNode, reversed->data);
		reversed = reversed->next;
	}
	return newNode;
}

void nodePrint(node* item) {
	if (item != NULL) {
		printf("%d\n", item->data);
		printf("\n");
		if (item->next != NULL) nodePrint(item->next);
	}
}

int main() {

	node* С;

	p = nodeAdd(p, 6);
	p = nodeAdd(p, 7);
	nodePrint(p);
	
	printf("\n");

	node* copy = nodeCopy(p);
	copy = nodeAdd(copy, 6);
	nodePrint(copy);

	free(a);
	free(b);
	free(c);

	system("PAUSE");
	return 0;
}