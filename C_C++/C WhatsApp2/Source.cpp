#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node
{
	int data;
	node* next;
};

node* nodeAdd(node* item, int data)
{
	node* newItem = (node*)malloc(sizeof(node));

	if (newItem != NULL)
	{
		newItem->data = data;
		newItem->next = item;
	}
	return newItem;
}

node* nodeDelete(node* item)
{
	if (item != NULL)
	{
		if (item->next != NULL) {
			node* nextNode = item->next;
			free(item);
			return nodeDelete(nextNode);
		}
		free(item);
	}
	return NULL;
}

node* nodeReverse(node* item, node* tail = NULL) {
	if (item != NULL) {
		node* nx = item->next;
		item->next = tail;
		if (nx != NULL) return nodeReverse(nx, item);
		else return item;
	}
	return item;
}

void nodePrint(node* item) {
	if (item != NULL) {
		printf("%d ", item->data);
		if (item->next != NULL) nodePrint(item->next);
	}
}

int find(node* item, int value) {
	while (item != NULL) {
		if (item->data == value) {
			return 1;
		}
		item = item->next;
	}
	return 0;
}

node* nodeCopy(node* item1, node* item2) {
	node* reversed = nodeReverse(item1);
	node* newNode = NULL;
	while (reversed != NULL) {
		if (find(item2, reversed->data)) {
			newNode = nodeAdd(newNode, reversed->data);
		}
		reversed = reversed->next;
	}
	return newNode;
}

int main() {

	node* C;

	node* M1 = NULL;
	node* M2 = NULL;

	for (int i = 1; i < 20; i += 2) {
		M1 = nodeAdd(M1, i);
	}
	for (int i = 5; i < 15; i++) {
		M2 = nodeAdd(M2, i);
	}

	nodePrint(M1);
	printf("\n");
	nodePrint(M2);
	printf("\n");

	C = nodeCopy(M1, M2);
	nodePrint(C);

	free(C);
	free(M1);
	free(M2);

	system("PAUSE");
	return 0;
}