#include <iostream>
#include <math.h>

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    double x = pow(5, 0), 
           y = 1.3, 
           z = -0.5;
    double a = (pow(exp(3*x), 1/4) + 0.8 * pow(tan(2*x), 2))/(pow(y - 3, 1/3) + 0.5 * tan(2*x));
    double b = 1.35 * log(pow(tan(2*x), 2)) + 7.5 * z;
    cout << "a = " << a << endl << "b = " << b << endl; 

    return 0;
}