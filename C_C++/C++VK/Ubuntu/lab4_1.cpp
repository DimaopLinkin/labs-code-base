#include <iostream>
#include <math.h>

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    double a, b, c, x, f;
    cout << "Введите a, b, c, x\n";
    cin >> a >> b >> c >> x;
    if (c < 0 && b != 0) f = a * pow(x, 2) + pow(b, 2) * x + c;
    else if (c > 0 && b == 0) f = (x + a) / (x + c);
    else f = x / c;
    cout << "F = " << f << endl;

    return 0;
}