#include <iostream>
#include <math.h>
using namespace std;
 
int main()
{   
    setlocale(LC_ALL, "RUS");
    unsigned long int n;
    do {
        cin >> n;
    }
    while (0 >= n && n > 2*pow(10,9));
    int k = 0;
    while (n % 10 == 0) {
        k++;
        n /= 10;
    }
    int a = n;
    cout << "n = " << a << "*10^" << k << endl;
    return 0;    
}