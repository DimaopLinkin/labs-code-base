#include <iostream>
using namespace std;
#define n 4

int main()
{   
    setlocale(LC_ALL, "RUS");
    int k = 0;
    int myArray[n][n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << "[" << i << "][" << j << "]" << endl;
            cin >> myArray[i][j];
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int sum = 0;
            for (int z = 0; z < n; z++) {
                if (z == j) continue;
                sum += myArray[i][z];
            }
            if (myArray[i][j] > sum) k++;
        }
    }
    cout << k << " особых элементов" << endl;
    return 0;
}