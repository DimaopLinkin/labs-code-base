#include <iostream>
using namespace std;

int main()
{   
    setlocale(LC_ALL, "RUS");
    int N1, N2;
    cin >> N1 >> N2;
    int ** myArray = new int*[N1];
    for (int i = 0; i < N1; i++)
    myArray[i] = new int[N2];
    for (int i = 0; i < N1; i++) {
        for (int j = 0; j < N2; j++) {
            cout << "[" << i << "][" << j << "]" << endl;
            cin >> myArray[i][j];
        }
    }

    for (int i = 0; i < N1; i++) {
        for (int j = 1; j < N2-1; j++) {
            int sum = 0;
            int sum2 = 0;
            for (int z = 0; z < j; z++) {
                sum += myArray[i][z];
            }
            for (int z = j + 1; z < N2; z++) {
                sum2 += myArray[i][z];
            }
            if (myArray[i][j] > sum && myArray[i][j] < sum2) {
                for (int z = 0; z < N2; z++) {
                    cout << myArray[i][z] << "; ";
                }
                cout << endl;
                break;
            }
        }
    }
    delete[] myArray;
    return 0;
}