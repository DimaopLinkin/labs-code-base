#include "patient1.h"
#include <iostream>

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    string lastName;
    string gender;
    int age;
    Patient patient1;
    do {
        cout << "Пациент 1\nФамилия: ";
        cin >> lastName;
    }
    while (!patient1.setLastName(lastName));
    do {
        cout << "Пол: ";
        cin >> gender;
    }
    while (!patient1.setGender(gender));
    
    do {
        cout << "Возраст: ";
        cin >> age;
    }
    while (!patient1.setAge(age));

    Patient patient2;
    do {
        cout << "Пациент 2\nФамилия: ";
        cin >> lastName;
    }
    while (!patient2.setLastName(lastName));
    do {
        cout << "Пол: ";
        cin >> gender;
    }
    while (!patient2.setGender(gender));
    
    do {
        cout << "Возраст: ";
        cin >> age;
    }
    while (!patient2.setAge(age));

    Patient patient3;
    do {
        cout << "Пациент 3\nФамилия: ";
        cin >> lastName;
    }
    while (!patient3.setLastName(lastName));
    do {
        cout << "Пол: ";
        cin >> gender;
    }
    while (!patient3.setGender(gender));
    
    do {
        cout << "Возраст: ";
        cin >> age;
    }
    while (!patient3.setAge(age));

    patient1.printInfo();
    patient2.printInfo();
    patient3.printInfo();

    return 0;
}