#include "patient2.h"
#include <iostream>

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");

    Patient patient1 = Patient();
    Patient patient2 = Patient("Михаил");
    Patient patient3 = Patient("Антон", "Ж", 30);
    Patient patient4 = Patient(patient3);

    patient1.printInfo();
    patient2.printInfo();
    patient3.printInfo();
    patient4.printInfo();

    return 0;
}