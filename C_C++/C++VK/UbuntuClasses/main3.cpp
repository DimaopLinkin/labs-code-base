#include <iostream>

using namespace std;

struct Song {
    string author;
    string name;
    int year;
};

struct Node {
    Song data;
    Node *next;
};
Node* addToHead(Node *list, Song *data){
    if (list != NULL) {
        Node *current = list;
        Node *newelem = new Node;
        newelem->data = *data;
        newelem->next = current;
        return newelem;
    }
    else {
        list = new Node;
        list->data = *data;
        list->next = NULL;
    }
    return list;
}
Node* add(Node *list, Song *data){
    if (list != NULL) {
        Node *current = list;
        while(current->next) {
            current = current->next;
        }
        Node *newelem = new Node;
        newelem->data = *data;
        newelem->next = NULL;
        current->next = newelem;
    }
    else {
        list = new Node;
        list->data = *data;
        list->next = NULL;
    }
    return list;
}
void findNode(Node *list, string author){
    if (list != NULL) {
        Node *current = list;
        while (current != NULL) {
            if (current->data.author == author){
                cout << "Author - " << current->data.author 
                    << ". Name - " << current->data.name 
                    << ". Year - " << current->data.year << endl;
            }
            if (current != NULL) {
                current = current->next;
            }
        }
    }
}
Node* deleteNode(Node *list, string name){
    if (list != NULL) {
        Node *prev = NULL;
        Node *current = list;
        while(current != NULL){
            if (current->data.name == name){
                if (prev != NULL){
                    prev->next = current->next;
                    delete current;
                    current = prev;
                }
                else {
                    list = current->next;
                    delete current;
                    current = list;
                }
            }
            if (current != NULL) {
                prev = current;
                current = current->next;
            }
        }
    }
    return list;
}
int listCount(Node *list){
    int count = 0;
    if (list != NULL){
        Node *current = list;
        while (current != NULL){
            current = current->next;
            count++;
        }
    }
    return count;
}
void print(Node *list){
    if (list != NULL){
        Node *current = list;
        int count = 1;
        while (current != NULL) {
            cout << count 
                << ". Author - " << current->data.author 
                << ". Name - " << current->data.name 
                << ". Year - " << current->data.year << endl;
            current = current->next;
            count++;
        }
    }
    else {
        cout << "List is empty" << endl;
    }
}
Node* deleteList(Node *list){
    if (list != NULL){
        deleteList(list->next);
        delete(list);
        list = NULL;
    }
    return list;
}

int main() {
    setlocale(LC_ALL, "RUS");

    int n = 0;
    Node *list = NULL;
    string author;
    string name;
    Song *data;
    while (n != 7) {
        cout << "0. Add to head \n";
        cout << "1. Add to list \n";
        cout << "2. Remove element by name \n";
        cout << "3. Find element by author \n";
        cout << "4. Print list \n";
        cout << "5. Return list count \n";
        cout << "6. Delete list \n";
        cout << "7. Exit \n";
        cin >> n;
        cin.ignore(256, '\n');
        switch (n) {
        case 0:
            data = new Song;
            cout << "Enter author ";
            getline(cin, data->author);
            cout << "Enter name ";
            getline(cin, data->name);
            cout << "Enter year ";
            cin >> data->year;
            list = addToHead(list, data);
            break;
        case 1:
            data = new Song;
            cout << "Enter author ";
            getline(cin, data->author);
            cout << "Enter name ";
            getline(cin, data->name);
            cout << "Enter year ";
            cin >> data->year;
            list = add(list, data);
            break;
        case 2:
            cout << "Enter name ";
            getline(cin, data->name);
            list = deleteNode(list, name);
            break;
        case 3:
            cout << "Enter author ";
            getline(cin, data->author);
            findNode(list, author);
            break;
        case 4:
            print(list);
            break;
        case 5:
            cout << "List count: " << listCount(list) << endl;
            break;
        case 6:
            list = deleteList(list);
            break;
        default:
            break;
        }
    }
    return 0;
}