#include "xyz4.h"
#include <iostream>

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    Xyz sample1 = Xyz();
    Xyz sample2 = Xyz(2, 4, 7);
    Xyz sample3 = Xyz(1,6,9);

    try {
        sample1.count();
    } catch (int a) {
        cout << "ОШИБКА НОМЕР " << a << endl;
    }
    try {
        sample2.count();
    } catch (int a) {
        cout << "ОШИБКА НОМЕР " << a << endl;
    }
    try {
        sample3.count();
    } catch (int a) {
        cout << "ОШИБКА НОМЕР " << a << endl;
    }
    sample1.print();
    sample2.print();
    sample3.print();

    sample1.setA(9);
    sample1.setB(2);
    sample1.setC(3);
    sample1.print();
    try {
        sample1.count();
    } catch (int a) {
        cout << "ОШИБКА НОМЕР " << a << endl;
    }
    

    return 0;
}