#include <iostream>

class Patient {
    public:
        Patient() {
            lastName = "";
            gender = "";
            age = 0;            
        }
        Patient(std::string lastName): lastName(lastName) {
            gender = "";
            age = 0;
        }
        Patient(std::string lastName, std::string gender, int age) :
        lastName(lastName), gender(gender), age(age)
        {}

        Patient(const Patient &patient): lastName(patient.lastName), gender(patient.gender), age(patient.age) 
        {}
        bool setLastName(std::string lastName) {
            if (lastName != "") {
                (*this).lastName = lastName;
                return true;
            }
            else return false;
        }
        bool setGender(std::string gender) {
            if (gender == "м" || gender == "ж" || 
                gender == "М" || gender == "Ж") {
                (*this).gender = gender;
                return true;
            }
            else return false;
        }
        bool setAge(int age) {
            if (age < 0 || age > 100) {
                return false;
            }
            this->age = age;
            return true;
        }
        std::string getLastName() {
            return lastName;
        }
        std::string getGender() {
            return gender;
        }
        int getAge() {
            return age;
        }
        void printInfo() {
            std::cout << "Фамилия: " << getLastName() 
            << "\nПол: " << getGender() 
            << "\nВозраст: " << getAge() << std::endl;
        }
    private:
        std::string lastName;
        std::string gender;
        int age;
};