#include <math.h>
#include <iostream>

class Xyz {
    public:
        Xyz(){
            a = 0;
            b = 0;
            c = 0;
        }
        Xyz(double a, double b, double c) : a(a), b(b), c(c) {}
        void setA(double a){
            (*this).a = a;
        }
        void setB(double b){
            (*this).b = b;
        }
        void setC(double c){
            (*this).c = c;
        }
        void count(){
            d = discr();
            if (d < 0) {
                state = "У уравнения не найдено корней";
                throw -1;
            }
            if (d == 0) {
                x = - b / 2 * a;
                state = "У уравнения найдено одно решение";
                std::cout << "x = " << x << std::endl;
            }
            if (d > 0) {
                x = - (b + sqrt(d)) / (2 * a);
                x1 = - (b - sqrt(d)) / (2 * a);
                state = "У уравнения найдено два решения";
                std::cout << "x1 = " << x << "; x2 = " << x1 << std::endl;
            }
        }
        void print(){
            std::cout << a << "^2";
            if (b > 0) std::cout << "+";
            std::cout << b << "x";
            if (c > 0) std::cout << "+";
            std::cout << c << "=0\n";
        }
    private:
    double a;
    double b;
    double c;
    double x;
    double x1;
    double d;
    std::string state;
    double discr(){
            return b*b-4*a*c;
        }
};