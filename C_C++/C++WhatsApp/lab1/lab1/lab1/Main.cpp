#include <iostream>
#include "Money.h"
#include "Person.h"

using namespace std;

int main() {
	// ����������� �������� ����� � �������
	setlocale(LC_ALL, "RUS");
	// �������� ����������� ������ Money � �������������� ����������
	Money money1 = Money(100, 50);
	Money money2 = Money(98, 51);
	// ����� ostream ��������� ����������� ������
	cout << money1 - money2 << endl;
	// ����� ���������� � ������������
	money2 = money2 + Money(2, 0);
	// �����
	cout << money1 << " " << money2 << " " << money1 + Money(25, 92) << endl;
	// ������������ �������
	cout << money1 << endl;
	money1 = money1 / 2;
	cout << money1 << endl;
	// ������������ ���������
	money1 = money1 * 4;
	cout << money1 << endl;
	// ������������ ������� � ��������� ������
	money1 = money1 / 5;
	cout << money1 << endl;
	Person person1 = Person(100, 5, "�����");
	cout << person1 << endl;
	person1.toUpBalance();
	cout << person1 << endl;
	person1.toUpBalance(50, 20);
	cout << person1 << endl;
	system("PAUSE");
	return 0;
}