#include "Person.h"

// ������ �����
void Person::setName(string name)
{
	this->name = name;
}

// ������ �����
string Person::getName()
{
	return name;
}

// ������ �������
float Person::getBalance()
{
	return balance;
}

// ���������� ������� � ����������
void Person::toUpBalance()
{
	int64_t rubles;
	uint8_t penny;
	int pennyHelper;
	cout << "�����: ";
	cin >> rubles;
	cout << "�������: ";
	cin >> pennyHelper;
	// ��� ��� uint8_t - ��� ����������� ��� unsigned char,
	// �� ��� ����������� ����� ������ ����� cin ���������
	// ��� ������ ����� ��������������� int ����������
	// ����� cin ����� �������� � ���������� ��� ������� ��������� �������
	penny = pennyHelper;
	this->rubles += rubles;
	this->penny += penny;
	this->rubles += this->penny / 100;
	this->penny = this->penny % 100;
	balance = this->rubles;
	balance += (float)this->penny / 100;
}

// ���������� ������� ����� ���������
void Person::toUpBalance(int64_t rubles, uint8_t penny)
{
	this->rubles += rubles;
	this->penny += penny;
	this->rubles += this->penny / 100;
	this->penny = this->penny % 100;
	balance = this->rubles;
	balance += (float)this->penny / 100;
}

// ���������� ������
ostream& operator<<(ostream& out, const Person& person)
{
	out << "���: " << person.name << endl << "������: " << person.balance;

	return out;
}
