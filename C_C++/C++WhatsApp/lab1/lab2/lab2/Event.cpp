#pragma once

#include <functional>
#include <map>
#include <typeinfo>
#include <iostream>

using namespace std;

struct Event {
	virtual ~Event() {}
};
struct CreditEvent : Event {
	float sum;
	CreditEvent(float sum) : sum(sum) {}
};
struct SalaryEvent : Event {
	float sum;
	SalaryEvent(float sum) : sum(sum) {}
};
struct SubscribeEvent : Event {
	float sum;
	SubscribeEvent(float sum) : sum(sum) {}
};

typedef multimap<const type_info*,
	const function<void(const Event&)>> EventMap;

class event {
private:
	static EventMap eventMap;

public:
	template<typename EventWanted>
	static void on(const function<void(const Event&)>& fn) {
		event::eventMap.emplace(&typeid(EventWanted), fn);
	}

	static void emit(const Event& event) {
		auto range = eventMap.equal_range(&typeid(event));
		for (auto it = range.first; it != range.second; ++it)
			it->second(event);

	}
};

EventMap event::eventMap;