#include <iostream>
#include "Money.h"
#include "Person.h"
#include "Event.cpp"

using namespace std;

int main() {
	setlocale(LC_ALL, "RUS");
	Person person1 = Person(100, 5, "�����");
	person1.toUpBalance(50000, 50);

	// ����� ��� �������� � person1
	event::emit(CreditEvent(3000));

	// �������� �� ������ �� �������
	person1.subscribeEvent(0);

	// ����� � ���������
	event::emit(CreditEvent(2500));
	system("PAUSE");
	return 0;
}