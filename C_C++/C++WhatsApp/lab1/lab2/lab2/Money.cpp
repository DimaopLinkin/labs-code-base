#include "Money.h"
#include <iostream>
#include <math.h>
using namespace std;

// ����������� �� ���������
Money::Money()
{
	rubles = 0;
	penny = 0;
}

// ����������� � �����������
Money::Money(int64_t rubles, uint8_t penny)
{
	this->rubles = rubles;
	this->penny = penny;
}

// ������ ������
void Money::set_rubles(int64_t rubles)
{
	this->rubles = rubles;
}

// ������ ������
void Money::set_penny(uint8_t penny)
{
	this->penny = penny;
}

// ������ ������
int64_t Money::get_rubles()
{
	return rubles;
}

// ������ ������
uint8_t Money::get_penny()
{
	return penny;
}

// ���������� ��������
Money operator+(const Money& left1, const Money& right1)
{
	Money left = left1;
	Money right = right1;
	left.rubles += right.rubles;
	left.penny += right.penny;
	left.rubles += left.penny / 100;
	left.penny = left.penny % 100;
	return left;
}

// ���������� ���������
Money operator-(const Money& left1, const Money& right1)
{
	Money left = left1;
	Money right = right1;
	left.rubles -= right.rubles;
	if (left.penny < right.penny) {
		left.penny = left.penny + 100 - right.penny;
		left.rubles--;
	}
	else {
		left.penny -= right.penny;
	}
	return left;
}

// ���������� ���������
Money operator*(const Money& left1, int right)
{
	Money left = left1;
	left.rubles *= right;
	left.penny *= right;
	left.rubles += left.penny / 100;
	left.penny = left.penny % 100;
	return left;
}

// ���������� �������
Money operator/(const Money& left1, int right)
{
	Money left = left1;
	left.rubles /= right;
	left.penny /= right;
	left.penny += left1.rubles % right * 100 / right;
	left.rubles += left.penny / 100;
	left.penny = left.penny % 100;
	return left;
}

// ���������� ���������
bool operator==(const Money& left, const Money& right)
{
	if (left.rubles == right.rubles &&
		left.penny == right.penny) {
		return true;
	}
	else return false;
}

// ���������� ������
ostream& operator<<(ostream& out, const Money& money)
{
	out << money.rubles << "," << (int)money.penny;

	return out;
}
