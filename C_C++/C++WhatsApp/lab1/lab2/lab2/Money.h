#pragma once

#include <cstdint>
#include <iostream>

using namespace std;

class Money {
protected:
	int64_t rubles;	// �����
	uint8_t penny;	// �������
public:
	Money();	// ����������� �� ���������
	Money(int64_t rubles, uint8_t penny);	// �����������
	void set_rubles(int64_t rubles);	// ���������� �����
	void set_penny(uint8_t penny);		// ���������� �������
	int64_t get_rubles();	// �������� �����
	uint8_t get_penny();	// �������� �������
	// ���������� ���������� + - * / == <<
	friend Money operator+(const Money& left, const Money& right);
	friend Money operator-(const Money& left, const Money& right);
	friend Money operator*(const Money& left, int right);
	friend Money operator/(const Money& left, int right);
	friend bool operator==(const Money& left, const Money& right);
	friend ostream& operator<<(ostream& out, const Money& money);
};