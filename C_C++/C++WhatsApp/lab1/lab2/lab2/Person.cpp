#include "Person.h"
#include "Event.cpp"

// ������ �����
void Person::setName(string name)
{
	this->name = name;
}

// ������ �����
string Person::getName()
{
	return name;
}

// ������ �������
float Person::getBalance()
{
	return balance;
}

// ���������� ������� � ����������
void Person::toUpBalance()
{
	int64_t rubles;
	uint8_t penny;
	int pennyHelper;
	cout << "�����: ";
	cin >> rubles;
	cout << "�������: ";
	cin >> pennyHelper;
	// ��� ��� uint8_t - ��� ����������� ��� unsigned char,
	// �� ��� ����������� ����� ������ ����� cin ���������
	// ��� ������ ����� ��������������� int ����������
	// ����� cin ����� �������� � ���������� ��� ������� ��������� �������
	penny = pennyHelper;
	this->rubles += rubles;
	this->penny += penny;
	this->rubles += this->penny / 100;
	this->penny = this->penny % 100;
	balance = this->rubles;
	balance += (float)this->penny / 100;
}

// ���������� ������� ����� ���������
void Person::toUpBalance(int64_t rubles, uint8_t penny)
{
	this->rubles += rubles;
	this->penny += penny;
	this->rubles += this->penny / 100;
	this->penny = this->penny % 100;
	balance = this->rubles;
	balance += (float)this->penny / 100;
}

void Person::debitBalance(int64_t rubles, uint8_t penny)
{
	this->rubles -= rubles;
	if (this->penny < penny) {
		this->rubles--;
		this->penny = this->penny + 100 - penny;
	}
	else {
		this->penny -= penny;
	}
}

void Person::subscribeEvent(int choose)
{
	switch (choose) {
	case 0: event::on<CreditEvent>([this](const Event& e) {
		int64_t rubles = floor(static_cast<const CreditEvent&>(e).sum);
		uint8_t penny = (int)static_cast<const CreditEvent&>(e).sum * 100 % 100;
		debitBalance(rubles, penny);
		cout << "����� �� ������� � ������� " << static_cast<const CreditEvent&>(e).sum << " �����" << endl;
		}); break;
	case 1: event::on<SalaryEvent>([this](const Event& e) {
		int64_t rubles = floor(static_cast<const CreditEvent&>(e).sum);
		uint8_t penny = (int)static_cast<const CreditEvent&>(e).sum * 100 % 100;
		toUpBalance(rubles, penny);
		cout << "��������� �������� � ������� " << static_cast<const CreditEvent&>(e).sum << endl;
		}); break;
	case 2: event::on<SubscribeEvent>([this](const Event& e) {
		int64_t rubles = floor(static_cast<const CreditEvent&>(e).sum);
		uint8_t penny = (int)static_cast<const CreditEvent&>(e).sum * 100 % 100;
		debitBalance(rubles, penny);
		cout << "����� �� �������� � ������� " << static_cast<const CreditEvent&>(e).sum << " �����" << endl;
		}); break;
	default: break;
	}
}

// ���������� ������
ostream& operator<<(ostream& out, const Person& person)
{
	out << "���: " << person.name << endl << "������: " << person.balance;

	return out;
}
