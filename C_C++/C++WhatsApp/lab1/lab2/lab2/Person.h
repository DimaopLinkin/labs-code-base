#pragma once
#include "Money.h"
using namespace std;

class Person : public Money {
private:
	float balance;	// ������
	string name;	// ���
public:
	// ����������� �� ���������
	Person() : Money(0, 0) { balance = 0; name = "Undifined"; };
	// ����������� � �����������
	Person(int64_t rubles,
		uint8_t penny,
		string name
	) : Money(rubles, penny) {
		this->name = name;
		balance = rubles + (float)penny / 100;
	};
	void setName(string name);	// ������ �����
	string getName();			// ������ �����
	float getBalance();			// ������ �������
	void toUpBalance();	// ��������� ������ � ����������
	void toUpBalance(int64_t rubles, uint8_t penny);	// ��������� ������ ����� ���������
	void debitBalance(int64_t rubles, uint8_t penny);	// ������� ������ �� �����
	void subscribeEvent(int choose);
	friend ostream& operator<<(ostream& out, const Person& person);
};