#include <iostream>
#include "stdlib.h"
using namespace std;
bool check_for_win(const char(&arr)[4][4]) {
	for (int i = 1; i < 4; i++) {
		if (arr[i][1] == arr[i][2] && arr[i][1] == arr[i][3] && arr[i][1] != 127) {
			if (arr[i][1] == 'X') {
				cout << endl << "�� ��������!";
			}
			else {
				cout << endl << "�� ��������";
			}
			return true;
		}
		if (arr[1][i] == arr[2][i] && arr[1][i] == arr[3][i] && arr[1][i] != 127) {
			if (arr[1][i] == 'X') {
				cout << endl << "�� ��������!";
			}
			else {
				cout << endl << "�� ��������";
			}
			return true;
		}
	}
	if (arr[1][1] == arr[2][2] && arr[1][1] == arr[3][3] && arr[1][1] != 127) {
		if (arr[1][1] == 'X') {
			cout << endl << "�� ��������!";
		}
		else {
			cout << endl << "�� ��������";
		}
		return true;
	}
	if (arr[1][3] == arr[2][2] && arr[1][1] == arr[3][1] && arr[1][3] != 127) {
		if (arr[1][3] == 'X') {
			cout << endl << "�� ��������!";
		}
		else {
			cout << endl << "�� ��������";
		}
		return true;
	}
	return false;
}
void pole_output(const char(&arr)[4][4]) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
int main() {
	setlocale(LC_ALL, "rus");
	system("color B");
	char pole[4][4];
	int bi, bj;
	bool win = false;
	bool eot;
	int count = 9;
	pole[0][0] = ' ';
	pole[0][1] = '1';
	pole[0][2] = '2';
	pole[0][3] = '3';
	pole[1][0] = '1';
	pole[2][0] = '2';
	pole[3][0] = '3';
	for (int i = 1; i < 4; i++) {
		for (int j = 1; j < 4; j++) {
			pole[i][j] = 127;
		}
	}
	pole_output(pole);
	int pi, pj;
	cout << "��� ������� ����, ������ ������ ����� ���� ������� (i,j), ��� i � j ��� ����� ������ � ������� ��������������." << endl;
	cout << "������ ��� �� ����, ������� ���������� ������ ����: ";

	while (!win) {
		eot = true;
		//input
		{
			cin >> pi >> pj;
			while (pi > 3 || pi < 1 || pj > 3 || pj < 1 || (pole[pi][pj] != 127)) {
				cout << "������������ ����������, ������� ������: ";
				cin >> pi >> pj;
			}
			pole[pi][pj] = 'X';
		}

		//check for win
		{

			for (int i = 1; i < 4; i++) {
				// rows
				if (pole[i][1] == pole[i][2] && pole[i][3] == 127 && pole[i][1] == 'O' && eot) {
					bi = i;
					bj = 3;
					eot = false;
					break;
				}
				if (pole[i][1] == pole[i][3] && pole[i][3] == 127 && pole[i][1] == 'O' && eot) {
					bi = i;
					bj = 2;
					eot = false;
					break;
				}
				if (pole[i][2] == pole[i][3] && pole[i][1] == 127 && pole[i][2] == 'O' && eot) {
					bi = i;
					bj = 1;
					eot = false;
					break;
				}
				// columns
				if (pole[1][i] == pole[2][i] && pole[3][i] == 127 && pole[1][i] == 'O' && eot) {
					bi = 3;
					bj = i;
					eot = false;
					break;
				}
				if (pole[1][i] == pole[3][i] && pole[2][i] == 127 && pole[1][i] == 'O' && eot) {
					bi = 2;
					bj = i;
					eot = false;
					break;
				}
				if (pole[2][i] == pole[3][i] && pole[1][i] == 127 && pole[2][i] == 'O' && eot) {
					bi = 1;
					bj = i;
					eot = false;
					break;
				}
			}
			//diagonali
			if (pole[1][1] == pole[2][2] && pole[3][3] == 127 && pole[1][1] == 'O' && eot) {
				bi = 3;
				bj = 3;
				eot = false;
			}
			if (pole[2][2] == pole[3][3] && pole[2][2] == 127 && pole[2][2] == 'O' && eot) {
				bi = 2;
				bj = 2;
				eot = false;
			}
			if (pole[2][2] == pole[3][3] && pole[1][1] == 127 && pole[2][2] == 'O' && eot) {
				bi = 1;
				bj = 1;
				eot = false;
			}
			if (pole[1][3] == pole[2][2] && pole[3][1] == 127 && pole[2][2] == 'O' && eot) {
				bi = 3;
				bj = 1;
				eot = false;
			}
			if (pole[3][1] == pole[2][2] && pole[1][3] == 127 && pole[2][2] == 'O' && eot) {
				bi = 1;
				bj = 3;
				eot = false;
			}
			if (pole[1][3] == pole[3][1] && pole[2][2] == 127 && pole[1][3] == 'O' && eot) {
				bi = 2;
				bj = 2;
				eot = false;
			}

		}

		//check for destroy player win
		{
			if (eot) {
				// rows
				for (int i = 1; i < 4; i++) {
					if (pole[i][1] == pole[i][2] && pole[i][3] == 127 && pole[i][1] == 'X' && eot) {
						bi = i;
						bj = 3;
						eot = false;
						break;
					}
					if (pole[i][1] == pole[i][3] && pole[i][3] == 127 && pole[i][1] == 'X' && eot) {
						bi = i;
						bj = 2;
						eot = false;
						break;
					}
					if (pole[i][2] == pole[i][3] && pole[i][1] == 127 && pole[i][2] == 'X' && eot) {
						bi = i;
						bj = 1;
						eot = false;
						break;
					}
					// columns
					if (pole[1][i] == pole[2][i] && pole[3][i] == 127 && pole[1][i] == 'X' && eot) {
						bi = 3;
						bj = i;
						eot = false;
						break;
					}
					if (pole[1][i] == pole[3][i] && pole[2][i] == 127 && pole[1][i] == 'X' && eot) {
						bi = 2;
						bj = i;
						eot = false;
						break;
					}
					if (pole[2][i] == pole[3][i] && pole[1][i] == 127 && pole[2][i] == 'X' && eot) {
						bi = 1;
						bj = i;
						eot = false;
						break;
					}
				}
				//diagonali
				if (pole[1][1] == pole[2][2] && pole[3][3] == 127 && pole[1][1] == 'X' && eot) {
					bi = 3;
					bj = 3;
					eot = false;
				}
				if (pole[2][2] == pole[3][3] && pole[1][1] == 127 && pole[2][2] == 'X' && eot) {
					bi = 1;
					bj = 1;
					eot = false;
				}
				if (pole[1][1] == pole[3][3] && pole[2][2] == 127 && pole[1][1] == 'X' && eot) {
					bi = 2;
					bj = 2;
					eot = false;
				}
				if (pole[1][3] == pole[2][2] && pole[3][1] == 127 && pole[2][2] == 'X' && eot) {
					bi = 3;
					bj = 1;
					eot = false;
				}
				if (pole[3][1] == pole[2][2] && pole[1][3] == 127 && pole[2][2] == 'X' && eot) {
					bi = 1;
					bj = 3;
					eot = false;
				}
				if (pole[1][3] == pole[3][1] && pole[2][2] == 127 && pole[1][3] == 'X' && eot) {
					bi = 2;
					bj = 2;
					eot = false;
				}
			}
		}
		//randgen
		{
			if (eot) {
				bi = rand() % 3 + 1;
				bj = rand() % 3 + 1;
				while (bi > 3 || bi < 1 || bj > 3 || bj < 1 || (pole[bi][bj] != 127)) {
					bi = rand() % 3 + 1;
					bj = rand() % 3 + 1;
				}
			}
		}

		//choosing bot's turn
		pole[bi][bj] = 'O';

		pole_output(pole);

		win = check_for_win(pole);

		count -= 2;
		//check for tie
		if (count == 1 && !win) {
			cout << endl << "�����";
			win = true;
		}
	}
	system("PAUSE");
}
