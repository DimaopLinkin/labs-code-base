#pragma once
#include <iostream>

using namespace std;

class Array {
protected:
	int* mas;
	int size;
	Array();
	Array(int* mas, int size);
	Array(Array& right);
public:
	virtual void slojenie(Array& right) = 0;
	virtual void foreach() = 0;
	virtual ostream& print(ostream& os = cout) const = 0;
	virtual istream& scan(istream& os = cin) const = 0;
};

class AndArray : public Array {

public:
	AndArray() : Array() {};
	AndArray(int* mas, int size) : Array(mas, size) {};
	AndArray(AndArray& right) : Array(right) {};
	~AndArray();
	void slojenie(Array& right);
};

class OrArray : public Array {

public:
	OrArray() : Array() {};
	OrArray(int* mas, int size) : Array(mas, size) {};
	OrArray(OrArray& right) : Array(right) {};
	~OrArray();
};