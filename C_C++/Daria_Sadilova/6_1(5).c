#include "stdio.h"
#include "locale.h"
#include "math.h"

int main() {
    setlocale(LC_ALL, "RUS");
    int n;  // Размер массива
    int doing;  // Зацикленность программы
    double *mas; // Массив
    double pi = 3.14;// Число Пи

    // Зацикленная программа
    do {
        // Ввод размера массива
        do {
            printf("\nВведите размер массива от 1 до 15 = ");
            scanf("%d",&n); 
        }
        while (n < 1 || n > 15);
        
        // Выделение памяти для массива
        mas = (double *)malloc(n * sizeof(double));

        printf("Исходный массив:\n");
        // Заполнение массива и вывод его исходного состояния
        for (int i = 0; i < n; i++) {
            mas[i] = (i+1)*sin(pi+i+1);
            printf("%.3lf ", mas[i]);
        }

        printf("\nИзменённый массив:\n");
        // Замена положительные элементы 
        // произведением предшествующих положительных
        // и вывод его нового состояния
        for (int i = 0; i < n; i++) {
            if (mas[i] > 0) {
                for (int j = 0; j < i; j++) {
                    if (mas[j] > 0) {
                        mas[i] *= mas[j];
                    }
                }
            }
            printf("%.3lf ", mas[i]);
        }

        printf("\nПовторить программу? 1 - да, 0 - нет ");
        scanf("%d",&doing);
    }
    while (doing != 0);

    free(mas);
    
    system("PAUSE");
    return 0;
}