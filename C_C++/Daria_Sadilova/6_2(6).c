#include "stdio.h"
#include "locale.h"
#include "cstdlib"

int main() {
    setlocale(LC_ALL, "RUS");
    int n;      // Размер массива
    int newN;   // Размер нового массива
    int doing;  // Зацикленность программы
    int *mas;   // Массив
    int *newMas;// Новый массив
    int k;      // Количество заменяемых элементов
    int elem;   // Вспомогательный элемент

    // Зацикленная программа
    do {
        // Ввод размера массива
        do {
            printf("\nВведите размер массива от 1 до 10 = ");
            scanf("%d",&n); 
        }
        while (n < 1 || n > 10);
        
        // Выделение памяти для массива
        mas = (int *)malloc(n * sizeof(int));

        // Заполнение массива
        printf("Заполните массив\n");
        for (int i = 0; i < n; i++) {
            printf("%d - ", i+1);
            scanf("%d",&mas[i]);
        }
        printf("Исходный массив:\n");
        for (int i = 0; i < n; i++) {
            printf("%d ", mas[i]);
        }

        // Ввод количества элементов, которые перенести в начало
        do {
            printf("\nВведите k от 0 до %d ", n);
            scanf("%d", &k);
        }
        while (k < 0 || k > n);

        // Перенос элементов и вывод результата
        for (int i = 0; i < k; i++) {
            elem = mas[n-1];
            for (int j = n-1; j > 0; j--) {
                mas[j] = mas[j-1];
            }
            mas[0] = elem;
        }

        // Вывод изменённого массива
        // Рассчёт количества элементов для нового массива
        newN = 0;
        printf("Массив после переноса:\n");
        for (int i = 0; i < n; i++) {
            printf("%d ", mas[i]);
            if (mas[i] > 9 && mas[i] < 100) {
                newN++;
            }
        }

        // Выделение памяти для массива
        newMas = (int *)malloc(newN * sizeof(int));

        // Заполнение массива двузначными числами
        int z = 0;
        for (int i = 0; i < n; i++) {
            if (mas[i] > 9 && mas[i] < 100) {
                newMas[z] = mas[i];
                z++;
            }
        }
        printf("\nНовый массив:\n");
        for (int i = 0; i < newN; i++) {
            printf("%d ", newMas[i]);
        }

        // Сортировка нового массива пузырьком(наоборот)
        for(int i = 0 ; i < newN - 1; i++) { 
            // сравниваем два соседних элемента.
            for (int j = (newN - 1); j > i; j--) {  
                if(newMas[j - 1] < newMas[j]) {           
                    // если они идут в неправильном порядке, то  
                    //  меняем их местами. 
                    int tmp = newMas[j - 1];
                    newMas[j - 1] = newMas[j] ;
                    newMas[j] = tmp; 
                }
            }
        }
        printf("\nОтсортированный массив:\n");
        for (int i = 0; i < newN; i++) {
            printf("%d ", newMas[i]);
        }

        printf("\nПовторить программу? 1 - да, 0 - нет ");
        scanf("%d",&doing);
    }
    while (doing != 0);

    free(mas);
    free(newMas);

    system("PAUSE");
    return 0;
}