#include "stdio.h"
#include "locale.h"
#include "math.h"

int main() {
    setlocale(LC_ALL, "RUS");
    int mas[10][10];    // Массив
    int n, m;   // Количество строк и столбцов
    int result[10][2];

    // Ввод размеров матрицы
    do {
        printf("Введите размеры матрицы от 2 до 10\nn: ");
        scanf("%d", &n);
        printf("m: ");
        scanf("%d", &m);
    }
    while (n < 2 || n > 10 || m < 2 || m > 10);

    // Заполнение матрицы
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("[%d][%d]: ", i+1, j+1);
            scanf("%d", &mas[i][j]);
        } 
    }

    // Вывод матрицы
    printf("   Исходная матрица\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%4d", mas[i][j]);
        }
        printf("\n");
    }

    // Получение матрицы сумм и произведений
    for (int i = 0; i < n; i++) {
        result[i][1] = 1;
        for (int j = 0; j < m; j++) {
            result[i][0]+=mas[i][j];
            result[i][1]*=mas[i][j];
        }        
    }

    // Вывод матрицы сумм и произведений
    printf("   Полученная матрица\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%8d", result[i][j]);
        }
        printf("\n");
    }

    // Сортировка матрицы сумм и произведений
    for(int i = 0 ; i < n - 1; i++) { 
        // сравниваем два соседних элемента.
        for (int j = (n - 1); j > i; j--) {  
            if(result[j - 1][1] > result[j][1]) {           
                // если они идут в неправильном порядке, то  
                // меняем их местами. 
                int tmp = result[j - 1][0];
                result[j - 1][0] = result[j][0];
                result[j][0] = tmp; 
                tmp = result[j - 1][1];
                result[j - 1][1] = result[j][1] ;
                result[j][1] = tmp;
            }
        }
    }

    // Вывод отсортированной матрицы сумм и произведений
    printf("   Отсортированная матрица\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%8d", result[i][j]);
        }
        printf("\n");
    }

    system("PAUSE");
    return 0;
}