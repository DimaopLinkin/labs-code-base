#include "stdio.h"
#include "locale.h"
#include "math.h"

int main() {
    setlocale(LC_ALL, "RUS");
    int mas[10][10];
    int n, m;
    int number;

    // Ввод размеров матрицы
    do {
        printf("Введите размеры матрицы от 2 до 8\nn: ");
        scanf("%d", &n);
        printf("m: ");
        scanf("%d", &m);
    }
    while (n < 2 || n > 8 || m < 2 || m > 8);

    // Заполнение массива
    for (int i = 0; i < n; i++) {
        int number = 1;
        for (int j = i; j < m; j += 2) {
            mas[i][j] = number;
            number++;
        }
        number = 1;
        for (int j = i; j > -1; j -= 2) {
            mas[i][j] = number;
            number++;
        }
    }

    // Вывод матрицы
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (j > m-3) {
                if (i > n-3) {
                    printf("  ");
                }
                else {
                    printf(" .");
                }
                continue;
            }
            if (i > n-3) {
                if (j < 2 || j > m-3) {
                    printf("  ");
                }
                else {
                    printf(" .");
                }
                continue;
            }
            printf("%2d", mas[i][j]);
        }
        printf("\n");
    }

    system("PAUSE");
    return 0;
}