#include "stdio.h"
#include "locale.h"
#include <cstdlib>

int main() {
    setlocale(LC_ALL, "RUS");
    int n, m;   // Размеры матрицы

    // Ввод размеров матрицы
    do {
        printf("Введите количество строк n от 2 до 6: ");
        scanf("%d", &n);

        printf("Введите количество столбцов m от 2 до 6: ");
        scanf("%d", &m);
    }
    while (n < 2 || n > 6 || m < 2 || m > 6);
    
    // Выделение памяти
    int **a = (int **)calloc(n, sizeof(int *));
    for(int i = 0; i < n; i++) {
        *(a+i) = (int *)calloc(m, sizeof(int));
    }

    // Заполнение элементов массива
    printf("Заполните матрицу числами:\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++){
            printf("a[%d][%d]: ", i+1, j+1);
            scanf("%d", *(&*a+i)+j);
        }
    }

    // Вывод исходной матрицы
    printf("Исходная матрица:\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("%3d", *(*(a+i)+j));
        }
        printf("\n");
    }

    // Вывод адресов исходной матрицы
    printf("Адреса исходной матрицы:\n");
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            printf("a[%d][%d]: 0x%x\n", i+1, j+1, ((a+i)+j));
        }
    }

    // Выделение памяти для одномерного массива максимумов
    int *b = (int *)calloc(m, sizeof(int));

    // Заполнение массива максимумов
    // Каждый элемент - максимум из соответствующего столбца матрицы a
    for (int j = 0; j < m; j++) {
        for (int i = 0; i < n; i++) {
            if (*(b+j) < *(*(a+i)+j)) {
                *(b+j) = *(*(a+i)+j);
            }
        }
    }

    // Вывод массива максимумов
    printf("Массив максимумов\n");
    for (int j = 0; j < m; j++) {
        printf("%3d", *(b+j));
    }

    // Вывод адресов массива максимумов
    printf("\nАдреса массива максимумов\n");
    for (int j = 0; j < m; j++) {
        printf("b[%d]: 0x%x\n", j+1, (b+j));
    }

    // Очистка памяти от матрицы a
    for(int i = 0; i < n; i++) {
        free(*(a+i));
    }
    free(a);

    // Выделение памяти под сумму и произведение элементов массива
    int *sum = (int *)calloc(1, sizeof(int));
    int *comp = (int *)calloc(1, sizeof(int));

    // *sum благодаря использованию calloc сразу заполнен нулями
    // Для корректного вычисления произведения необходимо записать в него единицу
    *comp = 1;

    // Вычисление суммы и произведения элементов массива максимумов
    for (int j = 0; j < m; j++) {
        *sum += *(b+j);
        *comp *= *(b+j);
    }

    // Вывод суммы и произведения
    printf("Сумма максимумов:\nsum: %d(0x%x)\n", *sum, sum);
    printf("Произведение максимумов:\ncomp: %d(0x%x)\n", *comp, comp);

    // Очистка памяти от массива b
    free(b);
    // Вывод очищенного массива b
    printf("Очищенный массив b\n");
    for (int j = 0; j < m; j++) {
        printf("b[%d]: %d(0x%x)\n", j+1, *(b+j), (b+j));
    }

    // Очистка памяти отсуммы и произведения
    free(sum);
    free(comp);

    // Вывод очищенных суммы и произведения
    printf("Очищенная сумма\nsum: %d(0x%x)\n", *sum, sum);
    printf("Очищенное произведение\ncomp: %d(0x%x)\n", *comp, comp);

    system("PAUSE");
    return 0;
}