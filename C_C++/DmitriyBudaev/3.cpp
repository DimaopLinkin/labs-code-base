#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
	ifstream fin;	// Поток файлового ввода
	try {
		fin.open("input.txt");	// Попытка открыть файл. Если файл не открылся или отсутствует, переход в блок catch

		unsigned long int max_length = 0;	// Максимальная длина последовательности
		short int IBKB_count = 0;	// Количество подстрок IBKB
		string letters;	// Прочитанная последовательность символов из файла
		string IBKB = "";	// Проверяемая подстрока
		int first_IBKB_index = -1;
		int second_IBKB_index = -1;
		int third_IBKB_index = -1;

		fin >> letters;

		/* 	РАЗБОР ЛОГИКИ
				ПРИМЕР 1     ....     ....       ...
				БЫЛА СТРОКА "IBKBFDSLFIBKBFDSJIFDIBK"
				ДОПУСТИМ, СЛЕДУЮЩИЙ СИМВОЛ В ИСХОДНОЙ СТРОКЕ "B"
				                   ....     ....       ....
				ПОЛУЧАЕТСЯ СТРОКА "IBKBFDSLFIBKBFDSJIFDIBKB" <~ НА ДАННЫЙ МОМЕНТ МЫ УЖЕ НАСЧИТАЛИ IBKB_COUNT == 3
				УБИРАЕМ ОДИН СИМВОЛ И ПРОВЕРЯЕМ НА МАКСИМАЛЬНУЮ ДЛИНУ
			    "IBKBFDSLFIBKBFDSJIFDIBK"
				 ^---------------------^ .length() == 23 и сравниваем с уже найденным максимумом или 0
				ДАЛЕЕ УДАЛЯЕМ СИМВОЛЫ ИЗ СТРОКИ ДО ПЕРВОГО IBKB включительно с буквой I, 
				УМЕНЬШАЕМ IBKB_COUNT НА 1 И ШАГАЕМ ДО СЛЕДУЮЩЕГО НАХОЖДЕНИЯ "IBKB"
				ПОЛУЧИТСЯ ЧТО-ТО ВРОДЕ
				         ....       ....      ....
				"BKBFDSLFIBKBFDSJIFDIBKB......IBKB" ЛИБО КОНЕЦ ФАЙЛА

				ПРИМЕР 2 ЕСЛИ НАЧИНАЕТСЯ НЕ С "IBKB"
				НАПРИМЕР ИСХОДНАЯ "BBKDKIBKB.......IBKB........IBKB..."
				КОГДА МЫ ВПЕРВЫЕ НАШЛИ  ^..^                   ^..^
				МЫ ЗАПОМИНАЕМ ИНДЕКС    ^                      ^
				ПОСЛЕ ЭТОГО НАХОДИМ ТРЕТИЙ                     ^
				ОТРЕЗАЕМ ХВОСТ СТРОКИ С  ^ (буква "B") ДАЛЕЕ КАК В ПРИМЕРЕ 1
			*/
		for (unsigned long int i = 0; i < letters.length(); i++) {
			IBKB += letters[i];
			if (IBKB.length() >= 4)
				if (IBKB.substr(IBKB.length() - 4) == "IBKB") {	// Если конец подстроки содержит "IBKB",
					if (first_IBKB_index == -1)	// то, если мы нашли первый "IBKB"
						first_IBKB_index = IBKB.length() - 4; // записать индекс первого "IBKB"
					else if (second_IBKB_index == -1)
						second_IBKB_index = IBKB.length() - 4;
					else if (third_IBKB_index == -1)
						third_IBKB_index = IBKB.length() - 4;
					IBKB_count++;	// увеличить их количество в подстроке
				}
			if (IBKB_count == 3) {	// Если их количество равно трем,
				if (IBKB.length() - 1 > max_length)	// то если текущая длина больше максимально найденной,
					max_length = IBKB.length() - 1;	// то найден новый максимум подстроки
				
				IBKB = IBKB.substr(first_IBKB_index + 1);	// Обрезать строку с буквой "I" по "IBKB"
				first_IBKB_index = second_IBKB_index - first_IBKB_index;
				second_IBKB_index = third_IBKB_index - first_IBKB_index;
				third_IBKB_index = -1;
				IBKB_count--;	// Уменьшить количество найденных "IBKB"
			}
		}
		if (IBKB.length() > max_length)
			max_length = IBKB.length();
		cout << max_length << endl;

		fin.close();
	} catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}
	
	return 0;
}
