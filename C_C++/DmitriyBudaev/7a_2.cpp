#include <iostream>
#include <fstream>
#include <locale>

using namespace std;

int** manualInput(int& a) {
    cout << "Введите матрицу в формате:" << endl;
    cout << "A" << endl;
    cout << "* * * ... *" << endl;
    cout << "... ... ..." << endl;
    cout << "... ... ..." << endl;
    cout << "... ... ..." << endl;
    cout << "* * * ... *" << endl;
    cin >> a;
    int** matr = new int*[a];
    for (int i = 0; i < a; i++) {
        matr[i] = new int[a];
        for (int j = 0; j < a; j++) {
            cin >> matr[i][j];
        }
    }
    return matr;
}

int** fileInput(int& a) {
    ifstream fin("input.txt");
    fin >> a;
    int** matr = new int*[a];
    for (int i = 0; i < a; i++) {
        matr[i] = new int[a];
        for (int j = 0; j < a; j++) {
            fin >> matr[i][j];
        }
    }
    fin.close();
    return matr;
}

bool calculate(int a, int** matr) {
    int max = -1;
    bool finded = false;
    for (int dia = 0; dia < a; dia++) {
        if (matr[dia][dia] < 0) {
            finded = true;
            for (int j = 0; j < a; j++) {
                if(max < matr[dia][j]) {
                    max = matr[dia][j];
                }
            }
        }
    }
    if (finded) {
        if (max < 0) {
            cout << "Максимум отрицательный" << endl;
            return 0;
        }
        else {
            cout << "Максимум: " << max << endl;
            return 1;
        }
    }
    cout << "Строка не найдена" << endl;
    return 0;
}

void change(int a, int** matr) {
    for (int i = 1; i < a; i++) {
        for (int j = 0; j < a; j++) {
            if ((i + j) % 2 == 0) {
                for (int z = 0; z < a; z++) {
                    if (matr[i][j] == matr[0][z]) {
                        matr[i][j] = 0;
                    }
                }
            }
        }
    }
}

void consolePrint(int a, int** matr) {
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < a; j++) {
            cout << matr[i][j] << " ";
        }
        cout << endl;
    }
}

void filePrint(int a, int** matr) {
    ofstream fout("output.txt");
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < a; j++) {
            fout << matr[i][j] << " ";
        }
        fout << endl;
    }
    fout.close();
}

int main() {

    setlocale(LC_ALL, "RUS");

    int choose;             // Параметр для меню
    int a;                  // Размеры матрицы
    int** matr;             // Матрица
    bool isInputed = false; // Был ли ввод матрицы
    do {
        cout << "1. Ввод матрицы с клавиатуры." << endl;
        cout << "2. Ввод матрицы из файла." << endl;
        cout << "3. Вычисление характеристики." << endl;
        cout << "4. Преобразование матрицы." << endl;
        cout << "5. Печать матрицы." << endl;
        cout << "6. Печать матрицы в файл." << endl;
        cout << "7. Выход." << endl;
        cin >> choose;
        switch (choose)
        {
        case 1: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
                isInputed = false;
            }
            matr = manualInput(a);
            isInputed = true;
            break;
        }
        case 2: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
                isInputed = false;
            }
            try
            {
                matr = fileInput(a);
                isInputed = true;
                break;
            }
            catch(const std::exception& e)
            {
                cout << "Файл ввода отсутствует." << endl;
                break;
            }
        }
        case 3: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }
            cout << calculate(a, matr) << endl;
            break;
        }
        case 4: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }
            change(a, matr);
            break;
        }
            
        case 5: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }          
            consolePrint(a, matr);              
            break;
        }
        case 6: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }
            filePrint(a, matr);
            break;
        }
        case 7: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
            }
            return 0;
        }
        default: {
            cout << "Неверный ввод, попробуйте снова." << endl;
            break;
        }
        }
    } while (choose != 7);

    return 0;
}