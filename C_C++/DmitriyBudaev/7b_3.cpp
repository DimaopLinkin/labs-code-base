#include <iostream>
#include <fstream>
#include <locale>

using namespace std;

int** manualInput(int& a) {
    cout << "Введите матрицу в формате:" << endl;
    cout << "A" << endl;
    cout << "* * * ... *" << endl;
    cout << "... ... ..." << endl;
    cout << "... ... ..." << endl;
    cout << "... ... ..." << endl;
    cout << "* * * ... *" << endl;
    cin >> a;
    int** matr = new int*[a];
    for (int i = 0; i < a; i++) {
        matr[i] = new int[a];
        for (int j = 0; j < a; j++) {
            cin >> matr[i][j];
        }
    }
    return matr;
}

int** fileInput(int& a) {
    ifstream fin("input.txt");
    fin >> a;
    int** matr = new int*[a];
    for (int i = 0; i < a; i++) {
        matr[i] = new int[a];
        for (int j = 0; j < a; j++) {
            fin >> matr[i][j];
        }
    }
    fin.close();
    return matr;
}

bool isLatin(int a, int** matr) {
    bool* latin = new bool[a+1];
    for (int i = 0; i < a; i++) {
        for (int l = 1; l < a + 1; l++) latin[l] = false;
        for (int j = 0; j < a; j++) {
            if (matr[i][j] >= 1 && matr[i][j] <= a)
                if (latin[matr[i][j]] == false)
                    latin[matr[i][j]] = true;
                else {
                    delete[] latin;
                    return false;
                }
            else {
                delete[] latin;
                return false;
            }
        }
    }
    for (int j = 0; j < a; j++) {
        for (int l = 1; l < a + 1; l++) latin[l] = false;
        for (int i = 0; i < a; i++) {
            if (matr[i][j] >= 1 && matr[i][j] <= a)
                if (latin[matr[i][j]] == false)
                    latin[matr[i][j]] = true;
                else {
                    delete[] latin;
                    return false;
                }
            else {
                delete[] latin;
                return false;
            }
        }
    }
    delete[] latin;
    return true;
}

void consolePrint(int a, int** matr) {
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < a; j++) {
            cout << matr[i][j] << " ";
        }
        cout << endl;
    }
}

void filePrint(int a, int** matr) {
    ofstream fout("output.txt");
    for (int i = 0; i < a; i++) {
        for (int j = 0; j < a; j++) {
            fout << matr[i][j] << " ";
        }
        fout << endl;
    }
    fout.close();
}

int main() {

    setlocale(LC_ALL, "RUS");

    int choose;             // Параметр для меню
    int a;                  // Размеры матрицы
    int** matr;             // Матрица
    bool isInputed = false; // Был ли ввод матрицы

    do {
        cout << "1. Ввод матрицы с клавиатуры." << endl;
        cout << "2. Ввод матрицы из файла." << endl;
        cout << "3. Проверка матрицы на латинский квадрат." << endl;
        cout << "4. Печать матрицы." << endl;
        cout << "5. Печать матрицы в файл." << endl;
        cout << "6. Выход." << endl;
        cin >> choose;
        switch (choose)
        {
        case 1: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
                isInputed = false;
            }
            matr = manualInput(a);
            isInputed = true;
            break;
        }
        case 2: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
                isInputed = false;
            }
            try
            {
                matr = fileInput(a);
                isInputed = true;
                break;
            }
            catch(const std::exception& e)
            {
                cout << "Файл ввода отсутствует." << endl;
                break;
            }
        }
        case 3: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }
            cout << isLatin(a, matr) << endl;
            break;
        }
            
        case 4: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }          
            consolePrint(a, matr);              
            break;
        }
        case 5: {
            if (!isInputed) {
                cout << "Матрица не была введена." << endl;
                break;
            }
            filePrint(a, matr);
            break;
        }
        case 6: {
            if (isInputed) {
                for(int i = 0; i < a; i++)
                    delete[] matr[i];
                delete[] matr;
            }
            return 0;
            break;
        }
        default: {
            cout << "Неверный ввод, попробуйте снова." << endl;
            break;
        }
        }
    } while (choose != 6);

    return 0;
}