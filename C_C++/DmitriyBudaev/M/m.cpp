#include <fstream>

using namespace std;

int main() {
    ifstream fin("input.txt");
    unsigned short n;
    fin >> n;

    short classes[50000];
    for (int i = 0; i < n; i++)
        fin >> classes[i];

    unsigned short m;
    fin >> m;
    short goods[1001];
    for (int i = 1; i <= 1000; i++)
        goods[i] = 1001;
    for (int i = 0; i < m; i++) {
        short watt, price;
        fin >> watt >> price;
        if (goods[watt] > price)
            goods[watt] = price;
    }
    fin.close();

    int finalSum = 0;
    for (int i = 0; i < n; i++) {
        int bestSum = 1001;
        for (int j = classes[i]; j <= 1000; j++)
            if (bestSum > goods[j])
                bestSum = goods[j];
        finalSum += bestSum;
    }
    // for (int i = 0; i < n; i++) {
    //     int bestChoice = -1;
    //     for (int j = 0; j < m; j++) {
    //         if (classes[i] <= goods[j][0])
    //             if (bestChoice == -1)
    //                 bestChoice = j;
    //             else if (goods[bestChoice][1] > goods[j][1])
    //                 bestChoice = j;
    //     }
    //     finalSum += goods[bestChoice][1];
    // }

    ofstream fout("output.txt");
    fout << finalSum;
    fout.close();
    return 0;
}