#include <fstream>

using namespace std;

int main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");

    unsigned short answer[2000001];
    for (int i = 0; i <= 2000000; i++)
        answer[i] = 0;

    unsigned short n;
    fin >> n;
    for (unsigned short i = 1; i <= n; i++) {
        int index;
        fin >> index;
        answer[index + 1000000] = 1;
    }

    unsigned short m;
    fin >> m;
    for (unsigned short i = 1; i <= m; i++) {
        int index;
        fin >> index;
        for (int j = 0; j <= 1000000; j++) {
            if (index + j <= 1000000)
                if (answer[index + 1000000 + j] == 1) {
                    fout << index + j << endl;
                    break;
                }
            if (index - j >= -1000000)
                if (answer[index + 1000000 - j] == 1) {
                    fout << index - j << endl;
                    break;
                }
        }
    }
    
    fin.close();
    fout.close();
    return 0;
}