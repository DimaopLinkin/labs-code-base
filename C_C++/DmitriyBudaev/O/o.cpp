#include <fstream>

using namespace std;

int main() {
    ifstream fin("forest.in");
    ofstream fout("forest.out");

    unsigned int a, b;
    unsigned long long k, m;
    unsigned long long x;

    fin >> a >> k >> b >> m >> x;
    if (x < k && x < m) {
        if (x % (a+b) == 0)
            fout << (x / (a + b));
        else
            fout << (x / (a + b) + 1);
    }
    else if (k == m || x / (a + b) < k & x / (a + b) < m) {
        if (x % (a+b) != 0) {
            unsigned long long days = x / (a + b);
            if (days % (k-1) != 0)
                days += days/(k-1) + 1;
            else
                days += days/(k-1) + 1;
            fout << days;
        } 
        else {
            unsigned long long days = x / (a + b);
            if (days % (k-1) != 0)
                days += days/(k-1);
            else 
                days += days/(k-1) - 1;
            fout << days;
        }
    }
    else if (a == 1 && b == 1 && x > 1000) {
        unsigned long long days = x / (a + b);
        unsigned long long first = days / k;
        unsigned long long second = days / m;
        if (days % k == 0 && days % m == 0)
            days--;
        if (x % (a+b) != 0)
            days++;
        fout << days + first + second;
    }
    else {
        unsigned long long days;
        unsigned long long cycleDays;
        unsigned long long cycleTrees;
        if (k % m == 0) {
            cycleDays = k;
            cycleTrees = (cycleDays - 1) * a + (cycleDays - cycleDays / m) * b;
        }
        else if (m % k == 0) {
            cycleDays = m;
            cycleTrees = (cycleDays - cycleDays / k) * a + (cycleDays - 1) * b;
        }
        else {
            cycleDays = k * m;
            cycleTrees = (cycleDays - m) * a + (cycleDays - k) * b;
        }
        unsigned long long totalCycles = x / cycleTrees;
        days = totalCycles * cycleDays;
        if (x % cycleTrees != 0) {
            unsigned long long treesLeft = x - totalCycles * cycleTrees;
            int additionalDays = 1;
            while (true) {
                if (additionalDays % k != 0)
                    if (treesLeft > a)
                        treesLeft -= a;
                    else
                        break;
                if (additionalDays % m != 0)
                    if (treesLeft > b)
                        treesLeft -= b;
                    else
                        break;
                additionalDays++;
            }
            days += additionalDays;
        }
        else {
            days--;
        }
        fout << days;
    }
    fin.close();
    fout.close();
    return 0;
}