#include <iostream>

int main() {
    bool isDiag = true;
    short int count = 0;
    short int n;
    std::cin >> n;
    short int** matr = new short int* [n+1];
    for (short int i = 1; i <= n; i++) {
        matr[i] = new short int[n+1];
        for (short int j = 1; j <= n; j++) {
            std::cin >> matr[i][j];
            if (matr[i][j] < 0) isDiag = false;
        }
    }
    for (short int i = 1; i <= n; i++) {
        int sum1 = 0;
        for (short int j = 1; j <= i-1; j++)
            sum1 += matr[i][j];
        int sum2 = 0;
        for (short int j = i + 1; j <= n; j++)
            sum2 += matr[i][j];
        if (matr[i][i] < sum1+sum2)
            isDiag = false;
        if (matr[i][i] > sum1 + sum2)
            count++;
    }
    if (!count)
        isDiag = false;
    if (isDiag) {
        std::cout << "YES" << std::endl;
        std::cout << count;
    } else {
        std::cout << "NO";
    }

    for (short int i = 1; i <= n; i++)
        delete[] matr[i];
    delete[] matr;
    return 0;
}