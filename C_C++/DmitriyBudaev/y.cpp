#include <fstream>

int main() {
    int x = 0;
    int y = 0;
    char dir = 'L';
    int done = 0;
    int toDone = 1;
    int N;
    std::ifstream cin("input.txt");
    cin >> N;
    cin.close();
    for (unsigned int i = 0; i < N; i++) {
        if (done == toDone) {
            done = 0;
            if (dir == 'D' || dir == 'U')
                toDone++;
            switch (dir)
            {
            case 'L': {
                dir = 'D';
                break;
            }
            case 'D': {
                dir = 'R';
                break;
            }
            case 'R': {
                dir = 'U';
                break;
            }
            case 'U': {
                dir = 'L';
                break;
            }
            default:
                break;
            }
        }
        switch (dir)
        {
        case 'L': {
            x--;
            done++;
            break;
        }
        case 'D': {
            y--;
            done++;
            break;
        }
        case 'R': {
            x++;
            done++;
            break;
        }
        case 'U': {
            y++;
            done++;
            break;
        }            
        default:
            break;
        }
    }
    std::ofstream fout("output.txt");
    fout << x << " " << y;
    fout.close();
    return 0;
}