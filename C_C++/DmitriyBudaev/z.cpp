#include <fstream>

int main() {
    int n, m;
    std::ifstream fin("input.txt");
    fin >> n >> m;
    bool** field = new bool*[n+1];
    for (int i = 1; i <= n; i++) {
        field[i] = new bool[n+1];
    }
    unsigned long int count = n*n;
    for (int i = 1; i <= m; i++) {
        int x, y;
        fin >> x >> y;
        if (!field[x][y]) {
            field[x][y] = true;
            count--;
        }
        if (x - 2 >= 1 && y + 1 <= n && !field[x-2][y+1]) {field[x-2][y+1] = true; count--;}
        if (x - 1 >= 1 && y + 2 <= n && !field[x-1][y+2]) {field[x-1][y+2] = true; count--;}
        if (x + 1 <= n && y + 2 <= n && !field[x+1][y+2]) {field[x+1][y+2] = true; count--;}
        if (x + 2 <= n && y + 1 <= n && !field[x+2][y+1]) {field[x+2][y+1] = true; count--;}
        if (x + 2 <= n && y - 1 >= 1 && !field[x+2][y-1]) {field[x+2][y-1] = true; count--;}
        if (x + 1 <= n && y - 2 >= 1 && !field[x+1][y-2]) {field[x+1][y-2] = true; count--;}
        if (x - 1 >= 1 && y - 2 >= 1 && !field[x-1][y-2]) {field[x-1][y-2] = true; count--;}
        if (x - 2 >= 1 && y - 1 >= 1 && !field[x-2][y-1]) {field[x-2][y-1] = true; count--;}
    }
    fin.close();

    std::ofstream fout("output.txt");
    fout << count;
    fout.close();
    for (int i = 1; i <= n; i++)
        delete[] field[i];
    delete[] field;
    return 0;
}