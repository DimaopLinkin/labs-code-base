#include "stdio.h"

int main() {
    int count = 0;
    int x;
    scanf("%d", &x);
    do {
        x = x / 10;
        if (x % 2 == 0) count++;
    } while (x / 10 >= 10);
    printf("%d\n", count);
    
    return 0;
}