#include "Complex.h"

Complex operator*(const Complex& first, const Complex& second) {
	return Complex(first.a * second.a - first.b * second.b, first.a * second.b + first.b * second.a);
}

Complex operator-(const Complex& first, const Complex& second) {
	return Complex(first.a - first.b, second.a - second.b);
}
