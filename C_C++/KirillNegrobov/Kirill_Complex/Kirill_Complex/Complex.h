#include "Pair.h"
#pragma once

class Complex : public Pair {
public:
	Complex() : Pair() {}
	Complex(double a, double b) : Pair(a, b) {}
	friend Complex operator*(const Complex&, const Complex&);
	friend Complex operator-(const Complex&, const Complex&);
};