#include <iostream>
#include "Complex.h"

using namespace std;

int main() {
	setlocale(LC_ALL, "RUS");

	Complex complex1 = Complex(5, 12);
	Complex complex2 = Complex(6, 15);
	cout << "First complex pair " << complex1 << endl;
	cout << "Second complex pair " << complex2 << endl;
	cout << "First + Second " << complex1 + complex2 << endl;
	cout << "First - Second " << complex1 - complex2 << endl;
	cout << "First * Second " << complex1 * complex2 << endl;

	system("PAUSE");
	return 0;
}