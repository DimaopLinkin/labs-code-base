#include "Pair.h"

Pair::Pair() {
	cout << "First = ";
	cin >> a;
	cout << "Second = ";
	cin >> b;
}

Pair::Pair(double a, double b) : a(a), b(b) {}

void Pair::setFirst(double a) {
	this->a = a;
}

void Pair::setSecond(double b) {
	this->b = b;
}

double Pair::getFirst() {
	return a;
}

double Pair::getSecond() {
	return b;
}

double Pair::getMultiplication() {
	return a * b;
}

Pair operator+(const Pair& first, const Pair& second) {
	return Pair(first.a + first.b, second.a + second.b);
}

ostream& operator<<(ostream& out, const Pair& pair) {
	out << "(" << pair.a << ", " << pair.b << ")";

	return out;
}
