#include <iostream>
using namespace std;
#pragma once

class Pair {
protected:
	double a;
	double b;
public:
	Pair();
	Pair(double, double);
	void setFirst(double);
	void setSecond(double);
	double getFirst();
	double getSecond();
	double getMultiplication();

	friend Pair operator+(const Pair&, const Pair&);
	friend ostream& operator<<(ostream&, const Pair&);
};