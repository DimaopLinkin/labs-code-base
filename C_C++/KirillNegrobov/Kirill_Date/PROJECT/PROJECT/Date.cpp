/*������� ����� Date ��� ������ � ������ � ������� ����.�����.����.� ���� �������������� ���������� � ����� ������ ����
unsigned int: ��� ����, ������ � ���.����� ������ �������� �� ����� ��� ������� ������������� : �������, �������� ���� ����.�����.����.�
(��������, �2004.08.31�) � �����.������������� ���������� �������� ���������� ���� ����� �������� ���������� ����, ��������� ��������� ����������
���� �� ����, ����������� ������������ ����, ���������� � ��������� ��������� ������(���, �����, ����), �������� ���(�����, ��, �����), ����������
���������� ���� ����� ������.*/
#include <iostream>
#include <string>
using namespace std;
#include "Date.h"

/*����������� �� ��������� � ����� 2004.08.31*/
Date::Date() :year(2004), month(8), day(31) {} 

/*����������� ���� �� ������*/
Date::Date(unsigned int Year, unsigned int Month, unsigned int Day) : year(Year), month(Month), day(Day) {}

/*����������� ���� �� ������*/
Date::Date(string str) {
	unsigned int Year = 0, Month = 0, Day = 0;
	for (int i = 0; i < 4; i++)
		Year = Year * 10 + ((int)str[i] - 48);
	for (int i = 5; i < 7; i++)
		Month = Month * 10 + ((int)str[i] - 48);
	for (int i = 8; i < 10; i++)
		Day = Day * 10 + ((int)str[i] - 48);
	setYear(Year); setMonth(Month); setDay(Day);
}



void Date::setYear(unsigned int Year) {
	year = Year;
}

void Date::setMonth(unsigned int Month) {
	month = Month;
}

void Date::setDay(unsigned int Day) {
	day = Day;
}

unsigned int Date::getYear() {
	return year;
}

unsigned int Date::getMonth() {
	return month;
}

unsigned int Date::getDay() {
	return day;
}

/*�������� ���� �� ������������*/
bool Date::isLeapYear() {
	if (year % 4 == 0) {
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				return true;
			}
			else {
				return false;
			}

		}
		else {
			return true;
		}

	}
	else {
		return false;
	}
}
/*����� ���������� �� ���������� ����������� ���� � ������ ������ � ������ ������������ ����*/
Date Date::addition_days(unsigned int days) {
	unsigned int *curr_month = new unsigned int[12]{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; /*���������� ���� �� �������*/

	Date curr_date(this->year, this->month, this->day);
	/*���� ��� ���������� �� ������ ����������� ��� � ������� 29 ���� ������ 28*/
	if (curr_date.month == 2 && curr_date.isLeapYear()) {
		curr_month[1] = 29;
	}
	/*���� ���������� ���� ������� ����� ��������� ������� �� ����� ���������� ���� ������ �� ����������� ����� � ���� �������� ���������*/
	for (unsigned int i = 0; i < days; i++) {
		curr_date.day++;
		if (curr_date.day > curr_month[curr_date.month - 1]) {
			curr_date.day = 1;
			curr_date.month++;
			/*���� ���������� ������� ������ 12 �� ����������� ���, � ������ �������� �� ���������*/
			if (curr_date.month > 12) {
				curr_date.month = 1;
				curr_date.year++;
				/*�� �������� ��� �������� ������������ ����*/
				if (curr_date.isLeapYear()) {
					curr_month[1] = 29;
				}
				else {
					curr_month[1] = 28;
				}
			}
		}
	}
	delete[] curr_month;
	return curr_date;
}
/*����� ��������� ���������� ���������� ���� �� ����*/
Date Date::subtraction_of_days(unsigned int days) {             
	unsigned int *curr_month = new unsigned int[12]{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }; /*���������� ���� �� �������*/

	Date curr_date(this->year, this->month, this->day);
	/*���� ��� ���������� �� ������ ����������� ��� � ������� 29 ���� ������ 28*/
	if (curr_date.month == 2 && curr_date.isLeapYear()) {
		curr_month[1] = 29;
	}
	/*���� ���������� ���� ������� ����� ������� ������ ������� ��� ������ �� ����� ����������� � ��� ���������� ��������� �������*/
	for (unsigned int i = 0; i < days; i++) {
		curr_date.day--;
		if (curr_date.day < 1) {
			curr_date.day = curr_month[curr_date.month - 2];
			curr_date.month--;
			/*���� ������������ ����� ������ �������, �� ��������� ���, ������� ����������*/
			if (curr_date.month < 1) {
				curr_date.month = 12;
				curr_date.year--;
				/*�������� ������������*/
				if (curr_date.isLeapYear()) {
					curr_month[1] = 29;
				}
				else {
					curr_month[1] = 28;
				}
			}
		}
	}
	delete[] curr_month;
	return curr_date;
}
/*����� ��������� �� ������ ������� ����� ����� ������*/
unsigned int Date::number_between_dates(Date data) {
	int *curr_month = new int[12]{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	Date new_data(data.year, data.month, data.day);

	if (new_data.month > 2 && new_data.isLeapYear()) {
		curr_month[1] = 29;
	}

	int days = 0;

	while (new_data.day != day || new_data.month != month || new_data.year != year) {
		new_data.day--;
		days++;
		if (new_data.day == 0) {
			new_data.month--;
			if (new_data.month == 0) {
				new_data.month = 12;
			}
			new_data.day = curr_month[new_data.month - 1];
			if (new_data.month == 12) {
				new_data.year--;
				if (new_data.isLeapYear()) {
					curr_month[1] = 29;
				}
				else {
					curr_month[1] = 28;
				}
			}
		}
	}
	delete[] curr_month;
	return days;
}
/*����� ���������� �� ��������� ���� ���*/
bool Date::date_comparison(Date data) {
	bool flag = 0;
	unsigned int y1 = year;
	unsigned int y2 = data.year;
	unsigned int m1 = month;
	unsigned int m2 = data.month;
	unsigned int d1 = day;
	unsigned int d2 = data.day;
	if (y1 > y2) {
		cout << "������ ���� ���� ����� ������." << endl;
		flag = 1;
	}
	else if (y1 < y2) cout << "������ ���� ���� ����� ������." << endl;
	else {
		if (m1 > m2) {
			cout << "������ ���� ���� ����� ������." << endl;
			flag = 1;
		}
		else if (m1 < m2) cout << "������ ���� ���� ����� ������." << endl;
		else {
			if (d1 > d2) {
				cout << "������ ���� ���� ����� ������." << endl;
				flag = 1;
			}
			else if (d1 < d2) cout << "������ ���� ���� ����� ������." << endl;
			else cout << "���� �����." << endl;
		}
	}
	return flag;
}

/*����� ������������ ���������� � ���� ������*/
string Date::toString() {
	string result;
	/*���� ��� ������ 10 �� � ������ ���������� "000", ���� ������ 100 �� "00", ���� ������ 1000 �� "0" */
	if (year < 10) result += "000";
	else if (year < 100) result += "00";
	else if (year < 1000) result += "0";
	result += to_string(year);

	result += ".";
	/*���������� � �������*/
	if (month < 10) result += "0";
	result += to_string(month);
	result += ".";
	/*���������� � ����*/
	if (day < 10) result += "0";
	result += to_string(day);
	
	return result;
}
