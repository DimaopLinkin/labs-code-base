#include <iostream>
#include <string>
using namespace std;
#pragma once

class Date {
	unsigned int year;
	unsigned int month;
	unsigned int day;
public:
	Date();
	Date(unsigned int Year, unsigned int Month, unsigned int Day);
	Date(string str);
	void setYear(unsigned int Year);
	void setMonth(unsigned int Month);
	void setDay(unsigned int Day);
	unsigned int getYear();
	unsigned int getMonth();
	unsigned int getDay();
	bool isLeapYear();
	Date addition_days(unsigned int days);
	Date subtraction_of_days(unsigned int days);
	unsigned int number_between_dates(Date data);
	bool date_comparison(Date data);
	string toString();
};