#include "Payment.h"
// ������������
Payment::Payment() {
	cout << "������� ��� ";
	cin >> fio;
	cout << "������� �������� ";
	cin >> salary;
	cout << "������� ��� ���������� � ���� ";
	cin >> nadbavka;
	cout << "������� ������� ������ ";
	cin >> nalog;
	cout << "������� ���������� ������������ ����";
	cin >> worked_days;
	cout << "������� ���������� ������� ����";
	cin >> working_days;
	cout << "������� ���� � ������� yyyy.mm.dd";
	string data;
	cin >> data;
	date = Date(data);
	getPayedSum();
}
Payment::Payment(
	string fio,
	float salary,
	float nadbavka,
	float nalog,
	int worked_days,
	int working_days,
	Date date) :
	fio(fio),
	salary(salary),
	nadbavka(nadbavka),
	nalog(nalog),
	worked_days(worked_days),
	working_days(working_days),
	date(date) {
	getPayedSum();
}

// �������
void Payment::setFio(string fio) {
	this->fio = fio;
}
void Payment::setSalary(float salary) {
	this->salary = salary;
}
void Payment::setDate(Date date) {
	this->date = date;
}
void Payment::setNadbavka(float nadbavka) {
	this->nadbavka = nadbavka;
}
void Payment::setNalog(float nalog) {
	this->nalog = nalog;
}
void Payment::setWorkedDays(int days) {
	this->worked_days = worked_days;
}
void Payment::setWorkingDays(int days) {
	this->working_days = working_days;
}

// �������
string Payment::getFio() {
	return fio;
}
float Payment::getSalary() {
	return salary;
}
Date Payment::getDate() {
	return date;
}
float Payment::getNadbavka() {
	return nadbavka;
}
float Payment::getNalog() {
	return nalog;
}
int Payment::getWorkedDays() {
	return worked_days;
}
int Payment::getWorkingDays() {
	return working_days;
}
float Payment::getPayedSum() {
	payed_sum = (float)worked_days / (float)working_days * salary + salary * nadbavka / 100;
	return payed_sum;
}
float Payment::getAbusedSum() {
	abused_sum = payed_sum * 0.01 + payed_sum * nalog / 100;
	return abused_sum;
}
float Payment::getOnHandSum() {
	return payed_sum - abused_sum;
}
int Payment::getDateExperience(Date date) {
	return this->date.number_between_dates(date) / 365;
}
