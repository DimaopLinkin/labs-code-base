#include <iostream>
#include <string>
#include "Date.h"
using namespace std;
#pragma once

class Payment {
	string fio;
	float salary;
	float nadbavka;
	float nalog;
	int worked_days;
	int working_days;
	float payed_sum;
	float abused_sum;
	Date date;
public:
	// ������������
	Payment();
	Payment(string fio, float salary, float nadbavka, float nalog, int worked_days, int working_days, Date date);

	// �������
	void setFio(string fio);
	void setSalary(float salary);
	void setDate(Date date);
	void setNadbavka(float nadbavka);
	void setNalog(float nalog);
	void setWorkedDays(int days);
	void setWorkingDays(int days);

	// �������
	string getFio();
	float getSalary();
	Date getDate();
	float getNadbavka();
	float getNalog();
	int getWorkedDays();
	int getWorkingDays();

	// ������� � �����������
	float getPayedSum();
	float getAbusedSum();

	// ������
	float getOnHandSum();
	int getExperience();
	int getDateExperience(Date date);
};