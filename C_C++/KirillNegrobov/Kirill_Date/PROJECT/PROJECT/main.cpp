/*������� ����� Date ��� ������ � ������ � ������� ����.�����.����.� ���� �������������� ���������� � ����� ������ ����
unsigned int: ��� ����, ������ � ���.����� ������ �������� �� ����� ��� ������� ������������� : �������, �������� ���� ����.�����.����.�
(��������, �2004.08.31�) � �����.������������� ���������� �������� ���������� ���� ����� �������� ���������� ����, ��������� ��������� ����������
���� �� ����, ����������� ������������ ����, ���������� � ��������� ��������� ������(���, �����, ����), �������� ���(�����, ��, �����), ����������
���������� ���� ����� ������.*/
#include <iostream>
using namespace std;
#include "Date.h"
#include "Payment.h"

void menu();

int main()
{
	setlocale(LC_ALL, "RUS");

	//menu();

	Payment payment = Payment("��� �����", 90000, 15, 13, 18, 25, Date("2003.12.03"));
	cout << "���������� " << payment.getPayedSum() << endl;
	cout << "�������� �������� " << payment.getAbusedSum() << endl;
	cout << "������ �� ���� " << payment.getOnHandSum() << endl;
	cout << "���� " << payment.getDateExperience(Date("2021.06.04")) << endl;

	system("pause");
	return 0;
}
/*���������� ����*/
void menu() {
	int k;
	do
	{
		cout << "�������� ��������: \n1) ��������� ���� ����� �������� ���-�� ����; \n2) ������� �������� ���-�� ���� �� ����; \n3) ���������� ������������ ����; \n4) �������� ����; \n5) ��������� ���-�� ���� ����� ������; \n6) ��������� ������ ���������; \n";
		cin >> k;

		switch (k) {

		case 1: {
			Date d;
			cout << "�������� ������ ������������� ����: 1) �������; 2) �������. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			cout << "������� ���-�� ����: "; unsigned int days; cin >> days;
			cout << "������ ����: " << d.toString() << "    " << "����� ����: " << d.addition_days(days).toString() << endl << endl;
			break;
		}
		case 2: {
			Date d;
			cout << "�������� ������ ������������� ����: 1) �������; 2) �������. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			cout << "������� ���-�� ����: "; unsigned int days; cin >> days;
			cout << "������ ����: " << d.toString() << "    " << "����� ����: " << d.subtraction_of_days(days).toString() << endl << endl;
			break;
		}
		case 3: {
			Date d;
			cout << "�������� ������ ������������� ����: 1) �������; 2) �������. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			if (d.isLeapYear()) cout << d.getYear() << " - ����������." << endl << endl;
			else cout << d.getYear() << " - �� ����������." << endl << endl;
			break;
		}
		case 4: {
			Date d1, d2;
			cout << "�������� ������ ������������� ������ ����: 1) �������; 2) �������. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d1 = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d1 = data;
				break;
			}
			}
			cout << "�������� ������ ������������� ������ ����: 1) �������; 2) �������. \n"; int k2; cin >> k2;
			switch (k2) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d2 = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d2 = data;
				break;
			}
			}
			d1.date_comparison(d2);
			break;
		}
		case 5: {
			Date d1, d2;
			cout << "�������� ������ ������������� ������ ����: 1) �������; 2) �������. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d1 = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d1 = data;
				break;
			}
			}
			cout << "�������� ������ ������������� ������ ����: 1) �������; 2) �������. \n"; int k2; cin >> k2;
			switch (k2) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "������� ���: "; cin >> Year;
				cout << "������� �����: "; cin >> Month;
				cout << "������� ����: "; cin >> Day;
				Date data(Year, Month, Day);
				d2 = data;
				break;
			}
			case 2: {
				string str;
				cout << "������� ����: "; cin >> str;
				Date data(str);
				d2 = data;
				break;
			}
			}
			if (!d1.date_comparison(d2)) cout << "���-�� ���� ����� ������: " << d1.number_between_dates(d2) << endl << endl;
			else cout << "���-�� ���� ����� ������: " << d2.number_between_dates(d1) << endl << endl;
			break;
		}
		case 6: {
			cout << "������ ��������� ���������." << endl;
			//system("pause");
			exit(1);
		}
		default: {
			cout << "�������� ����!" << endl;
			//system("pause");
			exit(1);
		}
		}
	} while (k != 6);
}