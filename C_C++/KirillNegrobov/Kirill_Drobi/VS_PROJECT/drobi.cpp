#include "drobi.h"
#include <sstream>
// ����������� �� ���������
Drobi::Drobi() {
	celoe = new long int(0);
	drobnoe = new unsigned short int(0);
	isPositive = new bool(true);
}

// ����������� � �����������
Drobi::Drobi(long int celoe, unsigned short int drobnoe, bool isPositive) {
	this->celoe = new long int(celoe);
	this->drobnoe = new unsigned short(drobnoe);
	this->isPositive = new bool(isPositive);
}

// ����������� � ���������� � ���� ������
Drobi::Drobi(string drob) {
	celoe = new long int(0);
	drobnoe = new unsigned short int(0);
	isPositive = new bool(true);
	int count = 0;
	int forLength = drob.find(".");
	if (forLength == -1) forLength = drob.length();
	for (int i = 0; i < forLength; i++) {
		if (drob[i] == '-') {
			continue;
		}
		*celoe = *celoe * 10 + drob[i] - 48;
	}
	if (drob[0] == '-') {
		*celoe = *celoe * -1;
		if (*celoe == 0) *isPositive = false;
	}
	if (forLength != drob.length()) {
		for (int i = drob.find(".") + 1; i < drob.length(); i++) {
			if (count == 4) {
				break;
			}
			*drobnoe = *drobnoe * 10 + drob[i] - 48;
			count++;
		}
		while (count < 4) {
			*drobnoe = *drobnoe * 10;
			count++;
		}
	}
}

// ����������� �����������
Drobi::Drobi(const Drobi& right) {
	celoe = new long int;
	*celoe = *right.celoe;
	drobnoe = new unsigned short;
	*drobnoe = *right.drobnoe;
	isPositive = new bool(true);
	*isPositive = *right.isPositive;
}

// ����������
Drobi::~Drobi() {
	delete celoe;
	delete drobnoe;
}

// �������
void Drobi::setCeloe(long int celoe) {
	delete this->celoe;
	this->celoe = &celoe;
}
void Drobi::setDrobnoe(unsigned short int drobnoe) {
	delete this->drobnoe;
	this->drobnoe = &drobnoe;
}

// �������
long int Drobi::getCeloe() {
	return *celoe;
}
unsigned short int Drobi::getDrobnoe() {
	return *drobnoe;
}

// ������� �������� �� float
float getFloat(const Drobi& drob) {
	float result;
	result = *drob.celoe;
	if (result < 0) {
		result -= *drob.drobnoe / 10000.;
	}
	else {
		result += *drob.drobnoe / 10000.;
	}
	if (!*drob.isPositive) {
		result *= -1;
	}
	return result;
}

// ���������� ����������
Drobi operator+(const Drobi& left, const Drobi& right) {
	float levoe;
	float pravoe;
	levoe = getFloat(left);
	pravoe = getFloat(right);
	float summa = levoe + pravoe;
	std::ostringstream ss;
	ss << summa;
	string str(ss.str());
	Drobi* result = new Drobi(str);
	return *result;
}
Drobi operator-(const Drobi& left, const Drobi& right) {
	float levoe;
	float pravoe;
	levoe = getFloat(left);
	pravoe = getFloat(right);
	float raznost = levoe - pravoe;
	std::ostringstream ss;
	ss << raznost;
	string str(ss.str());
	Drobi* result = new Drobi(str);
	return *result;
}
Drobi operator*(const Drobi& left, const Drobi& right) {
	float levoe;
	float pravoe;
	levoe = getFloat(left);
	pravoe = getFloat(right);
	float proizvedenie = levoe * pravoe;
	std::ostringstream ss;
	ss << proizvedenie;
	string str(ss.str());
	Drobi* result = new Drobi(str);
	return *result;
}
Drobi operator/(const Drobi& left, const Drobi& right) {
	float levoe;
	float pravoe;
	levoe = getFloat(left);
	pravoe = getFloat(right);
	float chastnoe = levoe / pravoe;
	std::ostringstream ss;
	ss << chastnoe;
	string str(ss.str());
	Drobi* result = new Drobi(str);
	return *result;
}
bool operator==(const Drobi& left, const Drobi& right) {
	if (*left.celoe == *right.celoe && *left.drobnoe == *right.drobnoe)
		return true;
	else return false;
}
bool operator<(const Drobi& left, const Drobi& right) {
	if (*left.celoe == *right.celoe) {
		if (*left.drobnoe < *right.drobnoe) {
			return true;
		}
		else return false;
	}
	else if (*left.celoe < *right.celoe) {
		return true;
	}
	else return false;
}
bool operator>(const Drobi& left, const Drobi& right) {
	if (*left.celoe == *right.celoe) {
		if (*left.drobnoe > *right.drobnoe) {
			return true;
		}
		else return false;
	}
	else if (*left.celoe > *right.celoe) {
		return true;
	}
	else return false;
}
Drobi& Drobi::operator=(const Drobi& right) {
	if (this == &right) return *this;

	*celoe = *right.celoe;
	*drobnoe = *right.drobnoe;
	return *this;
}
ostream& operator<<(ostream& out, const Drobi& drob) {
	if (*drob.isPositive == false) {
		out << "-";
	}
	out << *drob.celoe;
	if (*drob.drobnoe >= 1000)
		out << ".";
	else if (*drob.drobnoe >= 100)
		out << ".0";
	else if (*drob.drobnoe >= 10)
		out << ".00";
	else
		out << ".000";
	out << *drob.drobnoe;
	return out;
}
istream& operator>>(istream& in, const Drobi& drob) {
	in >> *drob.celoe >> *drob.drobnoe;
	return in;
}