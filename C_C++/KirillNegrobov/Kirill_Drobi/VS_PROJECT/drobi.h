#pragma once
#include <iostream>
using namespace std;

class Drobi {
private:
	long int* celoe;
	unsigned short int* drobnoe;
	bool* isPositive;
public:
	// ������������ � ����������
	Drobi(long int celoe, unsigned short int drobnoe, bool isPositive = true);
	Drobi(string drob);
	Drobi(const Drobi&);
	Drobi();
	~Drobi();

	// �������
	void setCeloe(long int celoe);
	void setDrobnoe(unsigned short int drobnoe);

	// �������
	long int getCeloe();
	unsigned short int getDrobnoe();

	// ������� �������� �� float
	friend float getFloat(const Drobi& drob);

	// ���������� ����������
	friend Drobi operator+(const Drobi& left, const Drobi& right);
	friend Drobi operator-(const Drobi& left, const Drobi& right);
	friend Drobi operator*(const Drobi& left, const Drobi& right);
	friend Drobi operator/(const Drobi& left, const Drobi& right);
	friend bool operator==(const Drobi& left, const Drobi& right);
	friend bool operator<(const Drobi& left, const Drobi& right);
	friend bool operator>(const Drobi& left, const Drobi& right);
	Drobi& operator=(const Drobi& right);
	friend ostream& operator<<(ostream& out, const Drobi& drob);
	friend istream& operator>>(istream& in, const Drobi& drob);
};