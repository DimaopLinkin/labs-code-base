#include "drobi.h"
#include <iostream>
using namespace std;

// Конструктор по умолчанию
Drobi::Drobi() {
    celoe = 0;
    drobnoe = 0;
}

// Конструктор с параметрами
Drobi::Drobi(long int celoe, unsigned short int drobnoe) {
    this->celoe = &celoe;
    while (drobnoe / 10000 < 0) drobnoe *= 10;
    this->drobnoe = &drobnoe;
}

// Конструктор копирования
Drobi::Drobi(const Drobi& right) {
    celoe = right.celoe;
    drobnoe = right.drobnoe;
}

// Деструктор
Drobi::~Drobi() {
    delete celoe;
    delete drobnoe;
}

// Сеттеры
void Drobi::setCeloe(long int celoe) {
    delete this->celoe;
    this->celoe = &celoe;
}
void Drobi::setDrobnoe(unsigned short int drobnoe) {
    delete this->drobnoe;
    this->drobnoe = &drobnoe;
}

// Геттеры
long int Drobi::getCeloe() {
    return *celoe;
}
unsigned short int Drobi::getDrobnoe() {
    return *drobnoe;
}

// Перегрузка операторов
Drobi operator+(const Drobi& left, const Drobi& right) {
    // newDrob = new Drobi();
}
Drobi operator-(const Drobi& left, const Drobi& right){}
Drobi operator*(const Drobi& left, const Drobi& right){}
Drobi operator/(const Drobi& left, const Drobi& right){}
bool operator==(const Drobi& left, const Drobi& right) {
    if (*left.celoe == *right.celoe && *left.drobnoe == *right.drobnoe)
        return true;
    else return false;
}
bool operator<(const Drobi& left, const Drobi& right) {
    if (*left.celoe == *right.celoe) {
        if (*left.drobnoe < *right.drobnoe) {
            return true;
        }
        else return false;
    }
    else if (*left.celoe < *right.celoe) {
        return true;
    }
    else return false;
}
bool operator>(const Drobi& left, const Drobi& right) {
    if (*left.celoe == *right.celoe) {
        if (*left.drobnoe > *right.drobnoe) {
            return true;
        }
        else return false;
    }
    else if (*left.celoe > *right.celoe) {
        return true;
    }
    else return false;
}
Drobi& Drobi::operator=(const Drobi& right) {
    if (this == &right) return *this;

    celoe = right.celoe;
    drobnoe = right.drobnoe
    return *this;
}
ostream& operator<<(ostream& out, const Drobi& drob) {
    if (drob.drobnoe >= 1000)
        out << drob.celoe << "," << drob.drobnoe;
    else if (drob.drobnoe >= 100)
        out << drob.celoe << ",0" << drob.drobnoe;
    else if (drob.drobnoe >= 10)
        out << drob.celoe << ",00" << drob.drobnoe;
    else
        oub << drob.celoe << ",000" << drob.drobnoe;
	return out;
}
istream& operator>>(istream& in, const Drobi& drob) {
    in >> drob.celoe;
    in >> drob.drobnoe;
}