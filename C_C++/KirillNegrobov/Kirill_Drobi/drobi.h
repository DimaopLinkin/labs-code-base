using namespace std;

class Drobi {
    long int *celoe;
    unsigned short int *drobnoe;
public:
    // Конструкторы и деструктор
    Drobi(long int celoe, unsigned short int drobnoe);
    Drobi(const Drobi&);
    Drobi();
    ~Drobi();
    
    // Сеттеры
    void setCeloe(long int celoe);
    void setDrobnoe(unsigned short int drobnoe);

    // Геттеры
    long int getCeloe();
    unsigned short int getDrobnoe();

    // Перегрузка операторов
    friend Drobi operator+(const Drobi& left, const Drobi& right);
    friend Drobi operator-(const Drobi& left, const Drobi& right);
    friend Drobi operator*(const Drobi& left, const Drobi& right);
    friend Drobi operator/(const Drobi& left, const Drobi& right);
    friend bool operator==(const Drobi& left, const Drobi& right);
    friend bool operator<(const Drobi& left, const Drobi& right);
    friend bool operator>(const Drobi& left, const Drobi& right);
    Drobi& operator=(const Drobi& right);
    friend ostream& operator<<(ostream& out, const Drobi& drob);
    friend istream& operator>>(istream& in, const Drobi& drob);
};