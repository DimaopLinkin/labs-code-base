#include "iostream"
#include "locale.h"
#include "drobi.h"

using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    Drobi drob = Drobi(15, 1540);
    cout << "Введите дробь" << endl;
    cin >> drob;

    cout << &drob;

    delete &drob;
    return 0;
}