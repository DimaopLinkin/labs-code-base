#pragma once
#include "List.h"

using namespace std;

class TableList :public List
{
public:
	bool searchElem(int numberadr)
	{
		Node* p = head;
		while (p)
		{
			if (p->data.address == numberadr)
			{
				p->k++;
				return true;

			}
			p = p->next;
		}
		return false;
	}

	void insertElem(Person& data)
	{
		AddToHead(data);
	}

	void deleteElem(int numberadr)
	{
		if (head)
		{
			if (head->data.address == numberadr)
				DeleteFromHead();
			else
			{
				Node* p = head;

				while (p->next)
				{
					if (p->next->data.address == numberadr)
						DeleteAfterNode(p);
					else
						p = p->next;
				}

			}
		}
	}
	void print()
	{
		Node* current = head;

		while (current)
		{
			cout << "_________________________________________________";
			cout << current->data.FIO << endl;
			cout << current->data.address << endl;
			cout << current->data.street << endl;
			cout << current->data.home_number << endl;
			cout << current->data.flat << endl;
			cout << current->data.phone_number << endl;
			cout << current->data.balance << endl;
			cout << "_________________________________________________" << endl;
			current = current->next;
		}
	}


};

