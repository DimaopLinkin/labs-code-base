#include <iostream>
using namespace std;
#include "linear.h"
#include "square.h"

int main()
{
	setlocale(LC_ALL, "RUS");

	Linear linear = Linear();
	linear.solution();
	Square square = Square();
	square.solution();
	linear.getString();
	cout << endl;
	square.getString();
	system("pause");
	return 0;
}