#pragma once
class Root
{
public:
	virtual void solution() = 0;
	virtual void getString() = 0;
};