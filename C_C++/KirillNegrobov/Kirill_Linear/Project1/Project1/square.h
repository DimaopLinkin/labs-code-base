#include <iostream>
#include "root.h"

using namespace std;

class Square : public Root {
	double a;
	double b;
	double c;
public:
	// ������������
	Square();
	Square(double a, double b, double c);

	// �������
	void	setFirst(double a);
	void	setSecond(double b);
	void	setThird(double c);
	double	getFirst();
	double	getSecond();
	double	getThird();
	void solution();
	void getString();
};