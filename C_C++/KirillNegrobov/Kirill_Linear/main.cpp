#include <iostream>
using namespace std;
#include "includes.h"

int main()
{
	setlocale(LC_ALL, "RUS");

	Linear linear = Linear();
	linear.solution();
	Square square = Square();
	square.solution();
	linear.getString();
	square.getString();
	system("pause");
	return 0;
}