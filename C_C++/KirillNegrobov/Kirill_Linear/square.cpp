#include "includes.h"
#include "math.h"

Square::Square() {
	cout << "������� �������� a,b,c ��������� ���� ax^2+bx+c=0 " << endl;
	cout << "A = ";
	cin >> a;
	cout << "B = ";
	cin >> b;
	cout << "C = ";
	cin >> c;
}

Square::Square(double a, double b, double c) : a(a), b(b), c(c) {}

void Square::setFirst(double a) {
	this->a = a;
}

void Square::setSecond(double b) {
	this->b = b;
}

void Square::setThird(double c) {
	this->c = c;
}

double Square::getFirst() {
	return a;
}

double Square::getSecond() {
	return b;
}

double Square::getThird() {
	return c;
}

void Square::solution()
{	

	
	double x;
	if (a == 0) //��������, �� � �� ������ ����� ������������ ����� Linear ��� �������
	{
		if (b == 0)
		{
			cout << "������� ��� " << endl;
		}
		else
		{
			x = -c / b;
			cout << "������ ��������� ���� ������ " << x << endl;
		}
	}
	else
	{
		if ((b * b - 4 * a * c) >= 0) 
		{
			x = (-1 * b + sqrt(b * b - 4 * a * c)) / (2 * a);
			cout << "������ ������ ����� " << x << endl;
			x = (-1 * b - sqrt(b * b - 4 * a * c)) / (2 * a);
			cout << "������ ������ ����� " << x << endl;
		}
		else
		{
			cout << "������������ ������ 0, ����� ��������������." << endl;
		}
	}
}

void Square::getString() {
	std::cout << a << "x^2 ";
	if (b > 0) {
		std::cout << "+";
	}
	std::cout << b << "x ";
	if (c > 0) {
		std::cout << "+";
	}
	std::cout << c << " = 0"; 
}