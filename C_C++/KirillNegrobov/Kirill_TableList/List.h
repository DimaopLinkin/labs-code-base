#include <iostream>
#include <string>

using namespace std;

struct Person {
	string FIO;
	int address;
	string street;
	int home_number;
	int flat;
	int phone_number;
	int balance;
};


class List {
public:
	struct Node	{
		Person data;
		int k = 0;
		Node* next;
	};
	int count;
	Node* head;

	List() : head(0), count(0) {}

	bool Empty() {
		return head == 0;
	}

	void AddToHead(Person data)	{
		Node* tmp = new Node;
		tmp->data = data;
		tmp->next = head;
		head = tmp;
		count++;
	}

	void DeleteFromHead() {
		if (head) {
			Node* tmp = head;
			head = head->next;
			tmp->next = 0;
			delete tmp;
			tmp = 0;
			count--;
		}
	}

	void DeleteAfterNode(Node* node) {
		if (node && node->next)	{
			Node* tmp = node->next;
			node->next = tmp->next;
			tmp->next = 0;
			delete tmp;
			tmp = 0;
			count--;
		}
	}

	void Clear() {
		while (!Empty())
			DeleteFromHead();
	}

	~List()	{
		Clear();
	}
};