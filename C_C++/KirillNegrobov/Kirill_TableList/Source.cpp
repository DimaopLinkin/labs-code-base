#include <iostream>
#include "TableList.h"

using namespace std;

void menu();

int main() {
	setlocale(LC_ALL, "Rus");
	menu();
	return 0;
}

void menu() {
	int k;
	do {
		TableList T;
		cout << "Выберите оберацию: \n1) добавить запись; \n2) найти запись; \n3) распечатать элементы; \n4) закончить программу;\n";
		cin >> k;
		switch (k) {
      case 1: {			
        Person date;
        cout << "Введите ФИО\n";
        cin >> date.FIO;
        cout << "Введите Адрес\n";
        cin >> date.address;
        cout << "Введите Улицу\n";
        cin >> date.street;
        cout << "Введите номер дома\n";
        cin >> date.home_number;
        cout << "Введите номер квартиры\n";
        cin >> date.flat;
        cout << "Введите номер телефона\n";
        cin >> date.phone_number;
        cout << "Введите баланс\n";
        cin >> date.balance;
        T.insertElem(date);
        T.Empty();
        break;
      }
      case 2: {
        int numadress;
        cout << "Введите адрес";
        cin >> numadress;
        T.searchElem(numadress);
        T.Empty();
        break;
      }
      case 3: {
        T.print();
        T.Empty();
        break;
      }		
      default: {
        cout << "Неверный ввод!" << endl;
        //system("pause");
        exit(1);
      }
		}
	} while (k != 4);
}



/*

void menu() {
	int k;
	do
	{
		cout << "Выберите операцию: \n1) вычислить дату через заданное кол-во дней; \n2) вычесть заданное кол-во дней из даты; \n3) определить високосность года; \n4) сравнить даты; \n5) вычислить кол-во дней между датами; \n6) закончить работу программы; \n";
		cin >> k;

		switch (k) {

		case 1: {
			Date d;
			cout << "Выберите способ инициализации даты: 1) числами; 2) строкой. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			cout << "Введите кол-во дней: "; unsigned int days; cin >> days;
			cout << "Старая дата: " << d.toString() << "    " << "Новая дата: " << d.addition_days(days).toString() << endl << endl;
			break;
		}
		case 2: {
			Date d;
			cout << "Выберите способ инициализации даты: 1) числами; 2) строкой. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			cout << "Введите кол-во дней: "; unsigned int days; cin >> days;
			cout << "Старая дата: " << d.toString() << "    " << "Новая дата: " << d.subtraction_of_days(days).toString() << endl << endl;
			break;
		}
		case 3: {
			Date d;
			cout << "Выберите способ инициализации даты: 1) числами; 2) строкой. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d = data;
				break;
			}
			}
			if (d.isLeapYear()) cout << d.getYear() << " - високосный." << endl << endl;
			else cout << d.getYear() << " - не високосный." << endl << endl;
			break;
		}
		case 4: {
			Date d1, d2;
			cout << "Выберите способ инициализации первой даты: 1) числами; 2) строкой. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d1 = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d1 = data;
				break;
			}
			}
			cout << "Выберите способ инициализации второй даты: 1) числами; 2) строкой. \n"; int k2; cin >> k2;
			switch (k2) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d2 = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d2 = data;
				break;
			}
			}
			d1.date_comparison(d2);
			break;
		}
		case 5: {
			Date d1, d2;
			cout << "Выберите способ инициализации первой даты: 1) числами; 2) строкой. \n"; int k1; cin >> k1;
			switch (k1) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d1 = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d1 = data;
				break;
			}
			}
			cout << "Выберите способ инициализации второй даты: 1) числами; 2) строкой. \n"; int k2; cin >> k2;
			switch (k2) {
			case 1: {
				unsigned int Year, Month, Day;
				cout << "Введите год: "; cin >> Year;
				cout << "Введите месяц: "; cin >> Month;
				cout << "Введите день: "; cin >> Day;
				Date data(Year, Month, Day);
				d2 = data;
				break;
			}
			case 2: {
				string str;
				cout << "Введите дату: "; cin >> str;
				Date data(str);
				d2 = data;
				break;
			}
			}
			if (!d1.date_comparison(d2)) cout << "Кол-во дней между датами: " << d1.number_between_dates(d2) << endl << endl;
			else cout << "Кол-во дней между датами: " << d2.number_between_dates(d1) << endl << endl;
			break;
		}
		case 6: {
			cout << "Работа программы завершена." << endl;
			//system("pause");
			exit(1);
		}
		default: {
			cout << "Неверный ввод!" << endl;
			//system("pause");
			exit(1);
		}
		}
	} while (k != 6);
}

*/
