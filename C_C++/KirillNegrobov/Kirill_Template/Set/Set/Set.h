#pragma once

#include <assert.h>
#include <iostream>

using namespace std;

template <class T>
class Set {
private:
	int set_length;
	T* set_data;

	T& operator[](int index) {
		assert(index >= 0 && index < set_length);
		return set_data[index];
	}
public:
	//class Iterator;
	Set() {
		set_length = 0;
		set_data = nullptr;
	}

	~Set() {
		delete[] set_data;
	}

	/*
	Iterator begin() {
		return set_data;
	}

	Iterator end() {
		return set_data + set_length;
	}
	*/
	void Erase() {
		delete[] set_data;
		set_data = nullptr;
		set_length = 0;
	}

	int Find(T value) {
		for (int i = 0; i < set_length; i++) {
			if (set_data[i] == value) return i;
		}
		return -1;
	}

	void Print() {
		cout << "[";
		for (int i = 0; i < set_length; i++) {
			cout << set_data[i];
			if (i + 1 < set_length) cout << ", ";
		}
		cout << "]";
	}

	bool Insert(T value) {
		if (Find(value) == -1) {
			if (set_length == 0) {
				set_length++;
				set_data = (T*)malloc(set_length * sizeof(T));
			}
			else {
				T* temp = (T*)malloc(set_length * sizeof(T));
				memcpy((void*)temp, (const void*)set_data, sizeof(T) * set_length);
				set_data = (T*)realloc(set_data, (set_length + 1) * sizeof(T));
				memcpy((void*)set_data, (const void*)temp, sizeof(T) * set_length);
				set_length++;
				delete[] temp;
			}
			set_data[set_length - 1] = value;
			for (int i = 0; i < set_length - 1; i++) {
				for (int j = 0; j < set_length - i - 1; j++) {
					if (set_data[j] > set_data[j + 1]) {
						T temp = set_data[j];
						set_data[j] = set_data[j + 1];
						set_data[j + 1] = temp;
					}
				}
			}
			return true;
		}
		return false;
	}

	bool Delete(T value) {
		int i = Find(value);
		if (i != -1) {
			for (int j = i; j < set_length; j++) {
				set_data[j] = set_data[j + 1];
			}
			set_length--;
			T* temp = new T[set_length];
			memcpy((void*)temp, (const void*)set_data, sizeof(T) * set_length);
			set_data = (T*)realloc(set_data, set_length * sizeof(T));
			memcpy((void*)set_data, (const void*)temp, sizeof(T) * set_length);
			delete[] temp; temp = nullptr;
			return true;
		}
		return false;
	}

	void Objedinenie(Set<T>& set1, Set<T>& set2) {
		for (int i = 0; i < set1.set_length; i++) {
			this->Insert(set1.set_data[i]);
		}
		for (int i = 0; i < set2.set_length; i++) {
			this->Insert(set2.set_data[i]);
		}
	}

	void Peresechenie(Set<T>& set1, Set<T>& set2) {
		for (int i = 0; i < set1.set_length; i++) {
			if (set2.Find(set1.set_data[i]) != -1) {
				this->Insert(set1.set_data[i]);
			}
		}
	}

	void Vichitanie(Set<T>& set1, Set<T>& set2) {
		for (int i = 0; i < set1.set_length; i++) {
			if (set2.Find(set1.set_data[i]) == -1) {
				this->Insert(set1.set_data[i]);
			}
		}
	}

	bool IsContains(Set<T>& set) {
		for (int i = 0; i < set.set_length; i++) {
			if (this->Find(set.set_data[i] == -1)) return false;
		}
		return true;
	}

	friend ostream& operator<<(ostream& out, const Set<T>& set) {
		out << "[";
		for (int i = 0; i < set.set_length; i++) {
			out << set.set_data[i];
			if (i + 1 != set.set_length) out << ", ";
		}
		out << "]";
		return out;
	}

	/*
	class Iterator {
		T* cur;
	public:
		Iterator(T* first) : cur(first) {}

		T& operator+ (int n) { return *(cur + n); }
		T& operator- (int n) { return *(cur - n); }

		T& operator++ (int) { return *cur++; }
		T& operator++ () { return *++cur; }
		T& operator-- (int) { return *cur--; }
		T& operator-- () { return *--cur; }

		bool operator!= (const Iterator& it) { return cur != it.cur; }
		bool operator== (const Iterator& it) { return cur == it.cur; }
		T& operator* () { return *cur; }
	};
	*/
};

/*
template <typename T>
ostream& operator<< (ostream& out, const Set<T>& i) {
	out << Set<T>::set_data[i];
	return out;
}
*/