#include <iostream>
#include <iomanip>
 
void labyrinth (int** board, int x, int y, int k)
{
    board[x][y] = k;
    if (x > 0 && !board[x - 1][y])
        labyrinth(board, x - 1, y, k + 1);
    if (y < 5 && !board[x][y + 1])
        labyrinth(board, x, y + 1, k + 1);
    if (x < 5 && !board[x + 1][y])
        labyrinth(board, x + 1, y, k + 1);
    if (y > 0 && !board[x][y - 1])
        labyrinth(board, x, y - 1, k + 1);
}
 
int main()
{
    int** board = new int*[6];

    int x = 3, y = 3;
    std::cout <<"Start: (" <<x <<',' <<y <<")\n";
    labyrinth(board, x, y, 1);
    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 6; j++)
        {
            std::cout <<std::setw(3) <<board[i][j];
        }
        std::cout <<std::endl;
    }
}