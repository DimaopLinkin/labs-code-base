#include <iostream>
#include <locale>
#include <fstream>

using namespace std;

struct Book {
    string author;
    string name;
    int year;
    int pages_count;
    string speciality;
};

struct List {
    List *next;
    Book data;
};
List* sort(List *list){
    List *new_list = NULL;

    while (list != NULL)
    {
        List *node = list;
        list = list->next;

        if (new_list == NULL || node->data.speciality < new_list->data.speciality)
        {
            node->next = new_list;
            new_list = node;
        }
        else
        {
            List *current = new_list;
            while (current->next != NULL && !(node->data.speciality < current->next->data.speciality))
            {                   
                  current = current->next;
            }                

            node->next = current->next;
            current->next = node;
        }
    }
    return new_list;
}
List* add(List *list, Book *data){
    if (list != NULL) {
        List *current = list;
        while(current->next) {
            current = current->next;
        }
        List *newelem = new List;
        newelem->data = *data;
        newelem->next = NULL;
        current->next = newelem;
    }
    else {
        list = new List;
        list->data = *data;
        list->next = NULL;
    }
    return sort(list);
}
List* deleteElements(List *list, string speciality){
    if (list != NULL) {
        List *prev = NULL;
        List *current = list;
        while(current != NULL){
            if (current->data.speciality == speciality){
                if (prev != NULL){
                    prev->next = current->next;
                    delete current;
                    current = prev;
                }
                else {
                    list = current->next;
                    delete current;
                    current = list;
                }
            }
            if (current != NULL) {
                prev = current;
                current = current->next;
            }
        }
    }
    return list;
}
void print(List *list){
    if (list != NULL){
        List *current = list;
        int count = 1;
        while (current != NULL) {
            cout << count 
                << ". " << current->data.author 
                << ". " << current->data.name 
                << ". " << current->data.year 
                << ". " << current->data.pages_count
                << ". " << current->data.speciality << endl;
            current = current->next;
            count++;
        }
    }
    else {
        cout << "Список пуст" << endl;
    }
}

int main() {
    setlocale(LC_ALL, "RUS");
    ifstream file("file_1.txt");
    if (!file) {
        cout << "Файл не был открыт!\n";
        return -1;
    }
    List *list = NULL;
    Book *file_data = new Book;

    while (!file.eof()) {
        file >> file_data->author;
        file >> file_data->name;
        file >> file_data->year;
        file >> file_data->pages_count;
        file >> file_data->speciality;
        list = add(list, file_data);
    }

    cout << "\nСписок после добавления с сортировкой\n";
    print(list);

    cout << "Введите удаляемую специализацию ";
    string speciality;
    cin >> speciality;
    list = deleteElements(list, speciality);
    cout << "\nСписок после удаления\n";
    print(list);

    delete(list);
    file.close();
    return 0;    
}