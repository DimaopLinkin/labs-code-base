#include <iostream>
#include <locale>
#include <fstream>

using namespace std;

struct List {
    List *next;
    float data;
};
List* add(List *list, float data){
    if (list != NULL) {
        List *current = list;
        while(current->next) {
            current = current->next;
        }
        List *newelem = new List;
        newelem->data = data;
        newelem->next = NULL;
        current->next = newelem;
    }
    else {
        list = new List;
        list->data = data;
        list->next = NULL;
    }
    return list;
}
float find(List *list){
    if (list != NULL) {
        List *current = list;
        float sum = 0;
        if (current->next != NULL) {
            while (current->next != NULL) {
                sum += current->data;
                current = current->next;
            }
            sum += current->data;
        }
        current = list;
        if (current->next != NULL) {
            while (current->next != NULL) {
                if (current->data > sum) return current->data;
                current = current->next;
            }
            if (current->data > sum) return current->data;
        }
    }    
    return NULL;
}
void print(List *list){
    if (list != NULL){
        List *current = list;
        int count = 1;
        while (current != NULL) {
            cout << count 
                << ". " << current->data << endl;
            current = current->next;
            count++;
        }
    }
    else {
        cout << "Список пуст" << endl;
    }
}

int main() {
    setlocale(LC_ALL, "RUS");
    ifstream file("file_2.txt");
    if (!file) {
        cout << "Файл не был открыт!\n";
        return -1;
    }
    List *list = NULL;
    float file_data;

    while (!file.eof()) {
        file >> file_data;
        list = add(list, file_data);
    }

    cout << "\nСписок после добавления\n";
    print(list);
    float result = find(list);
    if (result != NULL) {
        cout << "Найден элемент, который больше суммы всех элементов: " << result << endl;
    }
    else {
        cout << "Не найдено элемента, который больше суммы всех элементов\n"; 
    }

    delete(list);
    file.close();
    return 0;    
}