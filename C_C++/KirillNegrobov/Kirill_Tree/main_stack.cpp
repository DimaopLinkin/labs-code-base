#include <iostream>
#include <iomanip>
#include <locale>
#include <fstream>

using namespace std;

struct Stack {
    int x;
    int y;
    Stack* next;
    bool operator== (Stack *other) {
        return this->x == other->x && this->y == other->y;
    }
};

Stack* push(Stack* stack, int x, int y) {
    Stack* new_stack = new Stack;
    new_stack->x = x;
    new_stack->y = y;
    if (stack) {
        new_stack->next = stack;
    }
    else {
        new_stack->next = NULL;
    }    
    return new_stack;
}

Stack* pop(Stack* stack) {
    if (stack->next) {
        Stack* next = stack->next;
        delete stack;
        return next;
    } else {
        return NULL;
    }
}

Stack* findStart(int** field, int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (field[i][j] == 0 && (i == 0 || i == n - 1 || j == 0 || j == m - 1)) {
                Stack* stack = push(NULL, i, j);
                return stack;
            }
        }
    }
    return NULL;
}

/// Функция поиска выхода
/// road - стек, в котором лежит путь
/// start - стек, в котором содержится ячейка старта
/// field - массив лабиринта
/// n - высота массива
/// m - ширина массива
Stack* findExit(Stack* road, Stack* start, int** field, int n, int m) {
    // Отметка о том, что клетка пройдена
    field[road->x][road->y] = 2;
    // Пока не добрались до края поля, и это поле не старт
    while ((road == start) || road->x != n - 1 && road->y != m - 1 && road->x != 0 && road->y != 0) {
        // Шаг вверх, если там 0
        if (road->x > 0 && field[road->x-1][road->y] == 0) {
            road = push(road, road->x-1, road->y);
            field[road->x][road->y] = 2;
        }
        // Иначе, шаг вниз, если там 0
        else if (road->x < n - 1 && field[road->x+1][road->y] == 0) {
            road = push(road, road->x+1, road->y);
            field[road->x][road->y] = 2;
        }
        // Иначе, шаг влево, если там 0
        else if (road->y > 0 && field[road->x][road->y-1] == 0) {
            road = push(road, road->x, road->y-1);
            field[road->x][road->y] = 2;
        }
        // Иначе, если там 0 шаг вправо
        else if (road->y < m - 1 && field[road->x][road->y+1] == 0) {
            road = push(road, road->x, road->y+1);
            field[road->x][road->y] = 2;
        }
        // Иначе (если шагать некуда), шаг назад
        else {
            road = pop(road);
            if (!road) return NULL;
        }
    }
    return road;
}

void print(Stack* road) {
    cout << "Данные из стека:\n";
    while (road) {
        cout << "(" << road->x + 1 << "; " << road->y + 1 << ")\n";
        road = pop(road);
    }
}

int main() {
    setlocale(LC_ALL, "RUS");

    ifstream file("field.txt");
    if (!file) {
        cout << "Файл не был открыт!\n";
        return -1;
    }

    int n = 0;
    int m = 0;
    string temp;
    // Подсчёт количества строк
    while (getline(file, temp)) {        
        n++;
    }
    // Количество столбцов
    m = temp.length() / 2 + 1;

    // Выделение памяти
    int** field = new int* [n];
    for (int i = 0; i < n; i++) {
        field[i] = new int[m];
    }

    // Возврат в начало файла и запись данных из файла в массив
    file.seekg(ios_base::beg);
    int x = 0;
    int z = 0;
    while (!file.eof()) {
        file >> field[x][z];
        z++;
        if (z == 6) {
            x++;
            z = 0;
        }
    }

    // Вывод лабиринта на экран
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cout << setw(3) << field[i][j];
        }
        cout << endl << endl;
    }

    // Поиск выхода
    Stack* stack = findStart(field, n, m);
    cout << "Начало лабиринта: (" << stack->x + 1 << "; " << stack->y + 1 << ")" << endl;

    // Поиск выхода и вывод пути на экран
    stack = findExit(stack, stack, field, n, m);
    if (stack) {
        print(stack);
    }
    else {
        cout << "Выход не найден\n";
    }

    return 0;
}