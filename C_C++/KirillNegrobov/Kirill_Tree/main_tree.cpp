#include <iostream>
#include <locale>

using namespace std;

struct Node {
    int key;
    Node* left;
    Node* right;
};

Node* create(int key, int n) {
    if (n > 0) {
        Node* node = new Node;
        node->key = key;
        node->left = create(key + 1, n - 1);
        node->right = create(key + 1, n - 1);
        return node;
    } else {
        return NULL;
    }        
}

void print(Node *node, int level) {
    if (node) {
        print(node->left, level + 1);
        for (int i = 0; i < level; i++) cout<< "   ";
        cout << node->key << endl;
        print(node->right, level + 1);
    }
}

void freeNode(Node* node) {
    if (node) {
        freeNode(node->left);
        freeNode(node->right);
        delete node;
    }
}

int main() {
    setlocale(LC_ALL, "RUS");
    int n;

    cout << "Введите высоту дерева n: ";
    cin >> n;

    Node* tree = create(1, n);
    print(tree, 0);    
    
    freeNode(tree);

    return 0;
}
