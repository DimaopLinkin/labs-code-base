#include <iostream>
#include <iomanip>
#include <locale>
#include <fstream>
#include <string>

using namespace std;

struct Stack {
	int x;
	int y;
	Stack* next;
	bool operator== (Stack* other) {
		return this->x == other->x && this->y == other->y;
	}
};

Stack* push(Stack* stack, int x, int y) {
	Stack* new_stack = new Stack;
	new_stack->x = x;
	new_stack->y = y;
	if (stack) {
		new_stack->next = stack;
	}
	else {
		new_stack->next = NULL;
	}
	return new_stack;
}

Stack* pop(Stack* stack) {
	if (stack->next) {
		Stack* next = stack->next;
		delete stack;
		return next;
	}
	else {
		return NULL;
	}
}

Stack* findStart(int** field, int n, int m) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (field[i][j] == 0 && (i == 0 || i == n - 1 || j == 0 || j == m - 1)) {
				Stack* stack = push(NULL, i, j);
				return stack;
			}
		}
	}
	return NULL;
}

/// ������� ������ ������
/// road - ����, � ������� ����� ����
/// start - ����, � ������� ���������� ������ ������
/// field - ������ ���������
/// n - ������ �������
/// m - ������ �������
Stack* findExit(Stack* road, Stack* start, int** field, int n, int m) {
	// ������� � ���, ��� ������ ��������
	field[road->x][road->y] = 2;
	// ���� �� ��������� �� ���� ����, � ��� ���� �� �����
	while ((road == start) || road->x != n - 1 && road->y != m - 1 && road->x != 0 && road->y != 0) {
		// ��� �����, ���� ��� 0
		if (road->x > 0 && field[road->x - 1][road->y] == 0) {
			road = push(road, road->x - 1, road->y);
			field[road->x][road->y] = 2;
		}
		// �����, ��� ����, ���� ��� 0
		else if (road->x < n - 1 && field[road->x + 1][road->y] == 0) {
			road = push(road, road->x + 1, road->y);
			field[road->x][road->y] = 2;
		}
		// �����, ��� �����, ���� ��� 0
		else if (road->y > 0 && field[road->x][road->y - 1] == 0) {
			road = push(road, road->x, road->y - 1);
			field[road->x][road->y] = 2;
		}
		// �����, ���� ��� 0 ��� ������
		else if (road->y < m - 1 && field[road->x][road->y + 1] == 0) {
			road = push(road, road->x, road->y + 1);
			field[road->x][road->y] = 2;
		}
		// ����� (���� ������ ������), ��� �����
		else {
			road = pop(road);
			if (!road) return NULL;
		}
	}
	return road;
}

void print(Stack* road) {
	cout << "������ �� �����:\n";
	while (road) {
		cout << "(" << road->x + 1 << "; " << road->y + 1 << ")\n";
		road = pop(road);
	}
}

int main() {
	setlocale(LC_ALL, "RUS");

	ifstream file("field.txt");
	if (!file) {
		cout << "���� �� ��� ������!\n";
		return -1;
	}

	int n = 0;
	int m = 0;
	string temp;
	// ������� ���������� �����
	while (getline(file, temp)) {
		n++;
	}
	// ���������� ��������
	m = temp.length() / 2 + 1;

	// ��������� ������
	int** field = new int* [n];
	for (int i = 0; i < n; i++) {
		field[i] = new int[m];
	}

	// ������� � ������ ����� � ������ ������ �� ����� � ������
	file.close();

	ifstream file2("field.txt");
	int x = 0;
	int z = 0;
	int xxx;
	while (!file2.eof()) {
		if (x == n) break;
		file2 >> field[x][z];
		z++;
		if (z == m) {
			x++;
			z = 0;
		}
	}
	file2.close();

	// ����� ��������� �� �����
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << setw(3) << field[i][j];
		}
		cout << endl << endl;
	}

	// ����� ������
	Stack* stack = findStart(field, n, m);
	cout << "������ ���������: (" << stack->x + 1 << "; " << stack->y + 1 << ")" << endl;

	// ����� ������ � ����� ���� �� �����
	stack = findExit(stack, stack, field, n, m);
	if (stack) {
		print(stack);
	}
	else {
		cout << "����� �� ������\n";
	}

	system("PAUSE");

	return 0;
}