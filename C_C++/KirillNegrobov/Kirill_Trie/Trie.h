#pragma once
#include <string>
#include <iostream>

using namespace std;

class Trie
{

	struct Node
	{
		char ch;
		Node* sibling;
		Node* child;
		bool leaf;
	};
	Node* root;

	void insertNode(Node*& pNode, char ch_)
	{
		pNode = new Node;
		pNode->ch = ch_;
		pNode->child = NULL;
		pNode->sibling = NULL;
	}

	void printTree(Node* trie, int h)
	{
		if (trie != NULL)
		{
			printTree(trie->sibling, h + 1);
			for (int i = 0; i < h; i++)
				cout << " ";
			cout << trie->ch << "\n";
			printTree(trie->child, h + 1);
			
			
		}
	}

	void deleteTree(Node*& root)
	{
		if (root)
		{
			if (root->child)
				deleteTree(root->child);
			else if (root->sibling)
				deleteTree(root->sibling);
			delete root;
		}
	}


	
	
	
	
	void DeleteOdd(Node*& Tree, string s)
	{

		bool flag;
		{
			if (Tree == NULL)
			{
				exit;
			}
			flag = false;
			if (Tree->leaf && s.size() % 2 == 0) {
				Tree->leaf = false;
				flag = true;
			}

			if (Tree != NULL) {
				DeleteOdd(Tree, s + Tree->ch);
				flag = false;
			}
			if (flag)
			{
				delete Tree;
				
			}
		}
	}






public:
	Trie()
	{
		root = new Node;
		root->child = 0;
		root->sibling = 0;
	}

	Trie(char ch_)
	{
		root = new Node;
		root->ch = ch_;
		root->sibling = NULL;
		root->child = NULL;
	}
	~Trie()
	{
		delete root->child;
		delete root->sibling;
	}


	void insert(string word) {
		Node* p = this->root;
		for (int i = 0; i < word.size(); i++)
		{
			if (p->child == NULL)
			{
				insertNode(p->child, word[i]);
				p = p->child;
			}
			else
			{
				if (p->child->ch == word[i])
				{
					p = p->child;
				}
				else
				{
					p = p->child;
					while (p->sibling && p->sibling->ch != word[i])
						p = p->sibling;

					if (!p->sibling)
						insertNode(p->sibling, word[i]);
					p = p->sibling;
				}
			}
		}
		if (p->child == NULL)
			insertNode(p->child, '$');
		else
		{
			p = p->child;

			while (p->sibling && p->sibling->ch != '$')
				p = p->sibling;

			if (p->ch != '$')
				insertNode(p->sibling, '$');
		}
	}



	void print()
	{
		printTree(root, 0);
	}


	void task()
	{
		
		DeleteOdd(root, "");
	}


};