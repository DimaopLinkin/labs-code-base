#include <iostream>
#include <fstream>

using namespace std;

// ������� ���������� ����� ������ ����� �������
// � �������� ���������� � ������� ��������� ��������� �� ������ � ��� ������
// ���������� ����� �����
int chetSum(int* mas, int size) {
	int sum = 0;
	for (int i = 0; i < size; i++) {
		if (mas[i] % 2 == 0) sum += mas[i];
	}
	return sum;
}


// ������� ���������� ����� �����, ������� ����� ����������� � ������������ ����������
// � �������� ���������� � ������� ��������� ��������� �� ������ � ��� ������
// ���������� ����� �����
int minMaxSum(int* mas, int size) {
	int min = 0;
	int max = 0;
	// ����� ������������ � ������������� ��������
	for (int i = 0; i < size; i++) {
		if (mas[min] > mas[i]) min = i;
		if (mas[max] < mas[i]) max = i;
	}
	// ���� ����������� ������� ��������� ����� �������������, �������� ������� �������
	if (min > max) swap(min, max);
	// ������������ ��������� ����� min � max
	int sum = 0;
	// ���� ����� ��������� � ���������� ���� ���� �� 1 ������� (0 - 4, 19 - 5, 99 - 6; 6-4=2)
	if (max - min >= 2) {
		// ����� ����������� ��� ��������, �� min �� max �� ������������
		for (int j = min + 1; j < max; j++) sum += mas[j];
	}
	return sum;
}

// ������� ���������� ������� �� �����������
// � �������� ���������� � ������� ���������� ��������� �� ������ ��� ���������� � ��� ������
// �� ���������� ��������
void sortMas(int* sorted_mas, int size) {
	// ���������� �������
	int temp;
	for (int i = 0; i < size - 1; i++) {
		for (int j = 0; j < size - i - 1; j++) {
			if (sorted_mas[j] > sorted_mas[j + 1]) {
				temp = sorted_mas[j];
				sorted_mas[j] = sorted_mas[j + 1];
				sorted_mas[j + 1] = temp;
			}
		}
	}
}

int main() {
	// ��������� �������� ����� � �������
	setlocale(LC_ALL, "russian");

	// �������� �����
	ifstream input_file("input.txt");
	if (!input_file) {
		cout << "���� �� ��� ������!\n";
		return -1;
	}

	// ���������� ������� �� �����
	int file_data;
	int size = 0;
	int* mas = (int*)malloc(size * sizeof(int));
	while (!input_file.eof()) {
		mas = (int*)realloc(mas, (size + 1) * sizeof(int));
		input_file >> file_data;
		mas[size] = file_data;
		size++;
	}
	// �������� �����
	input_file.close();

	// ���������� ����� ������ �����
	int sum_chet = chetSum(mas, size);

	// ���������� ����� ����� ����� ����������� � ������������
	int sum_min_max = minMaxSum(mas, size);

	// ����������� ��������� ������� � ������ ��� ���������� ����������
	int* sorted_mas = (int*)malloc((size) * sizeof(int));
	for (int j = 0; j < size; j++) sorted_mas[j] = mas[j];

	// ���������� ������� �� �����������
	sortMas(sorted_mas, size);

	// �������� ����� ��� ������
	ofstream output_file("output.txt");
	if (!output_file) {
		cout << "���� �� ��� ������!\n";
		return -1;
	}

	// ������ ����������� � ����
	output_file << "����� ������ ��������� ������� = " << sum_chet << endl;
	output_file << "����� ��������� ����� min � max = " << sum_min_max << endl;
	output_file << "��������������� ������:\n";
	for (int j = 0; j < size; j++) output_file << sorted_mas[j] << " ";

	// �������� �����
	output_file.close();

	// ������� ������
	free(mas);
	free(sorted_mas);
	system("PAUSE");
	return 0;
}