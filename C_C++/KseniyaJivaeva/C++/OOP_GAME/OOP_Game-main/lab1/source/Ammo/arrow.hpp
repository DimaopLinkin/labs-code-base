#ifndef AMMO_H
#define AMMO_H

#include <SFML/Graphics.hpp>
#include "../Animation/animation.hpp"
#include "../Field/field.h"
#include "../Entity/entity.hpp"

class Arrow: public Entity
{
public:
    Arrow(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type, int DMG):
        Entity(field,a,x,y,DMG,10,type)
    {
        this->dir = dir;
        option("arrow", 0.3, 10, "move");
        if (this->dir==1) dx=-0.3;
    }

    void update(float time)
    {
        if (dir) anim->flip();
        x+=dx*time;

        for (int i = y/32; i<(y+h)/32; i++)
            for (int j = x/32; j<(x+w)/32; j++)
                if (field->tiles[j][i].getType() != AIR)
                {
                    dx=0; life=false; health = 0;
                }

        anim->tick(time);
    }
};

#endif //AMMO_H