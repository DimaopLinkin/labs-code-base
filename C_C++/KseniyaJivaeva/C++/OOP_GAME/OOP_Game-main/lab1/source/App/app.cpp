#include "app.h"

App::App()
{
    initField();
    initWindow();
    initView();
    initRules();
    initEntities();
}

App::~App()
{
    delete window;
    delete field;
    delete view;
    delete player;
    delete logger;
    delete anims["player"];
    delete anims["princess"];
    delete anims["chest"];
    delete anims["heart"];
    delete anims["key"];
    delete anims["small"];
    delete anims["medium"];
    delete anims["dragon"];
    delete anims["arrow"];
    delete anims["iceball"];
    delete anims["fireball"];
    delete game;
}

void App::start()
{
    logger = new Logger(true, false);
    while (window->isOpen())
    {
        updateEvents();
        updateLogic();
        render();
    }
}

void App::initWindow()
{
    if (!field)
        throw std::runtime_error("Field was not initialized before window"); 
    window = new sf::RenderWindow(sf::VideoMode(field->getWidth()*32, field->getHeight()*32), "The Game");
    window->setSize(sf::Vector2u(192*15, 64*15));
    window->setFramerateLimit(60);
}

void App::initField()
{
    FieldBuilder fieldBuilder;
    fieldBuilder.loadFromFile("../settings/tilemap.txt");
    fieldBuilder.placeHero();
    fieldBuilder.placeEnemies();
    fieldBuilder.placeItems();
    fieldBuilder.build();
    field = fieldBuilder.getResult();
}

void App::initView()
{
    if (!field)
        throw std::runtime_error("Field was not initialized before view"); 
    view = new View(field);
    view->loadFromFile("../images/tiles_001.png", sf::Vector2u(32u, 32u));

}

void App::initRules()
{
    easy = Easy();
    game = new Game<Easy>(easy);
}

void App::initEntities()
{
    sf::Texture t;
    t.loadFromFile("../images/player/goblinT.png");
    textures["player"] = t;
    anims["player"] = new AnimationManager();
    anims["player"]->create("stay", textures["player"], 17, 17, 30, 30, 1, 0.01);
    anims["player"]->create("shoot", textures["player"], 17, 17+64, 30, 30, 3, 0.01, 64);
    anims["player"]->create("jump", textures["player"], 17, 17+128, 30, 30, 4, 0.01, 64);
    anims["player"]->create("run", textures["player"], 17, 17+192, 30, 30, 8, 0.01, 64);
    anims["player"]->create("die", textures["player"], 15, 19+256, 30, 30, 5, 0.01, 64, false);

    t.loadFromFile("../images/princess/princess.png");
    textures["princess"] = t;
    anims["princess"] = new AnimationManager();
    anims["princess"]->create("sleep", textures["princess"], 21, 23, 32, 32, 3, 0.003, 72);
    anims["princess"]->create("win", textures["princess"], 21, 23+72, 32, 32, 4, 0.01, 72);

    t.loadFromFile("../images/items/chest.png");
    textures["chest"] = t;
    anims["chest"] = new AnimationManager();
    anims["chest"]->create("active", textures["chest"], 2, 7, 28, 17, 5, 0.01, 32);
    anims["chest"]->create("picked", textures["chest"], 0, 0, 1, 1, 1, 0.01, 1);

    t.loadFromFile("../images/items/heart.png");
    textures["heart"] = t;
    anims["heart"] = new AnimationManager();
    anims["heart"]->create("active", textures["heart"], 6, 7, 19, 17, 1, 0.01, 0);
    anims["heart"]->create("picked", textures["heart"], 0, 0, 1, 1, 1, 0.01, 1);

    t.loadFromFile("../images/items/key.png");
    textures["key"] = t;
    anims["key"] = new AnimationManager();
    anims["key"]->create("active", textures["key"], 3, 9, 25, 14, 1, 0.01, 0);
    anims["key"]->create("picked", textures["key"], 0, 0, 1, 1, 1, 0.01, 1);

    t.loadFromFile("../images/ammo/arrow.png");
    textures["arrow"] = t;
    anims["arrow"] = new AnimationManager();
    anims["arrow"]->create("move", textures["arrow"], 4, 12, 23, 7, 1, 0.01);

    t.loadFromFile("../images/ammo/fireball.png");
    textures["fireball"] = t;
    anims["fireball"] = new AnimationManager();
    anims["fireball"]->create("move", textures["fireball"], 5, 9, 19, 14, 2, 0.01, 32);

    t.loadFromFile("../images/ammo/iceball.png");
    textures["iceball"] = t;
    anims["iceball"] = new AnimationManager();
    anims["iceball"]->create("move", textures["iceball"], 5, 9, 19, 14, 2, 0.01, 32);

    t.loadFromFile("../images/enemy_barbarian/barbarian_walk.png");
    textures["small"] = t;
    anims["small"] = new AnimationManager();
    anims["small"]->create("walk", textures["small"], 17, 17, 30, 30, 8, 0.01, 64);
    anims["small"]->create("die", textures["small"], 17, 17+64, 30, 30, 5, 0.01, 64, false);

    t.loadFromFile("../images/enemy_garg/gargoyle.png");
    textures["medium"] = t;
    anims["medium"] = new AnimationManager();
    anims["medium"]->create("shoot", textures["medium"], 18, 18, 34, 34, 3, 0.005, 72);
    anims["medium"]->create("die", textures["medium"], 18, 18+72, 34, 34, 8, 0.01, 72, false);
    anims["medium"]->create("walk", textures["medium"], 18, 18+144, 34, 34, 4, 0.01, 72);

    t.loadFromFile("../images/enemy_dragon/dragon.png");
    textures["dragon"] = t;
    anims["dragon"] = new AnimationManager();
    anims["dragon"]->create("shoot", textures["dragon"], 5, 12, 71, 55, 3, 0.005, 80);
    anims["dragon"]->create("die", textures["dragon"], 5, 12+80, 71, 55, 5, 0.01, 80, false);
    anims["dragon"]->create("walk", textures["dragon"], 5, 12+160, 71, 55, 4, 0.005, 80);

    player = new Player(field, anims["player"], 32, 256, 0, game->rule.armored, game->rule.haveKey, game->rule.playerHP, game->rule.playerDMG);
    entities.push_back(new Princess(field, anims["princess"], 32*28, 32*8, 0, "princess"));
    entities.push_back(new Item(field, anims["chest"], 32*14, 16+32*7, 1, "chest"));
    entities.push_back(new Item(field, anims["key"], 32*27, 16+32*4, 1, "key"));
    entities.push_back(new Item(field, anims["heart"], 32*18, 16+32*7, 1, "heart"));
    entities.push_back(new Barbarian(field, anims["small"], 256, 256, 1, "small", game->rule.barbarianHP));
    entities.push_back(new Gargoyle(field, anims["medium"], 32*25, 32*2, 1, "medium", game->rule.gargoyleHP, game->rule.gargoyleDMG));
    entities.push_back(new Dragon(field, anims["dragon"], 32*26, 32*8, 1, "boss", game->rule.dragonHP, game->rule.dragonDMG));
}

void App::updateEvents()
{
    while (window->pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window->close();
        if (event.type == sf::Event::KeyPressed)
            if (event.key.code==sf::Keyboard::Space)
            {
                if (player->armored)
                    entities.push_back(new Arrow(field,anims["arrow"],player->x+18,player->y+10,player->dir, "arrow", player->damage));
            }
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) player->key["A"]=true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) player->key["D"]=true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) player->key["W"]=true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) player->key["Space"]=true;
}

void App::updateLogic()
{
    float time = clock.getElapsedTime().asMilliseconds();
    clock.restart();

    for(it=entities.begin();it!=entities.end();)
    {
        Entity* e = *it;
        e->update(time);
        if (e->life==false) 
        {
            it = entities.erase(it);
            it++;
        }
        else it++;
    }
    player->update(time);

    logger->logging(player);
    for(it=entities.begin();it!=entities.end();it++)
    {   
        logger->logging(*it);
        if((*it)->name=="enemy")
        {
            Entity* enemy = *it;
            if(enemy->health<=0) continue;

            if(enemy->anim->currentAnim=="shoot")
            {
                int i = enemy->anim->animList["shoot"].currentFrame;
                if(!i) enemy->shooted = false;
                if(i == enemy->anim->animList["shoot"].frames.size()-1 && !enemy->shooted)
                {
                    if (enemy->type=="medium") 
                    {
                        entities.push_back(new Arrow(field,anims["iceball"],enemy->x+18,enemy->y+10,enemy->dir, "iceball", enemy->damage));
                        enemy->shooted = true;
                    }
                    if (enemy->type=="boss")
                    {
                        entities.push_back(new Arrow(field,anims["fireball"],enemy->x+18,enemy->y+10,enemy->dir, "fireball", enemy->damage));
                        enemy->shooted = true;
                    }
                }
            }

            if(player->getRect().intersects(enemy->getRect()))
                if(player->dy>0 && enemy->type == "small") {enemy->dx=0; player->dy=-0.2; enemy->health=0;}
                else if(enemy->type != "princess"){ player->health = 0; }

            for (std::list<Entity*>::iterator it2=entities.begin(); it2!=entities.end(); it2++)
            {
                Entity* arrow = *it2;
                if (arrow->name=="arrow")
                    if (arrow->health>0)
                        if (arrow->type == "arrow" && arrow->getRect().intersects(enemy->getRect()))
                        {
                            arrow->health=0; arrow->life=false; enemy->health -=5;
                        }
                        if (arrow->type == "fireball" && arrow->getRect().intersects(player->getRect()))
                        {
                            arrow->health=0; arrow->life=false; player->health -=10;
                        }
                        if (arrow->type == "iceball" && arrow->getRect().intersects(player->getRect()))
                        {
                            arrow->health=0; arrow->life=false; player->health -=5;
                        }
            }
        }
        if((*it)->name=="princess")
        {
            Entity* princess = *it;
            if(player->getRect().intersects(princess->getRect()) && player->haveKey)
            {
                princess->health=4000;
                player->health=4000;
            }
        }
        if((*it)->name=="item")
        {
            Entity* item = *it;
            if(item->health<=0) continue;

            if(item->getRect().intersects(player->getRect()))
            {
                if(item->type=="chest")
                {
                    player->armored = true;
                }
                if(item->type=="heart")
                {
                    player->health+=250;
                }
                if(item->type=="key")
                {
                    player->haveKey = true;
                }
                item->health = 0;
            }
        }
    }
}

void App::render()
{
    window->clear();

    window->draw(*view);

    for(it=entities.begin();it!=entities.end();it++)
        {
            (*it)->draw(window);
        }

    player->draw(window);

    window->display();
}
