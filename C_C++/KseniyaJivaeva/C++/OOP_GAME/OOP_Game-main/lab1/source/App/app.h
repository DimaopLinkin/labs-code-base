#ifndef APP_H
#define APP_H

#include <iostream>

#include <SFML/Graphics.hpp>

#include "../Field/field.h"
#include "../FieldBuilder/field_builder.h"
#include "../Player/player.h"
#include "../Enemy/barbarian.hpp"
#include "../Enemy/gargoyle.hpp"
#include "../Enemy/dragon.hpp"
#include "../Friendly/princess.hpp"
#include "../View/view.h"
#include "../Animation/animation.hpp"
#include "../Ammo/arrow.hpp"
#include "../Entity/entity.hpp"
#include "../Item/item.hpp"
#include "../Rules/rule.hpp"
#include "../Rules/game.hpp"
#include "../Logger/logger.hpp"

#include <list>

class App
{
public:
    App();
    ~App();

    void start();
    
private:
    void initWindow();
    void initField();
    void initView();
    void initRules();
    void initEntities();

    void updateEvents();
    void updateLogic();
    void render();

    sf::RenderWindow* window;
    sf::Event event;

    Game<Easy>* game;
    Easy easy;
    Medium medium;
    Hard hard;

    Field* field;
    View* view;
    std::map<std::string, AnimationManager*> anims;
    std::map<std::string, sf::Texture> textures;
    sf::Texture t;

    Logger* logger;

    Player* player;
    std::list<Entity*> entities;
    std::list<Entity*>::iterator it;
    sf::Clock clock;
};

#endif // APP_H