#ifndef BARBARIAN_H
#define BARBARIAN_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "enemy_base.hpp"

class Barbarian: public EnemyBase
{
public:
    Barbarian(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type, int HP):
        EnemyBase(field, a, x, y, 0, dir, type)
    {
        option("enemy", 0.05, HP);
    }
    void update(float time)
    {
        if (!life || health <= 0) STATE=die;
        if (STATE==walk) anim->set("walk");
        if (STATE==die) anim->set("die");

        if (dx>0)dir=0;
        if (dx<0)dir=1;
        if (dir) anim->flip();

        if (life && health>0) 
        {
            x += dx * time;
            collision(0);

            dy += 0.001 * time;
            y += dy * time;
            collision(1);
        } else
        {
            dy += 0.001 * time;
            y += dy * time;
            collision(1);
            
        }
        anim->tick(time);
    }
};

#endif //BARBARIAN_H