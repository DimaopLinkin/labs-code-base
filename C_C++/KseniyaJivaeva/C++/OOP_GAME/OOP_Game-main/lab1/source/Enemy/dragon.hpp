#ifndef DRAGON_H
#define DRAGON_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "enemy_base.hpp"

class Dragon: public EnemyBase
{
public:
    Dragon(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type, int HP, int DMG):
        EnemyBase(field, a, x, y, DMG, dir, type)
    {
        option("enemy", 0.025, HP);
    }

    void update(float time)
    {
        if (!life || health <= 0) STATE=die;
        if (STATE==walk) anim->set("walk");
        if (STATE==die) anim->set("die");
        if (STATE==shoot) anim->set("shoot");

        if (dx>0)dir=0;
        if (dx<0)dir=1;
        if (dir) anim->flip();

        if (life && health>0) 
        {
            if (!dy) dy -=0.004*time;
            x += dx * time;
            collision(0);

            dy += 0.0002 * time;
            y += dy * time;
            collision(1);
            if (x<=32*24) STATE=walk;
            int i = anim->animList["walk"].currentFrame;
            if (i == anim->animList["walk"].frames.size()-1) 
            { 
                STATE=shoot;
            }
        } else
        {
            dy += 0.001 * time;
            y += dy * time;
            collision(1);
            
        }
        anim->tick(time);
    }
};

#endif //DRAGON_H