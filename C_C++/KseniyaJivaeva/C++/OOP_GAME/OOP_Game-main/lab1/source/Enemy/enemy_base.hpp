#ifndef ENEMY_BASE_H
#define ENEMY_BASE_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "../Entity/entity.hpp"

class EnemyBase: public Entity
{
public:
    EnemyBase(Field* field, AnimationManager* a, int x, int y, int DMG, bool dir, std::string type) :
        Entity(field, a, x, y, DMG, 25, type)
    {
        this->type = type;
        this->dir = dir;
        shooted = false;
        STATE = walk;
        dy = 0.1;
    }

    enum {walk, die, shoot} STATE;

    virtual void update(float time) = 0;

protected:
    void collision(int dir)
    {
        for (int i = y/32; i < (y+h)/32; i++)
        for (int j = x/32; j < (x+w)/32; j++)
        {
            if (field->tiles[j][i].getType() != AIR)
            {
                if ((dx>0) && (dir == 0)) {x = j*32 - w; dx*=-1;}
                else if ((dx<0) && (dir == 0)) {x = j*32 + 32; dx*=-1;}
                if ((dy>0) && (dir == 1)) { y = i*32 - h; dy = 0;}
                if ((dy<0) && (dir == 1)) { y = i*32 + 32; dy = 0;}
            }
        }
    }
};

#endif //ENEMY_BASE_H