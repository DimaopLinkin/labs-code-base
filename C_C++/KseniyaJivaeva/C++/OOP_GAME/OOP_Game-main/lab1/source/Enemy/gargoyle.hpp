#ifndef GARGOYLE_H
#define GARGOYLE_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "enemy_base.hpp"

class Gargoyle: public EnemyBase
{
public:
    Gargoyle(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type, int HP, int DMG):
        EnemyBase(field, a, x, y, DMG, dir, type)
    {
        option("enemy", 0, HP);
    }

    void update(float time)
    {
        if (!life || health <= 0) STATE=die;
        if (STATE==walk) anim->set("walk");
        if (STATE==die) anim->set("die");
        if (STATE==shoot) anim->set("shoot");

        if (dx>0)dir=0;
        if (dx<0)dir=1;
        if (dir) anim->flip();

        if (life && health>0) 
        {
            collision(0);
            if (!dy && y > 2) dy -= 0.007 * time;
            if (dy < 0.01) { dy += 0.0001 * time; }
            y += dy * time;
            collision(1);
            if (y>0.8*32 && y<2*32)
            {
                STATE = shoot;
            }
            if ((y >=2*32 || y<=0.8*32) && shooted)
            {
                STATE = walk;
            }
        } else
        {
            dy += 0.001 * time;
            y += dy * time;
            collision(1);
            
        }
        anim->tick(time);
    }
};

#endif //GARGOYLE_H