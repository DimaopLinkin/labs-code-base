#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "../Rules/game.hpp"
#include <fstream>
#include <iostream>

class Entity
{
public:
    Field* field;
    float x,y,dx,dy,w,h;
    std::string name;
    int health;
    int damage;
    AnimationManager* anim;
    bool life, dir, shooted;
    std::string type;

    Entity(Field* field, AnimationManager* a, int x, int y, int DMG, int health = 0, std::string type = "entity", bool shooted = false) : 
        field(field), anim(a), x(x), y(y), health(health), type(type), damage(DMG)
    {
        dir = 0;
        life = true;
        dx=dy=0;
    }

    sf::FloatRect getRect()
    {
        return sf::FloatRect(x,y,w,h);
    }

    virtual void update(float time) = 0;

    void draw(sf::RenderWindow* window)
    {
		anim->draw(window,x,y+h);
	}

    void option(std::string NAME, float SPEED=0, int HEALTH=10, std::string FIRST_ANIM="")
	{
		name = NAME;
		if (FIRST_ANIM!="") anim->set(FIRST_ANIM);
		w = anim->getW();
		h = anim->getH();
		dx = SPEED;
		health = HEALTH;
	}
    friend std::ostream& operator<< (std::ostream &out, const Entity &entity)
    {
        out << "Entity " << entity.name << "; " << entity.type << "; x: " << entity.x << "; y: " << entity.y << "; HP: " << entity.health;
 
        return out;
    }
        friend std::ofstream& operator<< (std::ofstream &fout, const Entity &entity)
    {
        fout << "Entity " << entity.name << "; " << entity.type << "; x: " << entity.x << "; y: " << entity.y << "; HP: " << entity.health;
 
        return fout;
    }
    friend bool operator== (const Entity &e1, const Entity &e2)
    {
        return e1.name == e2.name && e1.type == e2.type && e1.x == e2.x && e1.y == e2.y && e1.health == e2.health;
    }
};

#endif // ENTITY_H