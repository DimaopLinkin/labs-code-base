#ifndef PRINCESS_H
#define PRINCESS_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "../Entity/entity.hpp"

class Princess: public Entity
{
public:
    Princess(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type) : 
        Entity(field, a, x, y, 0, 25, type)
    {
        this->type = type;
        this->dir = dir;
        shooted = false;
        STATE = sleep;
        dy = 0.1;
        option("princess", 0, 30, "sleep");
    }

    enum {sleep, win} STATE;

    void update(float time)
    {
        if (STATE==win) anim->set("win");
        if (STATE==sleep) anim->set("sleep");

        if (dx>0)dir=0;
        if (dx<0)dir=1;
        if (dir) anim->flip();

        if (life && health>0) 
        {
            if (health==4000) 
            {
                anim->set("win");
                if (y==258)dy=-0.4;
                collision(0);

                dy += 0.001 * time;
                y += dy * time;
                collision(1);
            }
            else
            {
                anim->set("sleep");
                collision(0);

                dy += 0.001 * time;
                y += dy * time;
                collision(1);
            }
        } else
        {
            dy += 0.001 * time;
            y += dy * time;
            collision(1);
            
        }
        anim->tick(time);
    }

    void collision(int dir)
    {
        for (int i = y/32; i < (y+h)/32; i++)
            for (int j = x/32; j < (x+w)/32; j++)
            {
                if (field->tiles[j][i].getType() != AIR)
                {
                    if ((dx>0) && (dir == 0)) {x = j*32 - w; dx*=-1;}
                    else if ((dx<0) && (dir == 0)) {x = j*32 + 32; dx*=-1;}
                    if ((dy>0) && (dir == 1)) { y = i*32 - h; dy = 0;}
                    if ((dy<0) && (dir == 1)) { y = i*32 + 32; dy = 0;}
                }
            }
    }
};
#endif //PRINCESS_H