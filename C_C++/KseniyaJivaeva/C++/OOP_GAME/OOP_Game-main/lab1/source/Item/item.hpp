#ifndef ITEM_H
#define ITEM_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Entity/entity.hpp"
#include "../Animation/animation.hpp"

class Item:public Entity
{
public:
    Item(Field* field, AnimationManager* a, int x, int y, bool dir, std::string type):
        Entity(field,a,x,y,0,10,type)
    {
        option("item", 0, 10, "active");
    }

    void update(float time)
    {
        anim->tick(time);
        if (!health) anim->set("picked");
    }


};

#endif // ITEM_H