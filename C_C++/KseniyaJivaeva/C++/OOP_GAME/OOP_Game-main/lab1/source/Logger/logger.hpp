#ifndef LOGGER_H
#define LOGGER_H

#include "../Entity/entity.hpp"

#include <list>
#include <iostream>
#include <fstream>

class Logger
{
public:
    Logger(bool inFile, bool inConsole) : inFile(inFile), inConsole(inConsole)
    {
    }
    ~Logger()
    {
    }
    bool inFile;
    bool inConsole;
    void logging(Entity* entity)
    {
        if (entity->name=="player" || entity->type=="small" || entity->type=="medium" || entity->type=="boss")
        {
            if (inFile) {std::ofstream fout("log.txt", std::ios_base::app); fout << *entity << std::endl; fout.close();}
            if (inConsole) std::cout << *entity << std::endl;
        }
    }
};

#endif // LOGGER_H