#include "player.h"

Player::Player(Field* field, AnimationManager* a, int x, int y, bool dir, bool armored, bool haveKey, int HP, int DMG) : 
    Entity(field, a, x, y, DMG, HP)
{
    this->dir = dir;
    this->haveKey = haveKey;
    this->armored = armored;
    option("player", 0, HP);
    STATE = stay;
    hit = false;
}

void Player::keyCheck()
{
    if (key["A"])
    {
        dir=1;
        dx=-0.1;
        if (STATE==stay) STATE=run;
    }
    if (key["D"])
    {
        dir=0;
        dx=0.1;
        if (STATE==stay) STATE=run;
    }
    if (key["W"])
    {
        if (dy==0)
        {
            dy=-0.4;
            STATE=jump;
        }
    }
    if (key["Space"])
    {
        shoot=true;
    }
    if (!(key["A"] || key["D"]))
    {
        dx=0;
    }
    if (!key["Space"])
    {
        shoot=false;
    }
}

void Player::update(float time)
{
    
    if (!life || health <= 0) STATE=die;
    if (STATE==die) anim->set("die");
    if (life && health>0) 
    {
        keyCheck();
        if (STATE==stay) anim->set("stay");
        if (STATE==run) anim->set("run");
        if (STATE==jump) anim->set("jump");
        if (shoot) anim->set("shoot");
        if (dir) anim->flip();

        
        x += dx * time;
        collision(0);

        dy += 0.001 * time;
        y += dy * time;
        collision(1);
    }
    else
    {
        dy += 0.001 * time;
        y += dy * time;
        collision(1);
    }
        anim->tick(time);

    key["A"]=key["W"]=key["D"]=key["Space"]=false;

}

void Player::collision(int dir)
{
    for (int i = y/32; i < (y+h)/32; i++)
        for (int j = x/32; j < (x+w)/32; j++)
        {
            if (field->tiles[j][i].getType() != AIR)
            {
                if ((dx>0) && (dir == 0)) x = j*32 - w;
                if ((dx<0) && (dir == 0)) x = j*32 + 32;
                if ((dy>0) && (dir == 1)) { y = i*32 - h; dy = 0; STATE = stay;}
                if ((dy<0) && (dir == 1)) { y = i*32 + 32; dy = 0;}
            }
        }
}