#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>
#include "../Field/field.h"
#include "../Animation/animation.hpp"
#include "../Entity/entity.hpp"

class Player: public Entity
{
public:
    Player(Field* field, AnimationManager* a, int x, int y, bool dir, bool armored, bool haveKey, int HP, int DMG);

    bool shoot, hit, armored, haveKey;
    std::map<std::string,bool> key;
    enum {stay, run, jump, die} STATE;
    void keyCheck();

    void update(float time);
    
private:
    void collision(int dir);

};

#endif // PLAYER_H