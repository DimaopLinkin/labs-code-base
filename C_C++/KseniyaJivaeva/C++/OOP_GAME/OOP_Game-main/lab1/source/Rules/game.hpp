#ifndef GAME_H
#define GAME_H

template <class T>
class Game
{
public:
    Game(T rule) : rule(rule)
    {}

    T rule;

    bool isWin()
    {
        rule.endOfGame();
    }
};

#endif //GAME_H