#ifndef RULE_H
#define RULE_H

class Easy
{
public:
    Easy()
    {
    armored = true;
    haveKey = true;
    win = false;
    playerHP = 250;
    barbarianHP = 50;
    gargoyleHP = 40;
    dragonHP = 80;
    playerDMG = 20;
    gargoyleDMG = 10;
    dragonDMG = 20;
    heartHP = 100;
    }
    bool armored, haveKey, win;
    int playerHP, barbarianHP, gargoyleHP, dragonHP;
    int playerDMG, gargoyleDMG, dragonDMG;
    int heartHP;

    bool endOfGame()
    {
        return win;
    }
    
};

class Medium
{
    public:
    Medium()
    {
    armored = true;
    haveKey = false;
    win = false;
    playerHP = 200;
    barbarianHP = 100;
    gargoyleHP = 80;
    dragonHP = 160;
    playerDMG = 10;
    gargoyleDMG = 20;
    dragonDMG = 40;
    heartHP = 80;
    }
    bool armored, haveKey, win;
    int playerHP, barbarianHP, gargoyleHP, dragonHP;
    int playerDMG, gargoyleDMG, dragonDMG;
    int heartHP;

    bool endOfGame()
    {
        return win;
    }
};

class Hard
{
    public:
    Hard()
    {
    armored = false;
    haveKey = false;
    win = false;
    playerHP = 100;
    barbarianHP = 150;
    gargoyleHP = 140;
    dragonHP = 220;
    playerDMG = 10;
    gargoyleDMG = 40;
    dragonDMG = 50;
    heartHP = 60;
    }
    bool armored, haveKey, win;
    int playerHP, barbarianHP, gargoyleHP, dragonHP;
    int playerDMG, gargoyleDMG, dragonDMG;
    int heartHP;

    bool endOfGame()
    {
        return win;
    }
};

#endif //RULE_H