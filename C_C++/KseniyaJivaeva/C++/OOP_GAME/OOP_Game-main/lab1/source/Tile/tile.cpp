#include "tile.h"

Tile::Tile(TileType type)
    : type(type) {}

Tile& Tile::operator=(const Tile& other)
{
    type = other.getType();
    return *this;
}

TileType Tile::getType() const
{
    return type;
}