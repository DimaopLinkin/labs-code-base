#ifndef TILE_H
#define TILE_H

enum TileType {WALL, AIR, ENTRANCE, EXIT};

class Tile
{
public:
    Tile(TileType type = WALL);

    Tile& operator=(const Tile&); 

    TileType getType() const;

private:
    TileType type;
};

#endif // TILE_H