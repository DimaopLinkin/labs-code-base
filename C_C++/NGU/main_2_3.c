#include <stdio.h>
#include <cstring>
#include <cstdlib>

typedef struct _Queue
{
    int size; // размер массива
    int first; // номер первого элемента в очереди
    int leng; // длина очереди
    int* arr; // указатель на начало массива
} Queue;

Queue* create()
{
    Queue *q = (Queue*)malloc(sizeof(Queue));
    q->first = 0;
    q->leng = 0;
    q->size = 1;
    q->arr = (int*)malloc(sizeof(int)*q->size);
    return q;
}

int empty(Queue* q)
{
    return (q->leng == 0);
}

void enqueue(Queue* q, int a)
{
    if(q->leng == q->size)
    {
        q->arr = (int*)realloc(q->arr, sizeof(int)* q->size * 2);
        if (q->first > 0)
            memcpy(q->arr + q->size, q->arr, (q->size - q->first) * sizeof(int));
        q->size *= 2;
    }
    q->arr[(q->first + q->leng++) % q->size] = a;
}

int dequeue(Queue* q)
{
    int a = q->arr[q->first++];
    q->first %= q->size;
    q->leng--;
    return a;
}

int main() 
{
    Queue* q = create();
    int N, M; // длина очереди, количество управляющих элементов
    scanf("%d", &N);
    scanf("%d", &M);
    while (M > 0)
    {
        int input;
        scanf("%d", &input);
        M--;
        if (input > 0)
        {
            for (int i = 0; i < input; i++)
            {                
                int R;
                scanf("%d", &R);
                M--;
                if (q->leng == N)
                {
                    printf("Memory overflow\n");
                    break;
                }
                enqueue(q, R);
            }
        }
        else if (input < 0)
        {
            int sum = 0;
            for (int i = 0; i < -input; i++)
            {
                if (empty(q))
                {
                    printf("Empty queue\n");
                    break;
                }
                sum += dequeue(q);
                if (i + 1 == -input)
                    printf("%d\n", sum / i);
            }
        }
    }
    return 0;
}