#pragma once
#include "Resh.h"

namespace Lab1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ button1;
	protected:
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Label^ label10;
	private: System::Windows::Forms::TextBox^ txtx1;
	private: System::Windows::Forms::TextBox^ txty1;
	private: System::Windows::Forms::TextBox^ txtx2;
	private: System::Windows::Forms::TextBox^ txty2;
	private: System::Windows::Forms::TextBox^ txtx3;
	private: System::Windows::Forms::TextBox^ txty3;
	private: System::Windows::Forms::TextBox^ txtPttr;
	private: System::Windows::Forms::TextBox^ txtPltr;










	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->txtx1 = (gcnew System::Windows::Forms::TextBox());
			this->txty1 = (gcnew System::Windows::Forms::TextBox());
			this->txtx2 = (gcnew System::Windows::Forms::TextBox());
			this->txty2 = (gcnew System::Windows::Forms::TextBox());
			this->txtx3 = (gcnew System::Windows::Forms::TextBox());
			this->txty3 = (gcnew System::Windows::Forms::TextBox());
			this->txtPttr = (gcnew System::Windows::Forms::TextBox());
			this->txtPltr = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(540, 187);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(84, 31);
			this->button1->TabIndex = 0;
			this->button1->Text = L"���������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(540, 250);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(84, 31);
			this->button2->TabIndex = 1;
			this->button2->Text = L"���������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Arial Black", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::Color::Red;
			this->label1->Location = System::Drawing::Point(42, 10);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(536, 72);
			this->label1->TabIndex = 2;
			this->label1->Text = L"�������: ������� ������, ����������� �������� � ������� ������������ �� �������� "
				L"����������� ��� ������";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label2
			// 
			this->label2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->ForeColor = System::Drawing::Color::Green;
			this->label2->Location = System::Drawing::Point(103, 82);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(425, 19);
			this->label2->TabIndex = 3;
			this->label2->Text = L"������� �������� ��������� ������ ������������";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label3->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->ForeColor = System::Drawing::Color::Maroon;
			this->label3->Location = System::Drawing::Point(92, 131);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(29, 21);
			this->label3->TabIndex = 4;
			this->label3->Text = L"x1";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label4->ForeColor = System::Drawing::Color::Maroon;
			this->label4->Location = System::Drawing::Point(92, 197);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(29, 21);
			this->label4->TabIndex = 5;
			this->label4->Text = L"y1";
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label5->ForeColor = System::Drawing::Color::Maroon;
			this->label5->Location = System::Drawing::Point(230, 131);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(29, 21);
			this->label5->TabIndex = 6;
			this->label5->Text = L"x2";
			this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label6->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label6->ForeColor = System::Drawing::Color::Maroon;
			this->label6->Location = System::Drawing::Point(230, 197);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(29, 21);
			this->label6->TabIndex = 7;
			this->label6->Text = L"y2";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label7->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label7->ForeColor = System::Drawing::Color::Maroon;
			this->label7->Location = System::Drawing::Point(348, 131);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(29, 21);
			this->label7->TabIndex = 8;
			this->label7->Text = L"x3";
			this->label7->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->label8->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->ForeColor = System::Drawing::Color::Maroon;
			this->label8->Location = System::Drawing::Point(348, 197);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(29, 21);
			this->label8->TabIndex = 9;
			this->label8->Text = L"y3";
			this->label8->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label9->ForeColor = System::Drawing::Color::Blue;
			this->label9->Location = System::Drawing::Point(88, 275);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(202, 19);
			this->label9->TabIndex = 10;
			this->label9->Text = L"�������� ������������";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label10->ForeColor = System::Drawing::Color::Blue;
			this->label10->Location = System::Drawing::Point(330, 275);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(198, 19);
			this->label10->TabIndex = 11;
			this->label10->Text = L"������� ������������";
			// 
			// txtx1
			// 
			this->txtx1->Location = System::Drawing::Point(92, 155);
			this->txtx1->Name = L"txtx1";
			this->txtx1->Size = System::Drawing::Size(100, 20);
			this->txtx1->TabIndex = 12;
			// 
			// txty1
			// 
			this->txty1->Location = System::Drawing::Point(92, 221);
			this->txty1->Name = L"txty1";
			this->txty1->Size = System::Drawing::Size(100, 20);
			this->txty1->TabIndex = 13;
			// 
			// txtx2
			// 
			this->txtx2->Location = System::Drawing::Point(230, 155);
			this->txtx2->Name = L"txtx2";
			this->txtx2->Size = System::Drawing::Size(100, 20);
			this->txtx2->TabIndex = 14;
			// 
			// txty2
			// 
			this->txty2->Location = System::Drawing::Point(230, 221);
			this->txty2->Name = L"txty2";
			this->txty2->Size = System::Drawing::Size(100, 20);
			this->txty2->TabIndex = 15;
			// 
			// txtx3
			// 
			this->txtx3->Location = System::Drawing::Point(348, 155);
			this->txtx3->Name = L"txtx3";
			this->txtx3->Size = System::Drawing::Size(100, 20);
			this->txtx3->TabIndex = 16;
			// 
			// txty3
			// 
			this->txty3->Location = System::Drawing::Point(348, 221);
			this->txty3->Name = L"txty3";
			this->txty3->Size = System::Drawing::Size(100, 20);
			this->txty3->TabIndex = 17;
			// 
			// txtPttr
			// 
			this->txtPttr->Enabled = false;
			this->txtPttr->Location = System::Drawing::Point(92, 297);
			this->txtPttr->Name = L"txtPttr";
			this->txtPttr->Size = System::Drawing::Size(198, 20);
			this->txtPttr->TabIndex = 18;
			// 
			// txtPltr
			// 
			this->txtPltr->Enabled = false;
			this->txtPltr->Location = System::Drawing::Point(332, 297);
			this->txtPltr->Name = L"txtPltr";
			this->txtPltr->Size = System::Drawing::Size(196, 20);
			this->txtPltr->TabIndex = 19;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::PaleGoldenrod;
			this->ClientSize = System::Drawing::Size(636, 394);
			this->Controls->Add(this->txtPltr);
			this->Controls->Add(this->txtPttr);
			this->Controls->Add(this->txty3);
			this->Controls->Add(this->txtx3);
			this->Controls->Add(this->txty2);
			this->Controls->Add(this->txtx2);
			this->Controls->Add(this->txty1);
			this->Controls->Add(this->txtx1);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"MyForm";
			this->Text = L"���������� ��������� � ������� ������������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
		this->Close();
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		// ���� �������� x1 �� ������� txtx1 � ���������������
		// ���������� ���� � ��� double
		try
		{
			double x1 = Convert::ToDouble(txtx1->Text);
			double y1 = Convert::ToDouble(txty1->Text);
			double x2 = Convert::ToDouble(txtx2->Text);
			double y2 = Convert::ToDouble(txty2->Text);
			double x3 = Convert::ToDouble(txtx3->Text);
			double y3 = Convert::ToDouble(txty3->Text);
			double s, p;
			s = PS(x1, y1, x2, y2, x3, y3, p); // ����� ������� ������� ������
			// ����� �������� p � ��������� ���� txtPttr � 
			// ��������������� �� ���� double � ��������� ���
			txtPttr->Text = p.ToString();
			txtPltr->Text = s.ToString();
		}
		catch (System::FormatException^ e)
		{
			txtPttr->Text = "������ �����";
			txtPltr->Text = "������ �����";
		}

	}
	};
}
