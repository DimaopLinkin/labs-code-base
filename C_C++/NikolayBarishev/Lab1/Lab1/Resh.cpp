// ���� Resh.cpp

#include <cmath>
// ������� ���������� ������� ������������ - �.�.
// ����� ������� ����� ������� � �����. (Xa, Ya) � (Xb, Yb)
double LenS(double Xa, double Ya, double Xb, double Yb) {
	return sqrt((Xa - Xb) * (Xa - Xb) + (Ya - Yb) * (Ya - Yb));
}

// ������� ���������� ��������� ������������
// �� ��������� a, b, c
double CalcP(double a, double b, double c) {
	return a + b + c;
}

// ������� ���������� ������� ������������
// �� ��������� a, b, c �� ������� ������
double CalcS(double a, double b, double c) {
	double pp = CalcP(a, b, c) / 2;
	return sqrt(pp * (pp - a) * (pp - b) * (pp - c));
}

// ������� ��������� � ������� ������������
// �� ����������� ��� ������
double PS(double x1, double y1, double x2, double y2, double x3, double y3, double& p) {
	double a, b, c, s;
	a = LenS(x1, y1, x2, y2);
	b = LenS(x2, y2, x3, y3);
	c = LenS(x1, y1, x3, y3);
	p = CalcP(a, b, c);
	s = CalcS(a, b, c);
	return s;
}