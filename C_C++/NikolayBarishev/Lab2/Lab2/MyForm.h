#pragma once
#include "Resh.h"

namespace Lab2 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::ListBox^ lstLB;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::TextBox^ txtEps;

	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::Panel^ panel2;
	private: System::Windows::Forms::TextBox^ txtPi;
	private: System::Windows::Forms::TextBox^ txtFunc;
	private: System::Windows::Forms::Label^ label9;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::Button^ functionOn;
	private: System::Windows::Forms::Button^ exit;



	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->lstLB = (gcnew System::Windows::Forms::ListBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->txtEps = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->txtPi = (gcnew System::Windows::Forms::TextBox());
			this->txtFunc = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->functionOn = (gcnew System::Windows::Forms::Button());
			this->exit = (gcnew System::Windows::Forms::Button());
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::Color::Red;
			this->label1->Location = System::Drawing::Point(73, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(655, 60);
			this->label1->TabIndex = 0;
			this->label1->Text = L"��������� ��������� �� � ��������� �� ������� = 0.00001, ���������������� �������"
				L"���� � ���";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// lstLB
			// 
			this->lstLB->Font = (gcnew System::Drawing::Font(L"Arial Narrow", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->lstLB->FormattingEnabled = true;
			this->lstLB->ItemHeight = 16;
			this->lstLB->Location = System::Drawing::Point(78, 139);
			this->lstLB->Name = L"lstLB";
			this->lstLB->Size = System::Drawing::Size(403, 260);
			this->lstLB->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(77, 90);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(110, 43);
			this->label2->TabIndex = 2;
			this->label2->Text = L"� �������� i";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(193, 90);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(141, 43);
			this->label3->TabIndex = 3;
			this->label3->Text = L"������������ ��";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label4
			// 
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label4->Location = System::Drawing::Point(340, 90);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(141, 43);
			this->label4->TabIndex = 4;
			this->label4->Text = L"����������� ��";
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// panel1
			// 
			this->panel1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel1->Controls->Add(this->txtEps);
			this->panel1->Controls->Add(this->label6);
			this->panel1->Location = System::Drawing::Point(507, 139);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(306, 50);
			this->panel1->TabIndex = 5;
			// 
			// txtEps
			// 
			this->txtEps->Location = System::Drawing::Point(182, 15);
			this->txtEps->Name = L"txtEps";
			this->txtEps->Size = System::Drawing::Size(119, 20);
			this->txtEps->TabIndex = 5;
			this->txtEps->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label6
			// 
			this->label6->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label6->Location = System::Drawing::Point(3, 12);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(172, 24);
			this->label6->TabIndex = 4;
			this->label6->Text = L"��������� ��������";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label5
			// 
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(503, 112);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(172, 24);
			this->label5->TabIndex = 4;
			this->label5->Text = L"�������� ������";
			this->label5->TextAlign = System::Drawing::ContentAlignment::BottomLeft;
			// 
			// label7
			// 
			this->label7->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label7->Location = System::Drawing::Point(503, 214);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(225, 24);
			this->label7->TabIndex = 4;
			this->label7->Text = L"���������� ����������";
			this->label7->TextAlign = System::Drawing::ContentAlignment::BottomLeft;
			// 
			// panel2
			// 
			this->panel2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel2->Controls->Add(this->txtPi);
			this->panel2->Controls->Add(this->txtFunc);
			this->panel2->Controls->Add(this->label9);
			this->panel2->Controls->Add(this->label8);
			this->panel2->Location = System::Drawing::Point(507, 241);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(306, 84);
			this->panel2->TabIndex = 5;
			// 
			// txtPi
			// 
			this->txtPi->Enabled = false;
			this->txtPi->Location = System::Drawing::Point(182, 48);
			this->txtPi->Name = L"txtPi";
			this->txtPi->Size = System::Drawing::Size(119, 20);
			this->txtPi->TabIndex = 5;
			this->txtPi->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// txtFunc
			// 
			this->txtFunc->Enabled = false;
			this->txtFunc->Location = System::Drawing::Point(182, 15);
			this->txtFunc->Name = L"txtFunc";
			this->txtFunc->Size = System::Drawing::Size(119, 20);
			this->txtFunc->TabIndex = 5;
			this->txtFunc->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// label9
			// 
			this->label9->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label9->Location = System::Drawing::Point(3, 45);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(172, 24);
			this->label9->TabIndex = 4;
			this->label9->Text = L"����������� ��";
			this->label9->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// label8
			// 
			this->label8->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(3, 12);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(172, 24);
			this->label8->TabIndex = 4;
			this->label8->Text = L"������������ ��";
			this->label8->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// functionOn
			// 
			this->functionOn->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->functionOn->Location = System::Drawing::Point(507, 359);
			this->functionOn->Name = L"functionOn";
			this->functionOn->Size = System::Drawing::Size(137, 39);
			this->functionOn->TabIndex = 6;
			this->functionOn->Text = L"�����";
			this->functionOn->UseVisualStyleBackColor = true;
			this->functionOn->Click += gcnew System::EventHandler(this, &MyForm::functionOn_Click);
			// 
			// exit
			// 
			this->exit->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->exit->Location = System::Drawing::Point(676, 359);
			this->exit->Name = L"exit";
			this->exit->Size = System::Drawing::Size(137, 39);
			this->exit->TabIndex = 7;
			this->exit->Text = L"���������";
			this->exit->UseVisualStyleBackColor = true;
			this->exit->Click += gcnew System::EventHandler(this, &MyForm::exit_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(825, 439);
			this->Controls->Add(this->exit);
			this->Controls->Add(this->functionOn);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->lstLB);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm";
			this->Text = L"������������ ������ �2 \"�������� ���������� Windows Form, ������������ ����������"
				L"� ����������� ���������\"";
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void exit_Click(System::Object^ sender, System::EventArgs^ e) {
		this->Close();
	}
	private: System::Void functionOn_Click(System::Object^ sender, System::EventArgs^ e) {
		try
		{
			lstLB->Items->Clear();
			PutDouble(Root(GetDouble(txtEps), 10000, lstLB), txtFunc);
			PutDouble(Math::PI, txtPi);
		}
		catch (System::FormatException^ e)
		{
			txtEps->Text = "������ �����";
		}
	}
	};
}
