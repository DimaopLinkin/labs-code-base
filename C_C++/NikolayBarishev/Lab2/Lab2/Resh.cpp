#include "Resh.h"

double Funct(double x) {
	return pow(-1, x) * pow(1, 2 * x + 1) / (2 * x + 1);
}

double Root(double Eps, int Imax, ListBox^ LB) {
	int i = 0;
	double a = Math::PI;
	double x = 0;
	do {
		if (x == 0) {
			x = Funct(i);
		}
		else {
			x = x + Funct(i);
		}
		i++;
		String^ fs = String::Format("\t{0,2:D2}\t\t{1,15:F9}\t\t{2,15:F9}", i, 4 * x, a);
		LB->Items->Add(fs);
	} while (Math::Abs(4 * x - a) >= Eps && i < Imax);
	return 4 * x;
}