using namespace System;
using namespace System::Windows::Forms;
#include <cmath>
#pragma once

double GetDouble(TextBox^);
double Root(double, int, ListBox^);
void PutDouble(double, TextBox^);
double Funct(double);
