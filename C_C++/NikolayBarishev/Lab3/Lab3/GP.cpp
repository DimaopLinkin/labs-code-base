#include "Resh.h"

double GetDouble(TextBox^ TB) {
	double field;

	field = Convert::ToDouble(TB->Text);

	return field;
}

void PutDouble(double field, TextBox^ TB) {
	TB->Text = field.ToString();
}