#pragma once
#include "Resh.h"

namespace Lab3 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ txtStartValue;
	private: System::Windows::Forms::TextBox^ txtEndValue;
	private: System::Windows::Forms::TextBox^ txtStep;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::ListBox^ lstLB;
	private: System::Windows::Forms::Button^ bttnCreateTable;
	private: System::Windows::Forms::Button^ bttnExit;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ txtCountPositive;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->txtStartValue = (gcnew System::Windows::Forms::TextBox());
			this->txtEndValue = (gcnew System::Windows::Forms::TextBox());
			this->txtStep = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->lstLB = (gcnew System::Windows::Forms::ListBox());
			this->bttnCreateTable = (gcnew System::Windows::Forms::Button());
			this->bttnExit = (gcnew System::Windows::Forms::Button());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->txtCountPositive = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(100, 41);
			this->label1->TabIndex = 0;
			this->label1->Text = L"��������� ��������";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label2
			// 
			this->label2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(118, 9);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(100, 41);
			this->label2->TabIndex = 1;
			this->label2->Text = L"�������� ��������";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(224, 9);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(100, 41);
			this->label3->TabIndex = 2;
			this->label3->Text = L"��� �������";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// txtStartValue
			// 
			this->txtStartValue->Location = System::Drawing::Point(16, 53);
			this->txtStartValue->Name = L"txtStartValue";
			this->txtStartValue->Size = System::Drawing::Size(96, 20);
			this->txtStartValue->TabIndex = 3;
			this->txtStartValue->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtEndValue
			// 
			this->txtEndValue->Location = System::Drawing::Point(122, 53);
			this->txtEndValue->Name = L"txtEndValue";
			this->txtEndValue->Size = System::Drawing::Size(96, 20);
			this->txtEndValue->TabIndex = 4;
			this->txtEndValue->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtStep
			// 
			this->txtStep->Location = System::Drawing::Point(228, 53);
			this->txtStep->Name = L"txtStep";
			this->txtStep->Size = System::Drawing::Size(96, 20);
			this->txtStep->TabIndex = 5;
			this->txtStep->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// label4
			// 
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label4->Location = System::Drawing::Point(12, 85);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(100, 25);
			this->label4->TabIndex = 6;
			this->label4->Text = L"��������";
			this->label4->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// label5
			// 
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(118, 85);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(100, 25);
			this->label5->TabIndex = 7;
			this->label5->Text = L"�������";
			this->label5->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// lstLB
			// 
			this->lstLB->FormattingEnabled = true;
			this->lstLB->Location = System::Drawing::Point(16, 114);
			this->lstLB->Name = L"lstLB";
			this->lstLB->Size = System::Drawing::Size(308, 316);
			this->lstLB->TabIndex = 8;
			// 
			// bttnCreateTable
			// 
			this->bttnCreateTable->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->bttnCreateTable->ForeColor = System::Drawing::Color::Teal;
			this->bttnCreateTable->Location = System::Drawing::Point(372, 9);
			this->bttnCreateTable->Name = L"bttnCreateTable";
			this->bttnCreateTable->Size = System::Drawing::Size(210, 64);
			this->bttnCreateTable->TabIndex = 9;
			this->bttnCreateTable->Text = L"��������� �������";
			this->bttnCreateTable->UseVisualStyleBackColor = true;
			this->bttnCreateTable->Click += gcnew System::EventHandler(this, &MyForm::bttnCreateTable_Click);
			// 
			// bttnExit
			// 
			this->bttnExit->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->bttnExit->ForeColor = System::Drawing::Color::Teal;
			this->bttnExit->Location = System::Drawing::Point(372, 396);
			this->bttnExit->Name = L"bttnExit";
			this->bttnExit->Size = System::Drawing::Size(210, 34);
			this->bttnExit->TabIndex = 10;
			this->bttnExit->Text = L"���������";
			this->bttnExit->UseVisualStyleBackColor = true;
			this->bttnExit->Click += gcnew System::EventHandler(this, &MyForm::bttnExit_Click);
			// 
			// label6
			// 
			this->label6->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label6->Location = System::Drawing::Point(369, 114);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(235, 24);
			this->label6->TabIndex = 11;
			this->label6->Text = L"����� ������������� ��������";
			this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// txtCountPositive
			// 
			this->txtCountPositive->Enabled = false;
			this->txtCountPositive->Location = System::Drawing::Point(372, 141);
			this->txtCountPositive->Name = L"txtCountPositive";
			this->txtCountPositive->Size = System::Drawing::Size(96, 20);
			this->txtCountPositive->TabIndex = 12;
			this->txtCountPositive->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Info;
			this->ClientSize = System::Drawing::Size(642, 447);
			this->Controls->Add(this->txtCountPositive);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->bttnExit);
			this->Controls->Add(this->bttnCreateTable);
			this->Controls->Add(this->lstLB);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->txtStep);
			this->Controls->Add(this->txtEndValue);
			this->Controls->Add(this->txtStartValue);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm";
			this->Text = L"���������� �����. ������������� �������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	private: System::Void bttnExit_Click(System::Object^ sender, System::EventArgs^ e) {
		this->Close();
	}
	private: System::Void bttnCreateTable_Click(System::Object^ sender, System::EventArgs^ e) {
		try
		{
			lstLB->Items->Clear();
			PutDouble(Root(GetDouble(txtStartValue), GetDouble(txtEndValue), GetDouble(txtStep), lstLB), txtCountPositive);
		}
		catch (System::FormatException^ e)
		{
			txtStartValue->Text = "������ �����";
			txtEndValue->Text = "������ �����";
			txtStep->Text = "������ �����";
		}
	}
	};
}
