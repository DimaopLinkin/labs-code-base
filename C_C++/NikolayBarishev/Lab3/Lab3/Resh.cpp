#include "Resh.h"

double Funct(double x) {
	if (x > 2) return Math::Pow(Math::E, x);
	else if (x < -2) return 0;
	else return x + 4;
}

int Root(double a, double b, double h, ListBox^ LB) {
	double x = 0;
	double i = a;
	int positive = 0;
	do {
		x = Funct(i);
		if (x > 0) positive++;
		String^ fs = String::Format("{0:F2}\t\t\t{1:F3}", i, x);
		LB->Items->Add(fs);
		i += h;
	} while (i <= b);
	return positive;
}