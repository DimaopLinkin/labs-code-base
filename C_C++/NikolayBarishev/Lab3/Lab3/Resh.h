using namespace System;
using namespace System::Windows::Forms;
#include <cmath>
#pragma once

double GetDouble(TextBox^);
int Root(double, double, double, ListBox^);
void PutDouble(double, TextBox^);
double Funct(double);
