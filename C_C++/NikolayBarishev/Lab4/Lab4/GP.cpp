#include "Resh.h"

bool GetDouble(double& x, TextBox^ TB, String^ s) {
	if (TB->Text->Length == 0) {
		MessageBox::Show(s, "������", MessageBoxButtons::OK, MessageBoxIcon::Error);
		TB->Focus();
		return false;
	}
	x = Convert::ToDouble(TB->Text);
	return true;
}

void PutDouble(double field, TextBox^ TB) {
	TB->Text = field.ToString();
}