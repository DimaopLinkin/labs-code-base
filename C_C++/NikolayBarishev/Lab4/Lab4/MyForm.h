#pragma once
#include "Resh.h"

namespace Lab4 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::TextBox^ txtA;
	private: System::Windows::Forms::TextBox^ txtB;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ txtH1;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::TextBox^ txtC;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::TextBox^ txtD;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::TextBox^ txtH2;
	private: System::Windows::Forms::Label^ label7;
	private: System::Windows::Forms::ListBox^ listBox1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Label^ label8;
	private: System::Windows::Forms::TextBox^ txtMin;
	private: System::Windows::Forms::Button^ button2;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->txtA = (gcnew System::Windows::Forms::TextBox());
			this->txtB = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->txtH1 = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->txtC = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->txtD = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->txtH2 = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->txtMin = (gcnew System::Windows::Forms::TextBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->ForeColor = System::Drawing::SystemColors::Highlight;
			this->label1->Location = System::Drawing::Point(13, 13);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(612, 23);
			this->label1->TabIndex = 0;
			this->label1->Text = L"��������� � ��� ��������� ����������";
			this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label2
			// 
			this->label2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(13, 36);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(66, 18);
			this->label2->TabIndex = 1;
			this->label2->Text = L"a";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// txtA
			// 
			this->txtA->Location = System::Drawing::Point(13, 58);
			this->txtA->Name = L"txtA";
			this->txtA->Size = System::Drawing::Size(65, 20);
			this->txtA->TabIndex = 2;
			// 
			// txtB
			// 
			this->txtB->Location = System::Drawing::Point(84, 58);
			this->txtB->Name = L"txtB";
			this->txtB->Size = System::Drawing::Size(65, 20);
			this->txtB->TabIndex = 4;
			// 
			// label3
			// 
			this->label3->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(83, 36);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(65, 19);
			this->label3->TabIndex = 3;
			this->label3->Text = L"b";
			this->label3->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// txtH1
			// 
			this->txtH1->Location = System::Drawing::Point(155, 58);
			this->txtH1->Name = L"txtH1";
			this->txtH1->Size = System::Drawing::Size(65, 20);
			this->txtH1->TabIndex = 6;
			// 
			// label4
			// 
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label4->Location = System::Drawing::Point(154, 37);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(66, 18);
			this->label4->TabIndex = 5;
			this->label4->Text = L"h1";
			this->label4->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// txtC
			// 
			this->txtC->Location = System::Drawing::Point(226, 58);
			this->txtC->Name = L"txtC";
			this->txtC->Size = System::Drawing::Size(65, 20);
			this->txtC->TabIndex = 8;
			// 
			// label5
			// 
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label5->Location = System::Drawing::Point(226, 37);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(65, 18);
			this->label5->TabIndex = 7;
			this->label5->Text = L"c";
			this->label5->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// txtD
			// 
			this->txtD->Location = System::Drawing::Point(297, 58);
			this->txtD->Name = L"txtD";
			this->txtD->Size = System::Drawing::Size(65, 20);
			this->txtD->TabIndex = 10;
			// 
			// label6
			// 
			this->label6->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label6->Location = System::Drawing::Point(297, 37);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(65, 18);
			this->label6->TabIndex = 9;
			this->label6->Text = L"d";
			this->label6->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// txtH2
			// 
			this->txtH2->Location = System::Drawing::Point(368, 58);
			this->txtH2->Name = L"txtH2";
			this->txtH2->Size = System::Drawing::Size(65, 20);
			this->txtH2->TabIndex = 12;
			// 
			// label7
			// 
			this->label7->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label7->Location = System::Drawing::Point(368, 37);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(65, 18);
			this->label7->TabIndex = 11;
			this->label7->Text = L"h2";
			this->label7->TextAlign = System::Drawing::ContentAlignment::BottomCenter;
			// 
			// listBox1
			// 
			this->listBox1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->listBox1->FormattingEnabled = true;
			this->listBox1->ItemHeight = 18;
			this->listBox1->Location = System::Drawing::Point(12, 93);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(421, 364);
			this->listBox1->TabIndex = 13;
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->Location = System::Drawing::Point(440, 93);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(185, 54);
			this->button1->TabIndex = 14;
			this->button1->Text = L"������ ������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label8->Location = System::Drawing::Point(439, 180);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(121, 18);
			this->label8->TabIndex = 15;
			this->label8->Text = L"����������� Z";
			// 
			// txtMin
			// 
			this->txtMin->Enabled = false;
			this->txtMin->Location = System::Drawing::Point(440, 202);
			this->txtMin->Name = L"txtMin";
			this->txtMin->Size = System::Drawing::Size(185, 20);
			this->txtMin->TabIndex = 16;
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button2->Location = System::Drawing::Point(442, 407);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(185, 54);
			this->button2->TabIndex = 14;
			this->button2->Text = L"���������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(637, 473);
			this->Controls->Add(this->txtMin);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->listBox1);
			this->Controls->Add(this->txtH2);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->txtD);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->txtC);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->txtH1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->txtB);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->txtA);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm";
			this->Text = L"��������� �����. ������������� ������� ���� ����������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
		this->Close();
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		double a, b, h1, c, d, h2, min;
		try
		{
			if (!GetDouble(a, txtA, "������� ���. �������� 1-�� ���������"))return;
			if (!GetDouble(b, txtB, "������� �������� �������� 1-�� ���������"))return;
			if (!GetDouble(h1, txtH1, "������� ��� ��������� 1-�� ���������"))return;
			if (!GetDouble(c, txtC, "������� ���. �������� 2-�� ���������"))return;
			if (!GetDouble(d, txtD, "������� �������� �������� 2-�� ���������"))return;
			if (!GetDouble(h2, txtH2, "������� ��� ��������� 2-�� ���������"))return;

			min = Root(a, b, h1, c, d, h2, listBox1);
			PutDouble(min, txtMin);
		}
		catch (System::FormatException^ e)
		{
			txtMin->Text = "������ �����!";
		}
	}
	};
}
