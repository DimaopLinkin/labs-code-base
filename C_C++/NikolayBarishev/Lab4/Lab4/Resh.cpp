#include "Resh.h"

double Funct(double x, double y) {
	return x + 1 / x + (1 - y) / 2;
}

double Root(double a, double b, double h1, double c, double d, double h2, ListBox^ LB) {
	String^ fs;
	double z;
	double min = Funct(a, c);
	for (double x = a; x <= b; x += h1) {
		for (double y = c; y <= d; y += h2) {
			z = Funct(x, y);
			if (z < min) min = z;
			if (y == c)	fs = String::Format("x={0:F2}\t\ty={1:F2}\t\tz={2:F6}", x, y, z);
			else fs = String::Format("\t\ty={0:F2}\t\tz={1:F6}", y, z);
			LB->Items->Add(fs);
		}
	}
	return min;
}