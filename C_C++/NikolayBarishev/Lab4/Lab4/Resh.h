using namespace System;
using namespace System::Windows::Forms;
#include <cmath>
#pragma once

bool GetDouble(double&, TextBox^, String^);
double Root(double, double, double, double, double, double, ListBox^);
void PutDouble(double, TextBox^);
double Funct(double, double);
