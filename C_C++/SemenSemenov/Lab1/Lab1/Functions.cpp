#include "Functions.h"

/// <summary>
/// ������� ������������ �������� ���� �����
/// </summary>
/// <param name="first">������ ������</param>
/// <param name="second">������ ������</param>
/// <returns>��������� ��������</returns>
string plusius(string first, string second) {
	int result = 0;
	std::string resultat = "";
	int razryad = 0;
	// ���� ������ ������ ������ ������, ������ �� �������
	if (first.length() < second.length()) {
		string temp = first;
		first = second;
		second = temp;
	}
	// ���� ��� ����� ������������� ��� �������������, �� ������ ��������
	if (first[0] == '-' && second[0] == '-' || first[0] != '-' && second[0] != '-') {
		// � ����� �� ����� ������� �����
		for (int i = 0; i < first.length(); i++) {
			// ���� �� ����� �� ����� ������� �����
			if (second.length() == 1 && i == 0) {
				result = (first[first.length() - 1 - i] - 48) + second[0] - 48 + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
			else if (second.length() - 1 >= i && second[second.length() - i] != '-') {
				// ���������� ������� �����
				result = ((first[first.length() - 1 - i] - 48) + (second[second.length() - 1 - i] - 48)) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
			// ����� (���� ����� �� ����� ������� �����)
			else {
				// ���� �� ����� �� ����� ������� �����
				if (first[first.length() - 1 - i] == '-') break;
				// ��������� � ���������� ������� ������ ������
				result = (first[first.length() - 1 - i] - 48) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
		}
		if (razryad) resultat = '1' + resultat;
		while (resultat[0] == '0' && resultat.length() != 1) {
			resultat.erase(resultat.begin() + 1, resultat.end());
		}
		// ���� ������ ������ �������������
		if (first[0] == '-' && resultat[0] != '0') {
			resultat = "-" + resultat;
		}
	}
	// ����� (���� ����� ������ ������) �������� ����������
	else {
		for (int i = 0; i < first.length(); i++) {
			if (second.length() - 1 >= i && second[second.length() - 1 - i] != '-') {
				result = ((first[first.length() - 1 - i] - 48) - (second[second.length() - 1 - i] - 48)) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
					result += 10;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
			else {
				if (first[first.length() - 1 - i] == '-') break;
				// ��������� � ���������� ������� ������ ������
				result = (first[first.length() - 1 - i] - 48) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
					result += 10;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
		}
		if (razryad) resultat = '1' + resultat;
		while (resultat[0] == '0' && resultat.length() != 1) {
			resultat.erase(resultat.begin() + 1, resultat.end());
		}
		if (first[0] == '-' && resultat[0] != '0') {
			resultat = "-" + resultat;
		}
	}
	return resultat;
}

/// <summary>
/// ������� ������������ ��������� ���� �����
/// </summary>
/// <param name="first">������ ������</param>
/// <param name="second">������ ������</param>
/// <returns>��������� ���������</returns>
string minusius(string first, string second) {
	int result = 0;
	std::string resultat = "";
	int razryad = 0;
	bool reversed = false;
	// ���� ������ ������ ������ ������, ������ �� �������
	if (first.length() < second.length()) {
		string temp = first;
		first = second;
		second = temp;
		reversed = true;
	}
	// ���� ��� ����� ������������� ��� �������������, �� ������ ���������
	if (first[0] == '-' && second[0] == '-' || first[0] != '-' && second[0] != '-') {
		// � ����� �� ����� ������� �����
		for (int i = 0; i < first.length(); i++) {
			// ���� �� ����� �� ����� ������� �����
			if (second.length() > i && second[second.length() - i] != '-') {
				// ���������� ������� �����, �������� �� 10^i
				result = ((first[first.length() - 1 - i] - 48) - (second[second.length() - 1 - i] - 48)) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
					result += 10;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
			// ����� (���� ����� �� ����� ������� �����)
			else {
				// ���� �� ����� �� ����� ������� �����
				if (first[first.length() - 1 - i] == '-') break;
				// ��������� � ���������� ������� ������ ������
				result = (first[first.length() - 1 - i] - 48) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					razryad = -1;
					result += 10;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
		}
		if (razryad == 1) {
			resultat = "1" + resultat;
		}
		while (resultat[0] == '0' && resultat.length() != 1) {
			resultat.erase(resultat.begin(), resultat.begin() + 1);
		}
		if (first[0] == '-' && reversed) {

		}
		// ���� ������ ������ �������������
		else if (first[0] == '-' && resultat[0] != '0' || reversed) {
			resultat = "-" + resultat;
		}
	}
	// ����� (���� ����� ������ ������) ��������� ����������
	else {
		for (int i = 0; i < first.length(); i++) {
			if (second.length() - 1 >= i && second[second.length() - 1 - i] != '-') {
				result = ((first[first.length() - 1 - i] - 48) + (second[second.length() - 1 - i] - 48)) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					result += 10;
					razryad = -1;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
			else {
				if (first[first.length() - 1 - i] == '-') break;
				result = (first[first.length() - 1 - i] - 48) + razryad;
				if (result >= 10) {
					razryad = 1;
				}
				else if (result < 0) {
					result += 10;
					razryad = -1;
				}
				else razryad = 0;
				result %= 10;
				resultat = to_string(result) + resultat;
			}
		}
		if (razryad) resultat = '1' + resultat;
		while (resultat[0] == '0' && resultat.length() != 1) {
			resultat.erase(resultat.begin() + 1, resultat.end());
		}
		if (first[0] == '-' && reversed) {

		}
		else if (first[0] == '-' && resultat[0] != '0' || reversed) {
			resultat = "-" + resultat;
		}
	}
	return resultat;
}
