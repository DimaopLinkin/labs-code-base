#include <Conio.h>
#include <iostream>
#include <cstdlib>
#include <Windows.h>;
#include <conio.h>;
#include <ctime>;
#include <math.h>;
#include <string>;

using namespace std;

bool gameOver;
const int width = 20;
const int height = 20;
int x, y, fruitX, fruitY, score;
int tailX[100], tailY[100];
int nTail;


enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN };
eDirection dir;

void Setup() { // настройки старта игры
    srand(time(NULL));
    gameOver = false;
    dir = STOP;
    x = width / 2 - 1;
    y = height / 2 - 1;
    fruitX = rand() % (width - 2);
    fruitY = rand() % (height - 2);
    score = 0;
}

void DrawMap() { // карта
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { 0,0 });
    //system("cls"); // обновление карты

    Sleep(100 - score);
    for (int i = 0; i < width + 1; i++) {
        cout << "# ";
    }
    cout << endl;

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {

            if (j == 0 || j == width - 1) {
                cout << "# ";
            }
            if (i == y && j == x) {
                cout << "0 ";
            }
            else if (i == fruitY && j == fruitX) {
                cout << "F ";
            }
            else {
                bool print = false;
                for (int k = 0; k < nTail; k++) {
                    if (tailX[k] == j && tailY[k] == i)
                    {
                        print = true;
                        cout << "o ";
                    }
                }
                if (!print) {
                    cout << "  ";
                }
            }

        }
        cout << endl;
    }
    for (int i = 0; i < width + 1; i++) {
        cout << "# ";
    }
    cout << endl;
    cout << "Score: " << score << endl;


}

void Input() { // приём данных от игрока для определения направления
    if (_kbhit()) {
        switch (_getch())
        {
        case 'a':
            if (dir != RIGHT)
                dir = LEFT;
            break;
        case 'd':
            if (dir != LEFT)
                dir = RIGHT;
            break;
        case 'w':
            if (dir != DOWN)
                dir = UP;
            break;
        case 's':
            if (dir != UP)
                dir = DOWN;
            break;
        case 'x':
            gameOver = true;
            break;
        }

    }
}


void Logic() { // логика игры
    int prevX = tailX[0];
    int prevY = tailY[0];
    int prev2X, prev2Y;
    tailX[0] = x;
    tailY[0] = y;
    for (int i = 1; i < nTail; i++) { // логика обработки хвоста
        prev2X = tailX[i];
        prev2Y = tailY[i];
        tailX[i] = prevX;
        tailY[i] = prevY;
        prevX = prev2X;
        prevY = prev2Y;
    }
    switch (dir)
    {
    case LEFT:
        x--;
        break;
    case RIGHT:
        x++;
        break;
    case UP:
        y--;
        break;
    case DOWN:
        y++;
        break;
    }
    //if (x > width - 2 || x < 0 || y > height || y < 0) {
    //    gameOver = true;
    //}

    if (x >= width - 1) {
        x = 0;
    }
    else if (x < 0) {
        x = width - 2;
    }

    if (y >= height) {
        y = 0;
    }
    else if (y < 0) {
        y = height - 1;
    }



    for (int i = 0; i < nTail; i++) {
        if (tailX[i] == x && tailY[i] == y) {
            gameOver = true;
        }
    }
    if (x == fruitX && y == fruitY) {
        score += 1;
        fruitX = rand() % (width - 2);
        fruitY = rand() % (height - 2);
        nTail++;
    }

}

int main()
{
    void* handle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO structCursorInfo;
    GetConsoleCursorInfo(handle, &structCursorInfo);
    structCursorInfo.bVisible = FALSE;
    SetConsoleCursorInfo(handle, &structCursorInfo);

    Setup();
    while (!gameOver)
    {
        DrawMap();
        Input();
        Logic();
    }
    return 0;
}
