#include <iostream>;
#include <Windows.h>;
#include <conio.h>;
#include <ctime>;
#include <string>;
using namespace std;
bool isStopGame = true;
bool isStop = true;

struct Options {
	//массив с челиком —--------------------—
	int mas[3][3] = { {0,0,0,},
						{0,0,0,},
						{0,0,0,},
	};

	// корды Plaeyr для отметки
	string XPlayer;
	string YPlayer;

	int intBotX;
	int intBotY;

	// баллы
	int HavePoints = 0;

};

int Menu() {
	setlocale(0, "");
	int change;
	system("cls");
	cout << " [!]МЕНЮ[!]\n ";
	cout << " [1]Одиночная игра\n [2]2 игрока\n [3]Правила и условности игры\n [?]Выберите цыфру пункта меню для продолжения[?] -> ";
	cin >> change;

	return change;
}

void Rules(Options& options) {
	system("cls");
	cout << " [!]Правила игры[!]\n ";
	cout << "Игроки по очереди ставят на свободные клетки поля 3х3 знаки (один всегда крестики, другой всегда нолики).\nПервый, выстроивший в ряд 3 своих фигуры по вертикали, горизонтали или диагонали, выигрывает.\nПервый ход делает игрок, ставящий крестики.\nОбычно по завершении партии выигравшая сторона зачёркивает чертой свои три знака(нолика или крестика),\n составляющих сплошной ряд.\n ";
	cout << " [!]Как играть[!]\n ";
	cout << "Cначала ходит игрок 1 против бота или против игрока 2.\n для того чтобы походить нужно выбрать координаты сначала по оси Y потом X,\n требуется ввести число в диапозоне от 0 до 2 соотвествено [0] [1] [2] затем нажать [Enter] \n ";

	cout << "\n[?]нажмите любую кнопку и [Enter] для продолжения[?]\n ";
	char ContinuationGame;
	cin >> ContinuationGame;
}

void VictoryX(Options& options) {
	Sleep(1500);
	int change;
	system("cls");
	cout << "[!]Победа за Крестики[!]\n";
	Sleep(2000);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			options.mas[i][j] = 0;
		}
	}
	cout << "[?]Желаете продолжить?[?]\n[1]yes\t[2]no\n->";
	cin >> change;
	if (change == 2) {
		isStop = false;
	}
}

void VictoryO(Options& options) {
	Sleep(1500);
	int change;
	system("cls");
	cout << "[!]Победа за Нолики[!]\n";
	Sleep(2000);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			options.mas[i][j] = 0;
		}
	}
	cout << "[?]Желаете продолжить?[?]\n[1]yes\t[2]no\n->";
	cin >> change;
	if (change == 2) {
		isStop = false;
	}
}

void NOVictory(Options& options) {
	Sleep(1500);
	system("cls");
	cout << "[!]Победы не будет!!! т. к. поля для заполнения закончились[!]\n";
	Sleep(2000);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			options.mas[i][j] = 0;
		}
	}
}

void VerificationOfVictory(Options& options) {



	// проверка победы крестов "Х"
	if (options.mas[0][0] == 1 &&
		options.mas[1][1] == 1 &&
		options.mas[2][2] == 1) {
		VictoryX(options);
	}
	else if (options.mas[2][0] == 1 &&
		options.mas[1][1] == 1 &&
		options.mas[0][2] == 1) {
		VictoryX(options);
	}
	else if (options.mas[0][1] == 1 &&
		options.mas[1][1] == 1 &&
		options.mas[2][1] == 1) {
		VictoryX(options);
	}
	else if (options.mas[0][0] == 1 &&
		options.mas[1][0] == 1 &&
		options.mas[2][0] == 1) {
		VictoryX(options);
	}
	else if (options.mas[0][2] == 1 &&
		options.mas[1][2] == 1 &&
		options.mas[2][2] == 1) {
		VictoryX(options);
	}
	else if (options.mas[0][0] == 1 &&
		options.mas[0][1] == 1 &&
		options.mas[0][2] == 1) {
		VictoryX(options);
	}
	else if (options.mas[1][0] == 1 &&
		options.mas[1][1] == 1 &&
		options.mas[1][2] == 1) {
		VictoryX(options);
	}
	else if (options.mas[2][0] == 1 &&
		options.mas[2][1] == 1 &&
		options.mas[2][2] == 1) {
		VictoryX(options);
	}

	// проверка победы нолей "О"
	if (options.mas[0][0] == 2 &&
		options.mas[1][1] == 2 &&
		options.mas[2][2] == 2) {
		VictoryO(options);
	}
	else if (options.mas[2][0] == 2 &&
		options.mas[1][1] == 2 &&
		options.mas[0][2] == 2) {
		VictoryO(options);
	}
	else if (options.mas[0][1] == 2 &&
		options.mas[1][1] == 2 &&
		options.mas[2][1] == 2) {
		VictoryO(options);
	}
	else if (options.mas[0][0] == 2 &&
		options.mas[1][0] == 2 &&
		options.mas[2][0] == 2) {
		VictoryO(options);
	}
	else if (options.mas[0][2] == 2 &&
		options.mas[1][2] == 2 &&
		options.mas[2][2] == 2) {
		VictoryO(options);
	}
	else if (options.mas[0][0] == 2 &&
		options.mas[0][1] == 2 &&
		options.mas[0][2] == 2) {
		VictoryO(options);
	}
	else if (options.mas[1][0] == 2 &&
		options.mas[1][1] == 2 &&
		options.mas[1][2] == 2) {
		VictoryO(options);
	}
	else if (options.mas[2][0] == 2 &&
		options.mas[2][1] == 2 &&
		options.mas[2][2] == 2) {
		VictoryO(options);
	}







}

void showMap(Options options) {
	system("cls");

	// проверка на ничью 
	for (int i = 0; i < 3; i++) //переключение по строкам
	{
		for (int j = 0; j < 3; j++)// переключение по столбцам
		{
			if (options.mas[i][j] == 0) {
				options.HavePoints++;
			}
		}
	}
	if (options.HavePoints == 0)
	{
		NOVictory(options);
	}




	// вывод карты 

	for (int i = 0; i < 3; i++) //переключение по строкам
	{
		for (int j = 0; j < 3; j++)// переключение по столбцам
		{
			if (options.mas[i][j] == 0)
			{

				cout << ". ";
			}
			if (options.mas[i][j] == 1)
			{
				cout << "x ";
			}
			if (options.mas[i][j] == 2)
			{
				cout << "o ";
			}

		}
		cout << endl;
	}

	Sleep(3000);
}

void BOTRandomXY(Options& options) {

	cout << "Ход Бота";

	srand(time(0) % (rand() / 2 - 5 / 3 * 2));
	int i = 0;
	bool isStop = true;
	for (; isStop;) {
		i++;
		options.intBotX = rand() % 3;
		options.intBotY = rand() % 3;

		if (options.mas[options.intBotX][options.intBotY] == 2) {
			options.intBotX = rand() % 3;
			options.intBotY = rand() % 3;
			Sleep(100);
		}
		else if (options.mas[options.intBotX][options.intBotY] == 1) {
			options.intBotX = rand() % 3;
			options.intBotY = rand() % 3;
			Sleep(100);
		}
		else if (options.mas[options.intBotX][options.intBotY] == 0) {
			isStop = false;
			options.mas[options.intBotX][options.intBotY] = 2;
			VerificationOfVictory(options);
		}

	}


}

void PlayerMove1(Options& options) {
	
	bool isStopManyNum = true;


	for (; isStopManyNum;) {
		cout << "[!]Ход игрока 1: \n";
		cout << "Введите координату Y: ";
		cin >> options.XPlayer;
		cout << "Введите координату X: ";
		cin >> options.YPlayer;
		cout << "\n";
		int intPlayerX = atoi(options.XPlayer.c_str());
		int intPlayerY = atoi(options.YPlayer.c_str());
		intPlayerX %= 3; intPlayerY %= 3;
		isStopManyNum = false;
		if (options.mas[intPlayerX][intPlayerY] == 2) {
			cout << "[!]Введите координаты повторно\n";
			cout << "Введите координату Y: ";
			cin >> options.XPlayer;
			cout << "Введите координату X: ";
			cin >> options.YPlayer;
			int intPlayerX = atoi(options.XPlayer.c_str());
			int intPlayerY = atoi(options.YPlayer.c_str());

		}
		if (options.mas[intPlayerX][intPlayerY] == 1) {
			cout << "[!]Введите координаты повторно\n";
			cout << "Введите координату Y: ";
			cin >> options.XPlayer;
			cout << "Введите координату X: ";
			cin >> options.YPlayer;
			int intPlayerX = atoi(options.XPlayer.c_str());
			int intPlayerY = atoi(options.YPlayer.c_str());

		}
		if (options.mas[intPlayerX][intPlayerY] == 0) {
			isStop = false;
			options.mas[intPlayerX][intPlayerY] = 1;
			VerificationOfVictory(options);
			break;
		}
	}
}

void PlayerMove2(Options& options) {
	cout << "[!]Ход игрока 2: \n";

	cout << "Введите координату Y: ";
	cin >> options.XPlayer;
	cout << "Введите координату X: ";
	cin >> options.YPlayer;
	cout << "\n";
	int intPlayerX = atoi(options.XPlayer.c_str());
	int intPlayerY = atoi(options.YPlayer.c_str());

	bool isStop = true;
	for (; isStop;) {

		if (options.mas[intPlayerX][intPlayerY] == 2) {
			cout << "[!]Введите координаты повторно\n";
			cout << "Введите координату Y: ";
			cin >> options.XPlayer;
			cout << "Введите координату X: ";
			cin >> options.YPlayer;
		}
		if (options.mas[intPlayerX][intPlayerY] == 1) {
			cout << "[!]Введите координаты повторно\n";
			cout << "Введите координату Y: ";
			cin >> options.XPlayer;
			cout << "Введите координату X: ";
			cin >> options.YPlayer;
		}
		if (options.mas[intPlayerX][intPlayerY] == 0) {
			isStop = false;
			options.mas[intPlayerX][intPlayerY] = 2;
			VerificationOfVictory(options);
		}

	}
}

int main()
{
	setlocale(0, "");
	cout << "[!]НАЧАЛО ИГРЫ[!]\n[?]нажмите любую кнопку и [Enter] для продолжения[?]";
	char ContinuationGame;
	cin >> ContinuationGame;
	system("cls");
	isStopGame = true;
	isStop = true;
	Options options;

	for (; isStopGame;) {
		switch (Menu())
		{

		case 1:
			for (; isStop;) {
				showMap(options);
				PlayerMove1(options);
				BOTRandomXY(options);
			}
			isStop = true;
			break;
		case 2:
			for (; isStop;) {
				showMap(options);
				PlayerMove1(options);
				showMap(options);
				PlayerMove2(options);
			}
			isStop = true;
			break;
		case 3:
			Rules(options);
			break;
		}
	}






}