#include "MyForm.h"

using namespace Application1;

int main(array<String^>^ arg) {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	Application1::MyForm form;
	Application::Run(% form);
	return 0;
}