﻿#include "pch.h"
#include <conio.h>
#include <string.h>
#include <malloc.h>
#include <clocale>
#include <cstdio>
#include <Windows.h>

using namespace std;
using namespace System;
using namespace System::IO;

#define ENTER 13
#define ESC 27
#define UP 72
#define DOWN 80
char dan[7][70] = {
"Самая населенная страна?",
"Есть ли страны с одинаковым количеством городов миллионников?",
"Есть ли одинаковое население у двух городов из разных стран?",
"Алфавитный список городов",
"КАлфавитный список городов",
"Алфавитный список городов",
"Выход "
};
char BlankLine[] = "                                                                  ";
int NC;
struct a
{
	char name[40]; 
	char town[20];
	int population; 
	float persent;
	int perep; 

};
/*struct sp {
	char fi[40];
	float rang;
	struct sp* pred;
	struct sp* sled;
} *spisok;*/

int menu(int);
void maxim(struct a*);
void first(struct a*);
void text_data(char*, char*);
void kolvo(struct a*);
void alfalist(struct a*);
void vstavka(struct a*, char*);
void listing(struct a*);
void filminoneyear(struct a*);
void diagram(struct a*);

int main(array<System::String^>^ args)
{
	setlocale(LC_CTYPE, "Russian");
	FILE* in;
	int i, n;
	struct a* data;
	Console::CursorVisible::set(false);
	Console::BufferHeight = Console::WindowHeight;
	Console::BufferWidth = Console::WindowWidth;


	if ((in = fopen("data.dat", "r")) == NULL)
	{
		printf("Файл не открыт");
		_getch();
		exit(1);
	}
	fscanf(in, "%d", &NC);

	data = (struct a*)malloc(NC * sizeof(struct a));

	for (i = 0; i < NC; i++)
		fscanf(in, "%s%s%d%f%d", data[i].name, data[i].town, &data[i].population, &data[i].persent, &data[i].perep);

	for (i = 0; i < NC; i++)
		printf("\n%40s %-10s %-20d %-1.1f %d", data[i].name, data[i].town, data[i].population, data[i].persent, data[i].perep);
	_getch();

	while (1)
	{
		Console::ForegroundColor = ConsoleColor::Blue;
		Console::BackgroundColor = ConsoleColor::Red;
		Console::Clear();
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Yellow;
		Console::CursorLeft = 10;
		Console::CursorTop = 4;
		printf(BlankLine);

		for (i = 0; i < 7; i++)
		{
			Console::CursorLeft = 10;
			Console::CursorTop = i + 5;
			printf(" %-55s ", dan[i]);
		}
		Console::CursorLeft = 10;
		Console::CursorTop = 12;
		printf(BlankLine);

		n = menu(7);
		switch (n) {
		case 1: ; break;// функции из методички по очереди сюда 
		case 2: ; break;
		case 3: ; break;
		case 4: ; break;
		case 5: ; break;
		case 6: ; break;
		case 7: exit(0);
		}
	}
	return 0;
}
int menu(int n)
{
	int y1 = 0, y2 = n - 1;
	char c = 1;
	while (c != ESC)
	{
		switch (c)
		{
		case DOWN: y2 = y1; y1++; break;
		case UP: y2 = y1; y1--; break;
		case ENTER: return y1 + 1;
		}
		if (y1 > n - 1) { y2 = n - 1; y1 = 0; }
		if (y1 < 0) { y2 = 0; y1 = n - 1; }
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Red;
		Console::CursorLeft = 11;
		Console::CursorTop = y1 + 5;
		printf("%s", dan[y1]);
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Yellow;
		Console::CursorLeft = 11;
		Console::CursorTop = y2 + 5;
		printf("%s", dan[y2]);
		c = getch();
	}
	exit(0);
}