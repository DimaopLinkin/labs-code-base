﻿// Блок подключения библиотек
#include "pch.h"
#include <conio.h>
#include <string.h>
#include <malloc.h>
#include <clocale>
#include <cstdio>
#include <Windows.h>

// Отключение предупреждения о небезопасности использования scanf
#pragma warning (disable: 4996)

// Используемые пространства имён
using namespace std;
using namespace System;
using namespace System::IO;

// Константы
#define ENTER 13
#define ESC 27
#define UP 72
#define DOWN 80
// Двумерный массив строк для вывода в консоль
char dan[7][70] = {
"Самая населенная страна?                                        ",
"Есть ли страны с одинаковым количеством городов миллионников?   ",
"Есть ли одинаковое население у двух городов из разных стран?    ",
"Алфавитный список столиц                                        ",
"Количество стран с населением больше 1000000000                 ",
"Диаграмма. Процентное соотношение населения стран               ",
"Выход                                                           "
};
// Строка разделитель
char BlankLine[] = "                                                                  ";
int NC;
// Структура, определяющая данные
struct a
{
	char name[40];
	char town[20];
	int population;
	float persent;
	int million;

};
// Структура, определяющая списки для сортировки
struct sp {
	char name[40];
	char town[20];
	int population;
	struct sp* pred;
	struct sp* sled;
} *spisok1, * spisok2;

// Определение функций
int menu(int);							// Основная функция
void most_populated(struct a*);			// Поиск самой населенной страны
void is_same_million(struct a*);		// Поиск стран с одинаковым количеством миллионников
void is_same_population(struct a*);		// Поиск одинаково населённых городов
void alfalist(struct a*);				// Сортировка списка столиц
void vstavka(struct a*, char*);			// Функция вставки (для алгоритма сортировки)
void kolvo(struct a*);					// Подсчёт количества стран
void diagram(struct a*);				// Отрисовка диаграммы
void vstavka_stran(struct a*, char*);	// Функция вставки (для второго алгоритма сортировки)

int main(array<System::String^>^ args)
{
	// Подключение русской локали в консоль
	setlocale(LC_CTYPE, "Russian");
	// Указатель на файл
	FILE* in;
	// Итератор и переменная для работы с меню
	int i, n;
	// Указатель на список с данными
	struct a* data;
	// Отключение мигающего курсора в консоли
	Console::CursorVisible::set(false);
	// Установка ширины и высоты окна
	Console::BufferHeight = Console::WindowHeight;
	Console::BufferWidth = Console::WindowWidth;

	// Попытка открыть файл с данными
	if ((in = fopen("data.dat", "r")) == NULL)
	{
		// Если файл не открыт, вывести сообщение и завершить программу
		printf("Файл не открыт");
		_getch();
		exit(1);
	}
	// Считать содержимое файла
	fscanf(in, "%d", &NC);

	// Выделить динамически память под данные
	data = (struct a*)malloc(NC * sizeof(struct a));

	// Заполнение данных
	for (i = 0; i < NC; i++)
		fscanf(in, "%s%s%d%f%d", data[i].name, data[i].town, &data[i].population, &data[i].persent, &data[i].million);

	// Вывод данных
	for (i = 0; i < NC; i++)
		printf("\n%40s %-15s %-15d %-4.1f %d", data[i].name, data[i].town, data[i].population, data[i].persent, data[i].million);
	_getch();

	// После вывода данных открывается меню в бесконечном цикле
	while (1)
	{
		// Установка цветов и позиции курсора
		Console::ForegroundColor = ConsoleColor::Blue;
		Console::BackgroundColor = ConsoleColor::Red;
		Console::Clear();
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Yellow;
		Console::CursorLeft = 10;
		Console::CursorTop = 4;
		// Отрисовка разделителя
		printf(BlankLine);

		// Вывод на экран списка вопросов
		for (i = 0; i < 7; i++)
		{
			Console::CursorLeft = 10;
			Console::CursorTop = i + 5;
			printf(" %-55s ", dan[i]);
		}
		// Установка курсора в определенную позицию и отрисовка разделителя
		Console::CursorLeft = 10;
		Console::CursorTop = 12;
		printf(BlankLine);

		// Работа с меню через switch case
		n = menu(7);
		switch (n) {
		case 1: most_populated(data); break;	// Если выбран пункт 1
		case 2: is_same_million(data); break;	// Если выбран пункт 2
		case 3: is_same_population(data); break;// Если выбран пункт 3
		case 4: alfalist(data); break;			// Если выбран пункт 4
		case 5: kolvo(data); break;				// Если выбран пункт 5
		case 6: diagram(data); break;			// Если выбран пункт 6
		case 7: exit(0);						// Если выбран пункт 7
		}
	}
	// Завершие программы
	return 0;
}

// Функция работы с меню
// Позволяет отлавливать нажатия вверх, вниз, enter, esc и 
// возвращать в качестве числовых значений на выход
int menu(int n)
{
	// Границы
	int y1 = 0, y2 = n - 1;
	// Отлавливаемое нажатие
	char c = 1;
	// Пока не нажали ESC
	while (c != ESC)
	{
		// Проверяем нажатие
		switch (c)
		{
		case DOWN: y2 = y1; y1++; break;	// Если вниз
		case UP: y2 = y1; y1--; break;		// Если вверх
		case ENTER: return y1 + 1;			// Если enter
		}
		// Задание активной строчки
		if (y1 > n - 1) { y2 = n - 1; y1 = 0; }
		if (y1 < 0) { y2 = 0; y1 = n - 1; }
		// Закрасить пункты меню
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Red;
		Console::CursorLeft = 11;
		Console::CursorTop = y1 + 5;
		// Вывести вопрос
		printf("%s", dan[y1]);
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Yellow;
		Console::CursorLeft = 11;
		Console::CursorTop = y2 + 5;
		// Вывести вопрос
		printf("%s", dan[y2]);
		// Считать следующий символ
		c = getch();
	}
	exit(0);
}

// Функция поиска самой населенной страны
void most_populated(struct a* country)
{
	// Итератор и элемент, в котором будет храниться страна
	int i = 0; struct a best;
	// Копируем первую страну во временную переменную
	strcpy(best.name, country[0].name);
	best.population = country[0].population;
	// В цикле по всем оставшимся странам
	for (i = 1; i < NC; i++)
		// Если популяция у страны больше
		if (country[i].population > best.population)
		{
			// Записать её во временную
			strcpy(best.name, country[i].name);
			best.population = country[i].population;
		}
	// По окончанию поиска вывести информацию на экран
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	Console::CursorLeft = 10;
	Console::CursorTop = 15;
	printf("Максимальную популяцию %d человек", best.population);
	Console::CursorLeft = 10;
	Console::CursorTop = 16;
	printf("имеет страна %s", best.name);
	// Дождаться любого нажатия
	_getch();
}

// Функция поиска стран с одинаковым количеством миллионников
void is_same_million(struct a* country)
{
	// Установка цветов
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	// Найдены ли такие страны
	bool same = false;
	// В цикле по всем странам
	for (int i = 0; i + 1 < NC; i++)
		// Ищем среди оставшихся
		for (int j = i + 1; j < NC; j++)
			// Если миллионников равно
			if (country[i].million == country[j].million)
			{
				// То запоминаем, что нашли
				same = true;
				// Форматированный вывод результата на экран
				Console::CursorLeft = 10;
				Console::CursorTop = 15;
				printf("Одинаковое количество %d городов миллионников", country[i].million);
				Console::CursorLeft = 10;
				Console::CursorTop = 16;
				printf("имеют страны %s и %s", country[i].name, country[j].name);
			}
	// Если не нашли ни одной пары
	if (!same)
	{
		// Форматированный вывод сообщения об отсутствии результатов на экран
		Console::CursorLeft = 10;
		Console::CursorTop = 15;
		printf("Стран с одинаковым количеством городов миллионников не найдено");
	}
	// Ожидание нажатия любой клавиши
	_getch();
}

// Поиск одинаково населенных городов
// Так как информации о населении городов нет
// Функция сделана в виде заглушки
void is_same_population(struct a* country)
{
	// Форматированный вывод на экран сообщения о недостаточности данных
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	Console::CursorLeft = 10;
	Console::CursorTop = 15;
	printf("Недостаточно данных для проведения расчётов");
	// Ожидание нажатия любой клавиши
	_getch();
}

// Функция вывода списка в алфавитном порядке
void alfalist(struct a* country)
{
	// Итератор
	int i;
	// Список
	struct sp* nt;
	// Задание цветов
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	Console::Clear();
	// Если список не пуст
	if (!spisok1)
		// В цикле по всем странам
		for (i = 0; i < NC; i++)
			// Сортировка
			vstavka(country, country[i].town);
	// Вывод списка столиц
	printf("\n  Алфавитный список столиц");
	printf("\n  ============================\n");
	// Для всех элементов из отсортированного списка spisok1
	for (nt = spisok1; nt != 0; nt = nt->sled)
		// Вывод столицы и страны
		printf("\n %-20s %s", nt->town, nt->name);
	// Ожидание нажатия любой клавиши
	_getch();
}

// Функция сортировки списка стран по столице
void vstavka(struct a* country, char* town)
{
	// Итератор
	int i;
	// Вспомогательные списки
	struct sp* nov, * nt, * z = 0;
	// Поиск элемента списка, где сравнение городов даст результат >= 0
	for (nt = spisok1; nt != 0 && strcmp(nt->town, town) < 0; z = nt, nt = nt->sled);
	// Если список не пуст и функция сравнения вернула 0, завершить работу функции
	if (nt && strcmp(nt->town, town) == 0) return;
	// Выделение памяти под новый список
	nov = (struct sp*)malloc(sizeof(struct sp));
	// Копирование названия столицы
	strcpy(nov->town, town);
	// Сохранине в хвост списка nt
	nov->sled = nt;
	// Для всех элементов списка
	for (i = 0; i < NC; i++)
		// Если элемент с нужной столицей
		if (strcmp(country[i].town, town) == 0)
			// Копируем название страны
			strcpy(nov->name, country[i].name);
	// Если список z пуст
	if (!z) spisok1 = nov;
	// Иначе
	else z->sled = nov;
	return;
}

// Функция сортировки списка стран по названию страны
void vstavka_stran(struct a* country, char* name)
{
	// Итератор
	int i;
	// Вспомогательные списки
	struct sp* nov, * nt, * z = 0;
	// Поиск элемента списка, где сравнение стран даст результат >= 0
	for (nt = spisok2; nt != 0 && strcmp(nt->name, name) < 0; z = nt, nt = nt->sled);
	// Если список не пуст и функция сравнения вернула 0, завершить работу функции
	if (nt && strcmp(nt->name, name) == 0) return;
	// Выделение памяти под новый список
	nov = (struct sp*)malloc(sizeof(struct sp));
	// Копирование названия страны
	strcpy(nov->name, name);
	// Сохранение в хвост списка nt
	nov->sled = nt;
	// Для всех элементов списка
	for (i = 0; i < NC; i++)
		// Если элемент с нужным названием страны
		if (strcmp(country[i].name, name) == 0)
			// Копируем популяцию
			nov->population = country[i].population;
	// Если список z пуст
	if (!z) spisok2 = nov;
	// Иначе
	else z->sled = nov;
	return;
}

// Функция подсчёта количества стран с населением больше 1000000000
void kolvo(struct a* country)
{
	// Счётчик
	int count = 0;
	// Для всех стран
	for (int i = 0; i < NC; i++)
		// Если такая страна найдена, увеличить счётчик
		if (country[i].population > 1000000000) count++;
	// Форматированный вывод результатов на экран
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	Console::CursorLeft = 10;
	Console::CursorTop = 15;
	printf("%d стран(а,ы) с населением больше 1000000000", count);
	// Ожидание нажатия любой клавиши
	_getch();
}

// Функция отрисовки диаграммы
void diagram(struct a* country)
{
	// Вспомогательный список
	struct sp* nt;
	// Длина, итератор, цвет
	int len, i, NColor;
	// Сумма всего населения
	long long sum = 0;
	// Название страны
	char str1[20];
	// Популяция
	char str2[20];
	// Задание цветов
	System::ConsoleColor Color;
	Console::ForegroundColor = ConsoleColor::Black;
	Console::BackgroundColor = ConsoleColor::Yellow;
	Console::Clear();
	// Для всех стран
	for (i = 0; i < NC; i++) {
		// Посчитать суммарную популяцию
		sum = sum + country[i].population;
	}
	// Если список пуст
	if (!spisok2)
		// Отсортировать его
		for (i = 0; i < NC; i++)
			vstavka_stran(country, country[i].name);
	// Задать начальный цвет
	Color = ConsoleColor::Black; NColor = 0;
	// Для всех элементов списка
	for (nt = spisok2; nt != 0; nt = nt->sled, i++)
	{
		// Вывести страну и популяцию
		sprintf(str1, "%s", nt->name);
		sprintf(str2, "%3.1f%%", (nt->population * 100. / sum));
		Console::ForegroundColor = ConsoleColor::Black;
		Console::BackgroundColor = ConsoleColor::Yellow;
		Console::CursorLeft = 5; Console::CursorTop = i + 1;
		printf(str1);
		Console::CursorLeft = 20;
		printf("%s", str2);
		// Заменить цвета
		Console::BackgroundColor = ++Color; NColor++;
		Console::CursorLeft = 30;
		// Отрисовывать строку диаграммы
		for (len = 0; len < nt->population * 100. / sum; len++) printf(" ");
		// Если дошли до цвета 14
		if (NColor == 14)
		{
			// Начать сначала
			Color = ConsoleColor::Black; NColor = 0;
		}
		// Если дошли до цвета фона, увеличить его
		if (NColor == 12) NColor++;
	}
	// Ожидание нажатия любой клавиши
	_getch();
}
