hostname ISP
no ip domain-lookup
interface s0/0/0
ip address 10.1.1.2 255.255.255.252
no shutdown
interface s0/0/1
ip add 10.2.2.2 255.255.255.252
clock rate 56000
no shutdown
router eigrp 1
network 10.1.1.0 0.0.0.3
network 10.2.2.0 0.0.0.3
no auto-summary
end