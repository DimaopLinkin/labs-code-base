hostname LOCAL
no ip domain-lookup
interface s0/0/0
ip address 10.1.1.1 255.255.255.252
clock rate 56000
no shutdown
interface g0/1
ip add 192.168.1.1 255.255.255.0
no shutdown
router eigrp 1
network 10.1.1.0 0.0.0.3
network 192.168.1.0 0.0.0.255
no auto-summary
ip host REMOTE 10.2.2.1 192.168.3.1
ip host ISP 10.1.1.2 10.2.2.2
ip host LOCAL 192.168.1.1 10.1.1.1
ip host PC-C 192.168.3.3
ip host PC-A 192.168.1.3
ip host S1 192.168.1.11
ip host S3 192.168.3.11
end