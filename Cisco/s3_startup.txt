hostname S3
no ip domain-lookup
interface vlan 1
ip add 192.168.3.11 255.255.255.0
no shutdown
exit
ip default-gateway 192.168.3.1
end