double calculate(double numberLeft, double numberRight) {
  final left = numberLeft ?? 0;
  final right = numberRight ?? 0;
  return (left * right).roundToDouble();
}

final vkToken =
    'f66a983586f672f20934f23607918c2d08e38736451f3bf1c71902945858a61f33df537531d8053ace265';
