import 'package:dart_application_2/dart_application_2.dart';
import 'package:test/test.dart';

void main() {
  test('calculate 42 * 1 = 42', () {
    expect((calculate(42, 1)), 42);
  });
  test('calculate 42 * null = 0', () {
    expect((calculate(42, null)), 0);
  });
  test('calculate null * null = 0', () {
    expect((calculate(null, null)), 0);
  });
  test('calculate null * 42 = 0', () {
    expect((calculate(null, 42)), 0);
  });
  test('calculate', () {
    expect((calculate(2.4, 12.2)), 29.0);
  });
}
