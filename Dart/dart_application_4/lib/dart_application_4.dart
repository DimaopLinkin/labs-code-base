import 'package:http/http.dart' as http;

Future<http.Response> getRequest(String uri) async {
  return await http.get(Uri.parse(uri));
}
