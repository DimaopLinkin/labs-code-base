import 'package:assignment_03/employee.dart';
import 'package:flutter/material.dart';

import 'database.dart';

class CreateScreen extends StatelessWidget {
  const CreateScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Creating employee'),
      ),
      body: ListView(children: [
        WidgetForm(),
      ]),
      floatingActionButton: FloatingActionButton(
        heroTag: "btn_bck",
        child: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

class WidgetForm extends StatefulWidget {
  const WidgetForm({Key key}) : super(key: key);

  @override
  _WidgetFormState createState() => _WidgetFormState();
}

class _WidgetFormState extends State<WidgetForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    String firstName;
    String lastName;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              onSaved: (String value) {
                firstName = value;
              },
              controller: firstNameController,
              validator: (value) {
                if (value.isEmpty) return 'Enter a FirstName';
                return null;
              },
              decoration: InputDecoration(
                labelText: 'Enter FirstName',
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              onSaved: (String value) {
                lastName = value;
              },
              controller: lastNameController,
              validator: (value) {
                if (value.isEmpty) return 'Enter a LastName';
                return null;
              },
              decoration: InputDecoration(
                labelText: 'Enter LastName',
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              child: Icon(Icons.person_add),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  await DBProvider.db.create(
                      Employee(firstName: firstName, lastName: lastName));
                  Navigator.pop(context);
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
