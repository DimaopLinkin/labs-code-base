import 'dart:io';

import 'package:assignment_03/employee.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TestDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Employees ("
          "id INTEGER PRIMARY KEY,"
          "first_name TEXT,"
          "last_name TEXT"
          ")");
    });
  }

  create(Employee employee) async {
    final db = await database;
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Employees");
    int id = table.first['id'];
    var result = await db.rawInsert(
        "INSERT Into Employees (id,first_name,last_name)"
        " VALUES (?,?,?)",
        [id, employee.firstName, employee.lastName]);
    return result;
  }

  Future<List<Employee>> read() async {
    final db = await database;
    var result = await db.query("Employees");
    List<Employee> employees = result.isNotEmpty
        ? result.map((json) => Employee.fromMap(json)).toList()
        : [];
    return employees;
  }

  find(int id) async {
    final db = await database;
    var result = await db.query("Employees", where: "id = ?", whereArgs: [id]);
    return result.isNotEmpty ? Employee.fromMap(result.first) : null;
  }

  Future<List<Employee>> findWhere(
      {String firstName = "", String lastName = ""}) async {
    final db = await database;
    var result = await db.query("Employees",
        where: "(first_name like ?) or (last_name like ?)",
        whereArgs: ['%$firstName%', '%$lastName%']);
    List<Employee> employees = result.isNotEmpty
        ? result.map((json) => Employee.fromMap(json)).toList()
        : [];
    return employees;
  }

  update(Employee employee) async {
    final db = await database;
    var result = await db.update("Employees", employee.toMap(),
        where: "id = ?", whereArgs: [employee.id]);
    return result;
  }

  delete(int id) async {
    final db = await database;
    db.delete("Employees", where: "id = ?", whereArgs: [id]);
  }
}
