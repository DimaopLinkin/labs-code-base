import 'package:assignment_03/employee.dart';
import 'package:flutter/material.dart';

import 'database.dart';

class EditScreen extends StatelessWidget {
  const EditScreen({Key key, this.employ}) : super(key: key);
  final Employee employ;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editing employee'),
      ),
      body: ListView(children: [
        WidgetForm(
          empl: employ,
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        heroTag: "btn_bck",
        child: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

class WidgetForm extends StatefulWidget {
  const WidgetForm({Key key, this.empl}) : super(key: key);
  final Employee empl;

  @override
  _WidgetFormState createState() => _WidgetFormState();
}

class _WidgetFormState extends State<WidgetForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    firstNameController.text = widget.empl.firstName;
    lastNameController.text = widget.empl.lastName;
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              onSaved: (String value) {
                widget.empl.firstName = value;
              },
              controller: firstNameController,
              validator: (value) {
                if (value.isEmpty) return 'Enter a FirstName';
                return null;
              },
              decoration: InputDecoration(
                labelText: 'Enter FirstName',
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              onSaved: (String value) {
                widget.empl.lastName = value;
              },
              controller: lastNameController,
              validator: (value) {
                if (value.isEmpty) return 'Enter a LastName';
                return null;
              },
              decoration: InputDecoration(
                labelText: 'Enter LastName',
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              child: Icon(Icons.done),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  await DBProvider.db.update(widget.empl);
                  Navigator.pop(context);
                }
              },
            ),
          )
        ],
      ),
    );
  }
}
