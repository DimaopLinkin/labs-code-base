import 'dart:convert';

Employee employeeFromJson(String str) {
  final jsonData = json.decode(str);
  return Employee.fromMap(jsonData);
}

String employeeToJson(Employee data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Employee {
  int id;
  String firstName;
  String lastName;

  Employee({
    this.id,
    this.firstName,
    this.lastName,
  });

  factory Employee.fromMap(Map<String, dynamic> json) => Employee(
        id: json['id'],
        firstName: json['first_name'],
        lastName: json['last_name'],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'first_name': firstName,
        'last_name': lastName,
      };
}
