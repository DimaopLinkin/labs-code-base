import 'package:assignment_03/create_screen.dart';
import 'package:assignment_03/employee.dart';
import 'package:assignment_03/search_screen.dart';
import 'package:flutter/material.dart';

import 'dataBase.dart';
import 'edit_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Denis Andrianov')),
      body: FutureBuilder<List<Employee>>(
        future: DBProvider.db.read(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Employee>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                Employee employee = snapshot.data[index];
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(employee.lastName),
                            Text(employee.firstName),
                          ],
                        ),
                        ElevatedButton(
                          child: Icon(Icons.edit),
                          onPressed: () async {
                            final value = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        EditScreen(employ: employee)));
                            setState(() {});
                          },
                        )
                      ],
                    ),
                    leading: Text(employee.id.toString()),
                    trailing: FloatingActionButton(
                      heroTag: "btn$index",
                      child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      backgroundColor: Colors.white,
                      onPressed: () {
                        DBProvider.db.delete(employee.id);
                        setState(() {});
                      },
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: "btn-1",
            child: Icon(Icons.search),
            onPressed: () async {
              final value = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchScreen()));
              setState(() {});
            },
          ),
          SizedBox(width: 8),
          FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () async {
              final value = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CreateScreen()));
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
