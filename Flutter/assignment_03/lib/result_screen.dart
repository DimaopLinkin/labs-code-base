import 'package:flutter/material.dart';

import 'edit_screen.dart';
import 'employee.dart';

class ResultScreen extends StatefulWidget {
  const ResultScreen({Key key, this.employees}) : super(key: key);
  final employees;
  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Results screen')),
      body: ListView.builder(
        itemCount: widget.employees.length,
        itemBuilder: (BuildContext context, int index) {
          Employee employee = widget.employees[index];
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(employee.lastName),
                      Text(employee.firstName),
                    ],
                  ),
                  ElevatedButton(
                    child: Icon(Icons.edit),
                    onPressed: () async {
                      final value = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  EditScreen(employ: employee)));
                      setState(() {});
                    },
                  )
                ],
              ),
              leading: Text(employee.id.toString()),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: "btn_bck",
        child: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
