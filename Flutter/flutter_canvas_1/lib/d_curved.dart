import 'package:flutter/material.dart';

class DCurved extends StatelessWidget {
  final Offset lightSource;
  final Widget child;
  const DCurved({Key key, this.lightSource, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final innerShadowWidth = lightSource.distance * 0.1;
    return Container(
      decoration: BoxDecoration(
        // color: Colors.black,
        shape: BoxShape.circle,
        gradient: RadialGradient(
            // center: Alignment(
            //   this.widget.lightSource.dx,
            //   this.widget.lightSource.dy,
            // ),
            stops: [1 - innerShadowWidth, 1],
            colors: [Color(0x661F1F1F), Colors.black]),
      ),
      child: this.child,
    );
  }
}
