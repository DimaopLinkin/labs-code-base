import 'package:flutter/material.dart';
import 'package:flutter_canvas_1/sphere_ball.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter 3D Ball',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter 3D Ball'),
        ),
        body: Container(
          margin: EdgeInsets.all(8),
          child: Column(
            children: [
              SphereBall(),
            ],
          ),
        ),
      ),
    );
  }
}
