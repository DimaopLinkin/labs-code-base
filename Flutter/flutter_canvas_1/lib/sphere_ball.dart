import 'package:flutter/cupertino.dart';
import 'package:flutter_canvas_1/d_curved.dart';
import 'package:flutter_canvas_1/shadow_sphere.dart';
import 'package:flutter_canvas_1/sphere_density.dart';
import 'package:flutter_canvas_1/triangle.dart';

class SphereBall extends StatefulWidget {
  const SphereBall({Key key}) : super(key: key);

  @override
  _SphereBallState createState() => _SphereBallState();
}

class _SphereBallState extends State<SphereBall> {
  static const lightSoucre = Offset(0, -0.75);

  @override
  Widget build(BuildContext context) {
    final size = Size.square(MediaQuery.of(context).size.shortestSide);

    return Stack(
      children: [
        ShadowSphere(diameter: size.shortestSide),
        SphereDensity(
          lightSource: lightSoucre,
          diameter: size.shortestSide,
          child: Transform(
            origin: size.center(Offset.zero),
            transform: Matrix4.identity()..scale(0.5),
            child: DCurved(
              lightSource: lightSoucre,
              child: Triangle(text: "The Triangle"),
            ),
          ),
        ),
      ],
    );
  }
}
