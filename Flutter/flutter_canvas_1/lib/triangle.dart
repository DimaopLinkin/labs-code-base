import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Triangle extends StatelessWidget {
  const Triangle({Key key, this.text}) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _TrianglePainter(),
      child: Container(
          alignment: Alignment.center,
          child: Text(text,
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.white, fontSize: 30))),
    );
  }
}

class _TrianglePainter extends CustomPainter {
  final _gradient =
      LinearGradient(colors: [Colors.blue.shade400, Colors.blue.shade900]);

  @override
  void paint(Canvas canvas, Size size) {
    final painter = Paint()
      ..shader = _gradient.createShader(Offset.zero & size)
      ..style = PaintingStyle.fill;

    final w = size.width;
    final h = size.height;
    final path = Path()
      ..moveTo(w * 0.2, h * 0.3)
      ..quadraticBezierTo(w * 0.5, h * 0.1, w * 0.8, h * 0.3)
      ..quadraticBezierTo(w * 0.85, h * 0.6, w * 0.5, h * 0.85)
      ..quadraticBezierTo(w * 0.15, h * 0.6, w * 0.2, h * 0.3)
      // ..conicTo(w * 0.15, h * 0.6, w * 0.2, h * 0.3, 1)
      // ..lineTo(w * 0.2, h * 0.3)
      // ..cubicTo(w * 0.5, h * 0.5, w * 0, h * 0.5, w * 0.2, h * 0.3)
      // ..relativeLineTo(0, -500)
      // ..moveTo(w * 1, h * 1)
      // ..lineTo(w * 0, h * 1)
      // ..lineTo(w * 0, h * 0.8)
      ..close();
    final rotatedRedRect = Path()
      ..moveTo(300, 300)
      ..lineTo(340, 280)
      ..lineTo(340, 330)
      ..lineTo(300, 350)
      ..close();
    final rotatedBlueRect = Path()
      ..moveTo(300, 300)
      ..lineTo(260, 280)
      ..lineTo(260, 330)
      ..lineTo(300, 350)
      ..close();
    final rotatedOrangeRect = Path()
      ..moveTo(300, 300)
      ..lineTo(260, 280)
      ..lineTo(300, 260)
      ..lineTo(340, 280)
      ..close();
    final baseGreyRect = Path()
      ..moveTo(300, 300)
      ..lineTo(300.0 - 45 * 3, 300.0 - 22 * 3)
      ..lineTo(300.0 - 45 * 6, 300)
      ..lineTo(300.0 - 45 * 6, 300.0 + 55 * 3)
      ..lineTo(300.0 - 45 * 3, 300.0 + 55 * 3 + 22 * 3)
      ..lineTo(300.0, 300.0 + 55 * 3)
      ..close();

    final redSidePaint = Paint()..color = Colors.red[600];
    final blueSidePaint = Paint()..color = Colors.blue[600];
    final yellowSidePaint = Paint()..color = Colors.yellow[600];
    final greenSidePaint = Paint()..color = Colors.green[600];
    final orangeSidePaint = Paint()..color = Colors.orange[800];
    final whiteSidePaint = Paint()..color = Colors.grey[100];

    canvas
      ..drawPath(path, painter)
      ..drawCircle(Offset(0.0, -70), 80.0, Paint()..color = Colors.deepOrange)
      ..drawDRRect(
          RRect.fromLTRBXY(300, -80, 400, 40, 20, 60),
          RRect.fromLTRBXY(320, -60, 380, 20, 30, 10),
          Paint()..color = Colors.teal)
      ..drawRect(
          Rect.fromLTRB(320, 320, 420, 420), Paint()..color = Colors.red);
    for (var i = 30.0; i <= 170; i += 2) {
      canvas
        ..drawLine(Offset.zero, Offset(-100.0 + i, -100.0),
            Paint()..color = Colors.deepPurple);
    }
    canvas
      ..translate(234, 50)
      ..drawPath(baseGreyRect.shift(Offset(-218, 432)),
          Paint()..color = Colors.grey[900])
      ..drawPath(rotatedRedRect.shift(Offset(-350, 500)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-350, 555)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-350, 610)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-305, 478)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-305, 533)), yellowSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-305, 588)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-260, 456)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-260, 511)), redSidePaint)
      ..drawPath(rotatedRedRect.shift(Offset(-260, 566)), redSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-355, 500)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-355, 555)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-355, 610)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-400, 478)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-400, 533)), orangeSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-400, 588)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-445, 456)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-445, 511)), blueSidePaint)
      ..drawPath(rotatedBlueRect.shift(Offset(-445, 566)), blueSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-352.5, 497)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-397.5, 475)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-442.5, 453)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-307.5, 475)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-352.5, 453)), greenSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-397.5, 431)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-262.5, 453)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-307.5, 431)), whiteSidePaint)
      ..drawPath(rotatedOrangeRect.shift(Offset(-352.5, 409)), whiteSidePaint);
  }

  @override
  bool shouldRepaint(_TrianglePainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(_TrianglePainter oldDelegate) => false;
}
