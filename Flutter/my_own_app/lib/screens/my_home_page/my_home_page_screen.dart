import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: showMain(context),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.cake),
      ),
    );
  }

  Column showMain(BuildContext context) {
    if (_counter >= 15) {
      _counter = 0;
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Congratulations!!!'),
          Text(
            'You\'re IDIOT!',
            style: Theme.of(context).textTheme.headline4,
          ),
          Text('Hit the button to make yourself an idiot again'),
        ],
      );
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'You have tapped:',
        ),
        Text(
          '$_counter times',
          style: Theme.of(context).textTheme.headline4,
        ),
      ],
    );
  }
}
