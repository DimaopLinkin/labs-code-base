package com.company;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

class TreeLook {
    JFrame form;
    JScrollPane js;
    JTree jTree;

    TreeLook() {
        form = new JFrame(); // главная форма програмы
        form.setBounds(10, 20, 650, 500);
        form.setTitle("Просмотр дерева");
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setLayout(null);
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("Двоичное дерево"); // вершина дерева
        jTree = new JTree(top);
        js = new JScrollPane(jTree); // панель с деревом
        js.setBounds(0, 0, 300, 300);
        form.add(js); // закрепление панели к форме
        form.show();
        DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("111"); // три вершины дерева
        DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("222");
        DefaultMutableTreeNode node3 = new DefaultMutableTreeNode("333");
// добавление их корню
        top.add(node1);
        top.add(node2);
        top.add(node3);
// добавление листьев к второй ветке
        node2.add(new DefaultMutableTreeNode("444"));
        node2.add(new DefaultMutableTreeNode("555"));
        node2.add(new DefaultMutableTreeNode("666"));
        for (int i = 0; i < jTree.getRowCount(); i++) // показ всех вершин
        {
            jTree.expandRow(i);
        }
    }
}

public class Main {

    public static void main(String[] args) {
        Tree tr = new Tree();
        tr = tr.Insert(5,101);
        tr = tr.Insert(7,102);
        tr = tr.Insert(4,103);
        tr = tr.Insert(2,104);
        tr = tr.Insert(6,105);
        tr = tr.Insert(8,106);

        int k;
        k = tr.Find(6);
        System.out.println(k);
        k = tr.Find(3);
        System.out.println(k);

        TreeLook f;
        f= new TreeLook();
    }
}
