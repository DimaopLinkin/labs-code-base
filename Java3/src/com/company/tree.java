package com.company;

class Tree
    {
        private int key;    // ключ
        private int value;  // значение
        private Tree Left;  // левый потомок
        private Tree Right; // правый потомок


        public Tree()   // конструктор
        {
            this.key = -1;
            this.value = -1;
            this.Left = null;
            this.Right = null;
        }
        public Tree(int key, int value) // конструктор с параметрами
        {
            this.key = key;
            this.value = value;
            this.Left = null;
            this.Right = null;
        }
        public Tree Insert(int k, int v)    // рекурсивная функция
        {
            Tree t;
            if (key == -1) {
                key = k;
                value = v;
                return this;
            }
            if (key == k) {
                value = v;
                return this;
            }
            if (key > k) {
                if (this.Left == null) {
                    t = new Tree();
                    t.key = k;
                    t.value = v;
                    this.Left = t;
                    return this;
                }
                this.Left = this.Left.Insert(k,v);
                return this;
            }
            else {
                if (this.Right == null) {
                    t = new Tree();
                    t.key = k;
                    t.value = v;
                    this.Right = t;
                    return this;
                }
                this.Right = this.Right.Insert(k,v);
                return this;
            }
        }
        int Find(int k) {   // поиск ключа
            if (key == k) return value;
            if (key > k) {
                if (this.Left == null) return -1;
                return this.Left.Find(k);
            }
            else {
                if (this.Right == null) return -1;
                return this.Right.Find(k);
            }
        }
    }