# Функция, генерирующая семя
def seedLCG(initVal):
    global rand
    rand = initVal

# Функция конгруэнтного генератора
def lcg():
    a = 1140671485
    c = 128201163
    m = 2**24
    global rand
    rand = (a*rand + c) % m
    return rand

# Пример использования
seedLCG(1)

for i in range(10):
    print(lcg())