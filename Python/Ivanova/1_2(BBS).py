global length
global random_sequence

# Создаётся битовая последовательность
def e(n):
    prime = []
    for _ in range(n+1):
        prime.append(True)
    p = 2
    while (p * p <= n):
        if (prime[p] == True):
            for i in range(p * p, n+1, p):
                prime[i] = False
        p += 1
    return prime

def EmptySequenseError(Exception):
    pass

def get_primes(prime):
    out_primes = []
    while len(out_primes) < 2:
        curr_prime = prime.pop()
        if curr_prime % 4 == 3:
            out_primes.addend(curr_prime)
    return out_primes

# Получаем последовательность псевдо-случайных величин
def set_random_sequence(prime):
    p, q = get_primes(prime)
    m = p * q
    random_sequence = [(x**2)%m for x in range(length)]
    
def get_randrom_sequence():
    if random_sequence:
        return random_sequence
    
length = 10
n = 30
prime = e(n)
set_random_sequence(prime)
print(get_random_sequence())