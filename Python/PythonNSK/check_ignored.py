import argparse
import os
import re
# Инициализация парсера
parser = argparse.ArgumentParser()
# Добавление обязательного аргумента
parser.add_argument("--project_dir", type=str, help="Path to your project")
# Считывание введенного аргумента
directory = parser.parse_args().project_dir[1:]

# Открытие файла .gitignore
gitignore = open(directory + '/.gitignore', 'r')
ignored = []
# Запись в массив всех строк из .gitignore
for line in gitignore:
    ignored.append(line[:len(line)-1])

# Считывание "дерева" директории
tree = os.walk(directory)
print("Ignored files:")
# Для каждой директории дерева
for address, dirs, files in os.walk(directory):
    # Для каждого файла в директории
    for file in files:
        # Сохраняем полный путь к файлу
        checking_file = os.path.join(address, file)
        # Для всех строк из .gitignore
        for word in ignored:
            # Для re.search() необходимо удалить символ *, если он присутствует
            if word[0] == '*':
                result = re.search(word[1:], checking_file)
            else:
                result = re.search(word, checking_file)
            # Если поиск удался
            if result:
                # Вывести результат
                print(checking_file, "ingored by expression", word)
                break
