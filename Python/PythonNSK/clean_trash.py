import argparse
import time
import os
import logging
# Инициализация парсера
parser = argparse.ArgumentParser()
# Добавление первого обязательного аргумента
parser.add_argument("--trash_folder", type=str, help="Path to your trash")
# Добавление второго обязательного аргумента
parser.add_argument("--age_thr", type=int, help="Time to delete files")

# Считывание введенного аргумента
trash = parser.parse_args().trash_folder[1:]
# Считывание введенного аргумента
age = parser.parse_args().age_thr

# Настройка конфига логгера
logging.basicConfig(level=logging.INFO, filename='clean_trash.log', filemode='w', format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
# Бесконечный цикл
while True:
    # Для каждого пути в дереве
    for address, dirs, files in os.walk(trash):
        # Если директория пустая, и это не сама урна
        if dirs == [] and files == [] and address != trash:
            # Записать в лог сообщение
            logging.info('Path ' + address + ' was empty. Deleted.')
            # Удалить директорию
            os.rmdir(address)
            # Перейти к следующей итерации
            continue
        # Для всех файлов в директории
        for file in files:
            # Сохранить путь и имя файла
            file_path = os.path.join(address, file)
            # Если разница между текущим временем и временем последнего изменения файла больше или равно заданному
            if time.time() - os.path.getctime(file_path) >= age:
                # Записать в лог сообщение
                logging.info('File ' + file_path + ' too old. Deleted.')
                # Удалить файл
                os.remove(file_path)
    # Ожидание одну секунду
    time.sleep(1)