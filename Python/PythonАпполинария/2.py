# Ввод числа
N = int(input('Введите число N: '))

# Отрисовка через цикл
for i in range(0,N):
    if i == 0 or i == N-1:
        print(' '*i+'*'*(N))
    else:
        print(' '*i+'*'+' '*(N-2)+'*')
        