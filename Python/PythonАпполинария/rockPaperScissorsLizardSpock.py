import random

print("Добро пожаловать в игру камень ножницы бумага ящерица спок!")
print("-----------------------------------------------------------")
print()

player_total_score = 0
computer_total_score = 0

print(""" Допустимые команды:
             R - камень(rock)
        S - ножницы(scissors)
            P - бумага(paper)
          L - ящерица(lizard)
              O - спок(spock)

""")
while True:
    while True:
        text = input("Хотите сыграть? (Y - да, N - нет) >>> ")
        text = text.lower()
        if text in 'yn':
            break
    if text == 'n':
        print(f"Итоговый счёт: ИГРОК-{player_total_score} -- КОМП-{computer_total_score}")
        break
    if text == 'y':
        player_score = 0
        computer_score = 0
        
        game_round = 3
        while game_round > 0:
            
            while True:
                player_choice = input("Ваш ход(R, P, S, L, O) ")
                player_choice = player_choice.lower()
                if player_choice in 'rsplo':
                    break
                
            computer_choice = random.choice('rsplo')
            print(f"Компьютер выбрал: {computer_choice.upper()}")
            
            if computer_choice == player_choice:
                print("\tНичья!")
                
            elif player_choice == 'r' and computer_choice == 's':
                print("У игрока КАМЕНЬ. У компьютера НОЖНИЦЫ.")
                print("Камень затупляет ножницы")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
                
            elif player_choice == 'r' and computer_choice == 'p':
                print("У игрока КАМЕНЬ. У компьютера БУМАГА.")
                print("Бумага оборачивает камень")
                print("Побеждает компьтер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'r' and computer_choice == 'l':
                print("У игрока КАМЕНЬ. У компьютера ЯЩЕРИЦА.")
                print("Камень давит ящерицу")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
                
            elif player_choice == 'r' and computer_choice == 'o':
                print("У игрока КАМЕНЬ. У компьютера СПОК.")
                print("Спок превращает камень в пар")
                print("Побеждает компьтер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 's' and computer_choice == 'r':
                print("У игрока НОЖНИЦЫ. У компьютера КАМЕНЬ.")
                print("Камень затупляет ножницы")
                print("Побеждает компьтер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 's' and computer_choice == 'p':
                print("У игрока НОЖНИЦЫ. У компьютера БУМАГА.")
                print("Ножницы режут бумагу")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
                
            elif player_choice == 's' and computer_choice == 'o':
                print("У игрока НОЖНИЦЫ. У компьютера СПОК.")
                print("Спок ломает ножницы")
                print("Побеждает компьтер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 's' and computer_choice == 'l':
                print("У игрока НОЖНИЦЫ. У компьютера ЯЩЕРИЦА.")
                print("Ножницы отрезают голову ящерице")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
                
            elif player_choice == 'p' and computer_choice == 'r':
                print("У игрока БУМАГА. У компьютера КАМЕНЬ.")
                print("Бумага оборачивает камень")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'p' and computer_choice == 'l':
                print("У игрока БУМАГА. У компьютера ЯЩЕРИЦА.")
                print("Ящерица съедает бумагу")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'p' and computer_choice == 'o':
                print("У игрока БУМАГА. У компьютера СПОК.")
                print("Бумага опровергает спока")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'p' and computer_choice == 's':
                print("У игрока БУМАГА. У компьютера НОЖНИЦЫ.")
                print("Ножницы режут бумагу")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'l' and computer_choice == 'p':
                print("У игрока ЯЩЕРИЦА. У компьютера БУМАГА.")
                print("Ящерица съедает бумагу")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'l' and computer_choice == 'r':
                print("У игрока ЯЩЕРИЦА. У компьютера КАМЕНЬ.")
                print("Камень давит ящерицу")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'l' and computer_choice == 'o':
                print("У игрока ЯЩЕРИЦА. У компьютера СПОК.")
                print("Ящерица отравляет спока")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'l' and computer_choice == 's':
                print("У игрока ЯЩЕРИЦА. У компьютера НОЖНИЦЫ.")
                print("Ножницы отрезают голову ящерице")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'o' and computer_choice == 's':
                print("У игрока СПОК. У компьютера НОЖНИЦЫ.")
                print("Спок ломает ножницы")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'o' and computer_choice == 'p':
                print("У игрока СПОК. У компьютера БУМАГА.")
                print("Бумага опровергает спока")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
                
            elif player_choice == 'o' and computer_choice == 'r':
                print("У игрока СПОК. У компьютера КАМЕНЬ.")
                print("Спок превращает камень в пар")
                print("Побеждает игрок!")
                player_score += 1
                player_total_score += 1
            
            elif player_choice == 'o' and computer_choice == 'l':
                print("У игрока СПОК. У компьютера ЯЩЕРИЦА.")
                print("Ящерица отравляет спока")
                print("Побеждает компьютер!")
                computer_score += 1
                computer_total_score += 1
    
            print(f"Счёт раунда: ИГРОК-{player_score} -- КОМП-{computer_score}")
            print("-----------------------------------------------------------")
            print()
                
            game_round -= 1
    if game_round == 0:
        print("-- !!!ИТОГИ ИГРЫ!!! --")        
        if player_score == computer_score:
            print("\tНичья!")
        elif player_score > computer_score:
            print("\tВы победили!")
        else:
            print("\Извините... Но победил компьтер!")
    
