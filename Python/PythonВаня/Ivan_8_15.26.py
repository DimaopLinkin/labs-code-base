file = open('15_26.txt', 'r')                                                   #Открыть файл в режиме чтения

for line in file:                                                               #Пройти по всем строкам в файле
    substring = line[:len(line)-1] + '!'                                        #Вывести строку в консоль с восклицательным знаком
    print(substring)
file.close()                                                                    #Закрыть файл