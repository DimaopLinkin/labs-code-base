a = int(input('Введите A '))                                                   # Ввод А
b = int(input('Введите B '))                                                   # Ввод B

import random                                                                  # Подключение библиотеки random

for _ in range(5):                                                             # В цикле из пяти итераций
    print(random.randint(a, b))                                                # Вывести случайное целое число на отрезке [A;B]