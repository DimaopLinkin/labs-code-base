import random                                                                  # Подключение библиотеки random

bochonokList = []                                                              # Обозначение списка бочонков

for _ in range(5):                                                             # В цикле из пяти итераций
    bochonok = random.randint(1, 90)                                           # Задать бочонку целое значение из интервала [1;90]
    bochonokList.append(bochonok)                                              # Добавить номер бочонка в конец списка
        
print(bochonokList)                                                            # Вывести список бочонков