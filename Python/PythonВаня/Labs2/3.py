import random                                                                  # Подключение библиотеки random

bochonokList = []                                                              # Обозначение списка бочонков

for _ in range(5):                                                             # В цикле из пяти итераций
    bochonok = random.randint(1, 90)                                           # Задать бочонку целое значение из интервала [1;90]
    while bochonok in bochonokList:                                            # До тех пор, пока номер бочонка есть в нашем списке (проверка на повторное выпадение)
        bochonok = random.randint(1, 90)                                       # Генерировать новое число в заданном интервале
    bochonokList.append(bochonok)                                              # Добавить номер бочонка в конец списка
        
print(bochonokList)                                                            # Вывести список бочонков