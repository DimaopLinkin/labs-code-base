import random                                                                  # Подключение библиотеки random

a = random.randint(1, 6)                                                       # "Бросок" первого кубика
b = random.randint(1, 6)                                                       # "Бросок" второго кубика

print('Первый кубик %d' % a)                                                   # Вывод первого кубика
print('Второй кубик %d' % b)                                                   # Вывод второго кубика
print(a+b)                                                                     # Вывод суммы кубиков