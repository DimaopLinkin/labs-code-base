import random                                                                  # Подключение библиотеки random

# Предположим, что игра 2D, и метеорит летит в игрока. В таком случае,
# направление можно задать углом относительно персонажа от 1 до 360 градусов

direction = random.randint(1, 360)

print('Направление в 2D игре %d' % direction)

# Если игра 3D, и метеорит летит так же в игрока

directionXY = random.randint(1, 360)

directionYZ = random.randint(1, 90)

print('Направление в 3D игре на плоскости %d' % directionXY)
print('Угол с неба %d' % directionYZ)