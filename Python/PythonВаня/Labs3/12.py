a = str(input('Введите трехзначное число '))

boolean = True

for char in a:
    if int(char) % 2:
        boolean = False
        
if boolean:
    print('Да')
else:
    print('Нет')