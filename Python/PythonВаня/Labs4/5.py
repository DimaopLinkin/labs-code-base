number = int(input('Введите число ')) # Ввод числа
maximum = 0               # Зануляем максимум
for numeral in str(number):   # Проходим все цифры
    if int(numeral) > maximum: # Если течущая цифра больше максимума
        maximum = int(numeral)   # То делаем её максимальной
print(maximum)            # Выводим максимум