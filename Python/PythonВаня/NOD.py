def minus(a, b):
    i = 0
    a = ''.join(reversed(a))
    b = ''.join(reversed(b))
    for _ in range(len(b)):
        if a[i] == 'g' or a[i] < b[i]:
            a = a[:i] + str(int(a[i]) + 1) + a[i+1:]
            if a[i+1] == '0':
                a = a[:i+1] + 'g' + a[i+2:]
            else:
                a = a[:i+1] + '0' + a[i+2:]
        a = a[:i] + str(int(a[i]) - int(b[i])) + a[i+1:]
        i += 1
            
    return ''.join(reversed(a))

print("Добро пожаловать в программу рассчёта НОД")
print("Данная программа делает все расчёты в двоичной системе счисления")
system = 0
while system not in [2, 8, 10, 16]:
    system = int(input("Введите исходную систему счисления 2, 8, 10, 16: "))
a = bin(int(input(), system))[2:]
b = bin(int(input(), system))[2:]

while a != b:
    if a > b:
        a = minus(a, b)
        print(a)
    else:
        b = minus(b, a)
        print(b)