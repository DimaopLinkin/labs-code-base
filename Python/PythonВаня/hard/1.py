# Блок подключение библиотек
from tkinter import *
from tkinter import colorchooser
from PIL import ImageGrab
from tkinter.filedialog import *

# Основной класс приложения
class Paint(Frame):
    # Конструктор
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.color = "black"             # Цвет кисти
        self.brush_size = 10             # Размер кисти
        self.parent.geometry("1000x800") # Размер экрана
        self.setUI()                     # Вывод интерфейса
        
    # Функции задания цвета
    def set_color(self, new_color):
        if new_color == "white":
            self.color = new_color
        else:
            self.color = new_color[1]
    # Функция задания размера кисти
    def set_brush_size(self):
        self.brush_size = int(self.spinbox.get())
    # Функция рисования
    def draw(self, event):
        self.canvas.create_oval(event.x - self.brush_size,
                                event.y - self.brush_size,
                                event.x + self.brush_size,
                                event.y + self.brush_size,
                                fill = self.color, outline = self.color)
    # Функция использования ластика правой кнопкой мыши
    def drawErase(self, event):
        self.canvas.create_oval(event.x - self.brush_size,
                                event.y - self.brush_size,
                                event.x + self.brush_size,
                                event.y + self.brush_size,
                                fill = "white", outline = "white")
    # Функция сохранения рисунка
    def save(self):
        file = asksaveasfile(defaultextension='.jpg')
        x = self.canvas.winfo_rootx()
        y = self.canvas.winfo_rooty()
        x1 = x + self.canvas.winfo_width()
        y1 = y + self.canvas.winfo_height()
        ImageGrab.grab().crop((x,y,x1,y1)).save(file)
     
    # Функция вывода интерфейса
    def setUI(self):
        self.parent.title("Paint by Kalachev")
        self.pack(fill = BOTH, expand = 1)
        self.columnconfigure(6, weight = 1)
        self.rowconfigure(1, weight = 1)
        self.canvas = Canvas(self, bg = "white")
        self.canvas.grid(row = 1, column = 0, columnspan = 7, padx = 5, pady = 5, sticky = E+W+S+N)
        self.canvas.bind("<B1-Motion>", self.draw)
        self.canvas.bind("<B3-Motion>", self.drawErase)
        size_label = Label(self, text = "Размер кисти: ")
        size_label.grid(row = 0, column = 0)
        self.spinbox = Spinbox(self, 
                               width = 5, 
                               from_ = 1, 
                               to = 100, 
                               textvariable = start, 
                               command = self.set_brush_size)
        self.spinbox.grid(row = 0, column = 1, padx = 5)
        color_button = Button(self, text = "Выбрать цвет", width = 10, command = lambda:
            self.set_color(colorchooser.askcolor()))
        color_button.grid(row = 0, column = 2)
        erase_button = Button(self, 
                              text = "Ластик", 
                              width = 10, 
                              command = lambda: 
                              self.set_color("white"))
        erase_button.grid(row = 0, column = 3)
        clear_button = Button(self, text = "Очистить", width = 10, command = lambda: self.canvas.delete("all"))
        clear_button.grid(row = 0, column = 4)
        save_button = Button(self, text = "Сохранить", width = 10, command = self.save)
        save_button.grid(row = 0, column = 5)
        
root = Tk()
start = IntVar()
start.set(10)
app = Paint(root)
root.mainloop()