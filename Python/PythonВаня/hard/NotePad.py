# Блок подключения библиотек
import tkinter
from tkinter import *
# Библиотеки сохранения и открытия файлов
from tkinter.filedialog import asksaveasfile, askopenfile
# Библиотека для отображения ошибок
from tkinter.messagebox import showerror
from tkinter import messagebox

# Блок определения функций
FILE_NAME = tkinter.NONE # Переменная для хранения имени файла. По умолчанию пустое

# Функция создания нового файла
def new_file():
    global FILE_NAME
    FILE_NAME = "Новый"
    text.delete('1.0', tkinter.END)
    
# Функция сохранения файла
def save_file():
    data = text.get('1.0', tkinter.END) # Получаем весь текст из окна
    out = open(FILE_NAME, 'w')  # Открываем пустой файл в режиме записи
    out.write(data) # Запись полученного текста в файл
    out.close()     # Закрытие файла
    
# Функция сохраненить как
def save_as():
    # Открытие окна "сохранить как" в режиме записи и форматом txt по умолчанию
    out = asksaveasfile(mode='w', defaultextension='txt')
    data = text.get('1.0', tkinter.END)
    # Проверка на ошибку
    try:
        out.write(data.rstrip())
    except Exception:
        showerror(title="Ошибка", message="При сохранении файла произошла ошибка.")

# Функция открытия файла
def open_file():
    global FILE_NAME
    file = askopenfile(mode="r") # Открыть файл в режиме чтения
    if file is None:    # Если ничего не открылось
        return          # Выйти из программы
    FILE_NAME = file.name   # Если ОК задаем в программе имя файла
    data = file.read()  # Чистаем из него данные в переменную data
    text.delete('1.0', tkinter.END) # Очистка поля
    text.insert('1.0', data)    # Ввод в поле данных
    
# Функция отображения справки
def information():
    messagebox.showinfo("Справка", "Это просто редактор.\nСправка здесь не нужна\nВыполнил Калачев Иван")
    
# Блок описания окна
root = tkinter.Tk()     # root - Экземпляр tkinter, созданный конструктором Tk()
root.title("NotePad by Kalachev")   # Заголовок окна

root.minsize(width=550, height=550) # Минимальный и 
root.maxsize(width=550, height=550) # максимальный размеры окна

# Виджет текстового поля. (Окно, ширина, высота, _)
text = tkinter.Text(root, width=500, height=500, wrap="word")
# Скроллбар (Окно, ориентация, привязка в полю)
scrollbar = Scrollbar(root, orient=VERTICAL, command=text.yview)
scrollbar.pack(side="right", fill="y")
text.configure(yscrollcomman=scrollbar.set)

# Отображение интерфейса с помощью метода pack
text.pack()

menuBar = tkinter.Menu(root)

# Блок описания меню Файл
fileMenu = tkinter.Menu(menuBar)
fileMenu.add_command(label="Новый", command=new_file)
fileMenu.add_command(label="Открыть", command=open_file)
fileMenu.add_command(label="Сохранить", command=save_file)
fileMenu.add_command(label="Сохранить как", command=save_as)

# Блок описания меню
menuBar.add_cascade(label="Файл", menu=fileMenu)
menuBar.add_cascade(label="Справка", command=information)
menuBar.add_cascade(label="Выход", command=root.quit)

root.config(menu=menuBar)

# Вызов интерфейса
root.mainloop()