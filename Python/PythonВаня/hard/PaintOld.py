# Блок импорта библиотек
import tkinter
from tkinter import *
from tkinter.filedialog import asksaveasfile
from PIL import ImageGrab

# Основной класс
class Paint():
    def __init__(self,root):
        self.root = root
        self.root.title("Paint by Kalachev")   # Заголовок окна
        self.root.geometry("800x600")
        self.color = "black"
        self.size = "1"
        
# Глобальные переменные
CANVAS_WIDTH = 800  # Ширина канваса
CANVAS_HEIGHT = 800 # Высота канваса

BRUSH_SIZE = 1 # Размер кисти по умолчанию
BRUSH_COLOR = "black" # Цвет кисти по умолчанию

# Блок определения функций
# Функция рисования
def paint(event):
    global BRUSH_SIZE
    global COLOR
    x1 = event.x - BRUSH_SIZE
    x2 = event.x + BRUSH_SIZE
    y1 = event.y - BRUSH_SIZE
    y2 = event.y + BRUSH_SIZE
    
    window.create_oval(x1, y1, x2, y2, fill=BRUSH_COLOR, outline=BRUSH_COLOR)

# Функция изменения размера
def brush_size_change(size):
    global BRUSH_SIZE
    BRUSH_SIZE = size
    
# Функция изменения цвета
def color_change(color):
    global BRUSH_COLOR
    BRUSH_COLOR = color
    
# Блок описания окна
root = tkinter.Tk()     # root - Экземпляр tkinter, созданный конструктором Tk()
root.title("Paint by Kalachev")   # Заголовок окна

# Область рисования (окно, ширина, высота, цвет фона)
window = Canvas(root, 
                width=CANVAS_WIDTH,
                height=CANVAS_HEIGHT,
                bg="white")

# Работа мыши в канвас
window.bind("<B1-Motion>", paint)

erase_btn = Button(text="Ластик", width=10, command=lambda: color_change("white"))

window.grid(row=2, 
            column=0, 
            columnspan=7, 
            padx=5, 
            pady=5, 
            sticky=E+W+S+N)
window.columnconfigure(6, weight=1)
window.rowconfigure(2, weight=1)
erase_btn.grid(row=0, column=0)

root.mainloop()