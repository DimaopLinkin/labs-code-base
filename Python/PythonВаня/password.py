import random # Подключение библиотеки random

## Определение функции генерации рандомной строки
def generate_random_pass():
    # Список символов, используемых в пароле
    letters = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!@#$%^&*()_+=-'
    # В цикле из 6 итераций присоединяем к строке символ, выбранный случайно из строки letters
    password = ''.join(random.choice(letters) for i in range(6))
    return password # Возвращаем полученную строку (пароль)

## Код программы
f = open('pass.txt', 'w') # Открыть файл pass.txt в режиме записи (w)
for index in range(100):  # В цикле из 100 итераций
    f.write(generate_random_pass() + '\n')  # Записать полученный в функции пароль с символом переноса строки в конце (\n)
f.close() # Закрыть файл