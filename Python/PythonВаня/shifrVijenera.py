## Функция преобразования строки в шифр
## Получает в параметре исходную строку
## Возвращает зашифрованную строку методом кодового слова
def shifr(key, string):
    # Алфавит
    letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    # Новое слово
    new_string = ''
    # Для всех символов из исходной строки
    for i in range(len(string)):
        # Если символ не буква
        if string[i] in letters:
            # Записать зашифрованный символ
            new_string = new_string + letters[(letters.index(string[i])+letters.index(key[i%(len(key))]))%len(letters)]
        # Иначе
        else:
            # То записать символ
            new_string = new_string + string[i]
    # Вернуть получившуюся строку
    return new_string

# Ввод строки
string = input('Введите строку на русском языке\n')
# Ввод ключа
key = input('Введите ключ шифрования\n')
# Вывод строки
print(shifr(key, string))
