## Функция преобразования строки в шифр
## Получает в параметре исходную строку
## Возвращает зашифрованную строку методом кодового слова
def shifr(key, string):
    # Алфавит
    letters = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    # Новое слово
    new_string = ''
    # Шифр
    new_letters = letters
    # Для всех символов ключа
    for char in key:
        # Удалить символ из нового словаря
        new_letters = new_letters[0:new_letters.index(char)] + new_letters[new_letters.index(char)+1: len(new_letters)]
    # Добавить вначале словаря ключ
    new_letters = key + new_letters
    # Для всех символов из исходной строки
    for char in string:
        # Если символ не буква
        if char in new_letters:
            # Записать зашифрованный символ
            new_string = new_string + new_letters[(letters.index(char))]
        # Иначе
        else:
            # То записать символ
            new_string = new_string + char
    # Вернуть получившуюся строку
    return new_string

# Ввод строки
string = input('Введите строку на русском языке\n')
# Ввод ключа
key = input('Введите ключ шифрования\n')
# Вывод строки
print(shifr(key, string))
