a = float(input('Введите а: ')) # Ввод а

a2 = a * a      # a * a = a^2
a4 = a2 * a2    # a^2 * a^2 = a^4
a8 = a4 * a4    # a^4 * a^4 = a^8
a16 = a8 * a8   # a^8 * a^8 = a^16
a20 = a16 * a4  # a^16 * a^4 = a^20
a21 = a20 * a   # a^20 * a = a^21

print('a= %d' % a21)