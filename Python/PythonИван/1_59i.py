x = float(input('Введите координату x: '))  # Ввод координаты x
y = float(input('Введите координату y: '))  # Ввод координаты y

if -2 <= x <= 1 and y >= (x-1)/3:           # Если точка выше нижней черты И
    if y <= (x-3)/2 and -2 <= x <= -1:      # Если точка ниже на отрезке [-2; -1]
        print('Точка принадлежит заштрихованной области')
    if y <= -x and -1 <= x <= 0:            # Если точка ниже на отрезке [-1; -0]
        print('Точка принадлежит заштрихованной области')
    if y <= 0 and 0 <= x <= 1:              # Если точка ниже на отрезке [0; 1]
        print('Точка принадлежит заштрихованной области')