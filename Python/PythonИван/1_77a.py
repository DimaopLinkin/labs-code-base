n = int(input('Введите n: '))   # Ввод n

result = 2                      # Задать начальное значение результата 2

for _ in range(n):              # Умножить результат на самого себя n раз
    result = result*result
        
if n == 0:          # Если степень равна нулю
    result = 1      # То result^0 = 1
    
print('Результат равен: %d' % result)   # Вывести результат