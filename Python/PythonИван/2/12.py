x = str(input('Введите трехзначное число x: '))

logic = True # Задать значение по умолчанию

for char in x: # Для всех цифр в x
    if int(char) % 2:    # Если цифра нечетная
        logic = False    # То задать ложь
        
if logic:
    print('Да')
else:
    print('Нет')