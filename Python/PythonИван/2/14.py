number = int(input("Введите номер месяца: "))

if (number == 1 or number == 2 or number ==12):
    print("Зима")
elif (number > 2 and number < 6):
    print("Весна")
elif (number > 5 and number < 9):
    print("Лето")
elif (number > 8 and number < 12):
    print("Осень")
else:
    print("Неверный ввод")