a = float(input('Введите а: ')) # Ввод а
b = float(input('Введите b: ')) # Ввод b

if a == 0: # Если первый коэффициент нулевой
    print('Х - любое число') # То невозможно найти икс
else: # Иначе считаем икс
    x = b/a
    print('Х = %.2f' % x)