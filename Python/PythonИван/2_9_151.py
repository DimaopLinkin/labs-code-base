string = input('Введите текст: ')   # Ввод текста
    
maximum = 0     # Максимальное число
i = 0           # Начальный индекс
j = 0           # Конечный индекс

for char in string:
    if char == ' ' or j == len(string) - 1: # Находим пробел или конец строки
        try:        # Проверяем число ли между индексом i и j
            if j == len(string) - 1:    # Если конец строки
                j += 1              # То увеличить j на 1
            substring = string[i:j] # Если да, то сравниваем с сохранённым максимальным числом
            if int(maximum) < int(substring):
                maximum = substring     # Если найденное число больше, то сохраняем его
        except:
            print('Найдено не число')   # Если не число, то выводим ошибку в консоль
        i = j + 1   # Увеличиваем начальный индекс
    j += 1          # Увеличиваем конечный индекс
print('Максимальное число: %s' % maximum)   # Вывод максимума
            