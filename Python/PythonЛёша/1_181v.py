import math
a = [int(input('Целое число: ')) for _ in range(5)] # Ввести 5 целых чисел
s = 0                       # Cумма элементов         
for element in a[:len(a)]:# Циклом по всем элементам
    if math.fabs(element) < 12:     # Если значение элемента меньше, чем его порядок в квадрате
        s += element        # То добавить элемент к сумме
print('Сумма элементов = %d' % s)   # Вывод суммы элементов