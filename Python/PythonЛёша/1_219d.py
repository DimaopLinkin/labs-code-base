a = -1          # По условию
b = 2           # По условию
n = 40          # По условию
h = (b - a) / n # По условию
x = []          # Сделать Х списком
y = []          # Сделать Y списком

import math     # Подключить библиотеку math
for i in range(n):                          # Для n элементов
    x.append(a + i * h)                     # Посчитать x[i]
    y.append(x[i] * math.e ** -x[i])        # Посчитать y[i]
    if (x[i] >= 0):
        print('%d\t( %.2f;\t %.2f)' % (i+1, x[i], y[i])) # Вывести пару элементов
    else:
        print('%d\t(%.2f;\t%.2f)' % (i+1, x[i], y[i])) # Вывести пару элементов
    