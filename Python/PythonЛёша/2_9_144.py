string = input('Введите десятичное число: ')    # Ввести число и записать его в виде строки
result = 0                  # Сумма цифр
for char in string:         # Для всех цифр
    result += int(char)     # Прибавить цифру
print(result)               # Вывести результат