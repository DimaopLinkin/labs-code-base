def impl(a, b):
    if a == 1 and b == 0: return 0
    return 1

A = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1]
B = [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1]
C = [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1]
D = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
print("(B&C)!|(D&A)|(D->C)")
print("A B C D F")
for i in range(16):
    print(A[i], B[i], C[i], D[i], int((not((B[i] and C[i]) or (D[i] and A[i]))) or (impl(D[i], C[i]))))