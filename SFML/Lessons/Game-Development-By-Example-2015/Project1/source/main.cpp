#include "window.hpp"

int main(){
	sf::RenderWindow window(sf::VideoMode(640,480),
		"Rendering the rectangle and soldier.");
	
	// Creating our shape.
	sf::RectangleShape rectangle(sf::Vector2f(128.0f,128.0f));
	rectangle.setFillColor(sf::Color::Red);
	rectangle.setPosition(320,240);
	rectangle.setOrigin(rectangle.getSize().x / 2,rectangle.getSize().y / 2);

	// Creating our soldier
	sf::Texture soldierTexture;
	if(!soldierTexture.loadFromFile("../images/soldier.png")){
		// Handle error.
	}
	sf::Sprite soldier(soldierTexture);
	sf::Vector2u size = soldierTexture.getSize();
	soldier.setOrigin(size.x / 2, size.y / 2);
	sf::Vector2f increment(0.4f, 0.4f);

	while(window.isOpen()){
		sf::Event event;
		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				window.close();
			}
		}

		if((soldier.getPosition().x + (size.x / 2) > 
			window.getSize().x && increment.x > 0) ||
			(soldier.getPosition().x - (size.x / 2) < 0 &&
			increment.x < 0))
		{
			// Reverse the direction on X axis.
			increment.x = -increment.x;
		}

		if((soldier.getPosition().y + (size.y / 2) > 
			window.getSize().y && increment.y > 0) ||
			(soldier.getPosition().y - (size.y / 2) < 0 &&
			increment.y < 0))
		{
			// Reverse the direction on Y axis.
			increment.y = -increment.y;
		}

		soldier.setPosition(soldier.getPosition() + increment);

		window.clear(sf::Color(16,16,16,255));
		window.draw(rectangle);
		window.draw(soldier);
		window.display();
	}
	return 0;
}