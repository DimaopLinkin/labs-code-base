#ifndef GAME_HPP
#define GAME_HPP

#include "window.hpp"

class Game{
public:
  Game() : m_window("Chapter 2", sf::Vector2u(800,600)){
    // Setting up class members.
    m_soldierTexture.loadFromFile("../images/soldier.png");
    m_soldier.setTexture(m_soldierTexture);
    m_increment = sf::Vector2i(400,400);
  }
  ~Game(){}

  void HandleInput(){}
  void Update(){
    m_window.Update();  // Update window events.
    MoveSoldier();
  }
  void Render(){ 
    m_window.BeginDraw(); // Clear.
    m_window.Draw(m_soldier);
    m_window.EndDraw(); // Display.
  }
  Window* GetWindow(){ return &m_window; }
  sf::Time GetElapsed(){ return m_elapsed; }
  void RestartClock(){ m_elapsed = m_clock.restart(); }
private:
  void MoveSoldier(){
    sf::Vector2u l_windSize = m_window.GetWindowSize();
    sf::Vector2u l_textSize = m_soldierTexture.getSize();

    if((m_soldier.getPosition().x >
        l_windSize.x - l_textSize.x && m_increment.x > 0) ||
        (m_soldier.getPosition().x < 0 && m_increment.x < 0)){
          m_increment.x = -m_increment.x;
    }

    if((m_soldier.getPosition().y >
        l_windSize.y - l_textSize.y && m_increment.y > 0) ||
        (m_soldier.getPosition().y < 0 && m_increment.y < 0)){
          m_increment.y = -m_increment.y;
    }

    float fElapsed = m_elapsed.asSeconds();

    m_soldier.setPosition(
        m_soldier.getPosition().x + (m_increment.x * fElapsed),
        m_soldier.getPosition().y + (m_increment.y * fElapsed));

  }
  Window m_window;
  sf::Texture m_soldierTexture;
  sf::Sprite m_soldier;
  sf::Vector2i m_increment;
  sf::Clock m_clock;
  sf::Time m_elapsed;
};

#endif // GAME_HPP