#include "game.hpp"

int main(){
	// Program entry point.
	Game game;
	while(!game.GetWindow()->IsDone()){
		// Game loop.
		game.HandleInput();
		game.Update();
		game.Render();
		game.RestartClock();
	}
	return 0;
}