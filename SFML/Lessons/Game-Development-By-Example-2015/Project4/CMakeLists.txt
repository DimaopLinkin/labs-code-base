cmake_minimum_required(VERSION 3.0.0)
project(Project4 VERSION 0.1.0)

set(
    INTERNAL_LIBS
    source/window.hpp
    source/game.hpp
    source/snake.hpp
    source/world.hpp
    source/textbox.hpp
)

add_executable(
    Project4 
    source/main.cpp
    ${INTERNAL_LIBS}
)

set (
    GLIBS
    sfml-graphics
    sfml-system
    sfml-window
)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -I/usr/local/Cellar/sfml/2.5.1_1/include -L/usr/local/Cellar/sfml/2.5.1_1/lib -Dnullptr=0")

target_link_libraries(Project4 ${GLIBS})