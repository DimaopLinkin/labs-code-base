#ifndef GAME_HPP
#define GAME_HPP

#include "window.hpp"
#include "world.hpp"
#include "textbox.hpp"

class Game{
public:
  Game() : m_window("Snake", sf::Vector2u(800,600)),
    m_snake(m_world.GetBlockSize(), &m_textbox), 
    m_world(sf::Vector2u(800,600))
  {
    m_clock.restart();
    srand(time(nullptr));

	  m_textbox.Setup(5,14,350,sf::Vector2f(225,0));
    m_elapsed = 0.0f;

    m_textbox.Add("Seeded random number generator with: " + std::to_string(time(nullptr)));
  }
  ~Game(){}

  void HandleInput(){
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
      && m_snake.GetPhysicalDirection() != Down)
    {
      m_snake.SetDirection(Up);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
      && m_snake.GetPhysicalDirection() != Up)
    {
      m_snake.SetDirection(Down);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
      && m_snake.GetPhysicalDirection() != Right)
    {
      m_snake.SetDirection(Left);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
      && m_snake.GetPhysicalDirection() != Left)
    {
      m_snake.SetDirection(Right);
    }
  }
  void Update(){
    m_window.Update();  // Update window events.
    float timestep = 1.0f / m_snake.GetSpeed();

    if(m_elapsed >= timestep){
      m_snake.Tick();
      m_world.Update(m_snake);
      m_elapsed -= timestep;
      if(m_snake.HasLost()){
        m_textbox.Add("GAME OVER! Score: "
				  + std::to_string((long long)m_snake.GetScore()));
        m_snake.Reset();
      }
    }
  }
  void Render(){ 
    m_window.BeginDraw(); // Clear.
    // Render here.
    m_world.Render(*m_window.GetRenderWindow());
    m_snake.Render(*m_window.GetRenderWindow());
    m_textbox.Render(*m_window.GetRenderWindow());

    m_window.EndDraw(); // Display.
  }
  Window* GetWindow(){ return &m_window; }
  float GetElapsed(){ return m_elapsed; }
  void RestartClock(){ m_elapsed += m_clock.restart().asSeconds(); }
private:
  Window m_window;
  sf::Clock m_clock;
  float m_elapsed;
  World m_world;
  Snake m_snake;
  Textbox m_textbox;
};

#endif // GAME_HPP