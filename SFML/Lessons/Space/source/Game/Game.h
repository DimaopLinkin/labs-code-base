#ifndef GAME_H
#define GAME_H

#include <iostream>

#include <SFML/Graphics.hpp>

namespace Textures
{
    enum ID { Landscape, Airplane, Missile };
}

class Game
{
    public:
    Game();
    void run();

    private:
    void processEvents();
    void update(sf::Time deltaTime);
    void render();
    void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);

    private:
    sf::RenderWindow mWindow;
    sf::CircleShape mPlayer;
    bool mIsMovingUp;
    bool mIsMovingDown;
    bool mIsMovingLeft;
    bool mIsMovingRight;
};

class TextureHolder
{
    public:
        void load(Textures::ID id, const std::string& filename);
        sf::Texture& get(Textures::ID id);

    private:
        std::map<Textures::ID, std::unique_ptr<sf::Texture> > mTextureMap;
};

#endif // GAME_H